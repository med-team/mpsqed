<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE helpset PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 2.0//EN" "http://java.sun.com/products/javahelp/helpset_2_0.dtd">
<helpset version="2.0">
<title>RKI Sequence Editor</title>
<maps>
<homeID>_file.ae869282</homeID>
<mapref location="map.jhm" />
</maps>
<view xml:lang="ar" mergetype="javax.help.UniteAppendMerge">
  <name>TOC</name>
  <label>helpset.toc.title</label>
  <type>javax.help.TOCView</type>
  <data>toc.xml</data>
</view>
<view xml:lang="ar">
<name>Search</name>
<label>helpset.search.title</label>
<type>javax.help.SearchView</type>
<data engine="com.sun.java.help.search.DefaultSearchEngine">
JavaHelpSearch
</data>
</view>
</helpset>
