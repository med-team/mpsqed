:title Analyze
:label analyze

The analysis menu offers some basic sequence analysis options.

:image analyzeMenu.png|The analysis menu

The following options are available:
- Calculate master consensus: Calculates and displays the consensus sequence of all sequences in the alignment
- Calculate group consensus: Calculates and displays the consensus sequence for every defined <<grouping|group>>
- Keep consensus up to date: If this is checked, the consensus sequence(s) will automatically be updated whenever a sequence is modified.
- Group identical sequences: A new group is created for each set of identical sequences in the alignment
- Sort sequences: The sequences can be sorted by name, by degree of identity (to the consensus sequence or, if a reference sequence is selected, to the reference sequence) or by degree of identity within the current selection. Sorting maintains grouping: If sequences are grouped, then sorting will sort the sequences within each group individually
- Find all SNPs: This automatically calculates master and group consensus sequences. Then, all positions are marked which are fully conserved within each of the defined groups but not across the whole alignment (meaning that they can be used to differentiate between at least two of the defined groups)
- Show predicted flowgrams: If <<multiplexPyrosequencing|pyrosequencing primers>> are defined, the flowgrams are shown which would be generated during multiplex pyrosequencing for each of the defined <<grouping|groups>>. A list of <<grouping|groups>> which are expected to generate no unique pyrogram is provided.

:image pyrograms.png|Predicted pyrograms: The groups VARV and MPXV generate unique pyrograms with the two defined pyrosequencing primers, while the pyrograms for the groups All, VACV and CPXV are identical to each other.