:title Grouping
:label grouping

Sequences can be organised in groups. This is useful for finding SNPs which could be used in species-specific assays using the "Find all SNPs" entry in the <<analyze|Analyze>> menu or in the design of <<multiplexPyrosequencing|multiplex pyrosequencing assays>>.

Sequences can be grouped together by <<selecting|selecting>> one or more sequences, right-clicking on the selection and then choosing the "Set as group" option. A group name can then be entered, and the selected sequences will be moved to a new group with that name. The group of a sequence is shown in braces in front of the sequence name and groups can be collapsed or expanded by clicking on the minur or plus sign in front of the first sequence in a group. 

Sequences can be moved from group to group by right-clicking on a sequence and selecting the target group from the "Add sequence to group" entry.

There is no separate option to rename a group. However, empty groups will be deleted, so renaming a group can be done by selecting all sequences from a group, right-clicking, selecting "Set as group" and entering the new group name. All selected sequences will be moved to a new group with the new name, and the old group will be deleted since it will then be empty.