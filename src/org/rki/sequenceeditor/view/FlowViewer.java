/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author dabrowskiw
 */
public class FlowViewer extends JFrame implements ChangeListener {
    private FlowPanel _flowPanel;
    private JSpinner _spinner = new JSpinner();

    public FlowViewer(EditorInterface parent) {
        super();
        JPanel settings = new JPanel();
        settings.setLayout(new GridBagLayout());
        _spinner.setValue(20);
        _spinner.addChangeListener(this);
        settings.add(new JLabel("Flowgram length"));
        settings.add(_spinner);
        _flowPanel = new FlowPanel(parent);
        _flowPanel.length = 20;
        setLayout(new BorderLayout());
        add(settings, BorderLayout.NORTH);
        add(_flowPanel, BorderLayout.CENTER);
        setSize(600, 600);
        setVisible(false);
    }

    public void stateChanged(ChangeEvent e) {
        if(e.getSource() == _spinner) {
            _flowPanel.length = Integer.parseInt(_spinner.getValue().toString());
            repaint();
        }
    }

}
