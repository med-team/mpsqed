/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author dabrowskiw
 */
public class IntDocument extends PlainDocument {
    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        if(str==null)
            return;
        StringBuilder res = new StringBuilder();
        for(char c : str.toCharArray()) {
            if(c >= 48 && c <= 57) {
                res.append(c);
            }
        }
        super.insertString(offs, res.toString(), a);
    }
}