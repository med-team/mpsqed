/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.GroupKey;
import org.rki.sequenceeditor.model.MasterSequence;
import org.rki.sequenceeditor.model.Sequence;

/**
 *
 * @author dabrowskiw
 */
public class NamesPane extends JPanel implements ActionListener, MouseListener {
    private static final String INVERT = "Invert selected sequences";
    
    private boolean _menusInitialized = false;
    private EditorInterface _parent;
    private int[] _offset;
    private int[] _menuPos = {0, 0};
    private JPopupMenu _menu = new JPopupMenu();
    private JMenuItem _invert;
    private JMenuItem _setMaster;
    private JMenuItem _getConsensus;
    private JMenuItem _deleteSelected;
    private JMenuItem _getGroupConsensus = new JMenuItem("Calculate group consensus sequences");
    private JMenu _sortMenu = new JMenu("Sort sequences");
    private JMenuItem _sortByHomology = new JMenuItem("By degree of identity");
    private JMenuItem _sortBySelectionHomology = new JMenuItem("By degree of identity within selection");
    private JMenuItem _sortByName = new JMenuItem("By name");
    private JMenuItem _createGroup = new JMenuItem("Set as group");
    private JMenuItem _collapseGroup = new JMenuItem("Collapse group");
    private JMenu _addToGroup = new JMenu("Add sequence to group");
    private JMenuItem _groupIdentical = new JMenuItem("Group identical sequences");
    private ArrayList<GroupMenuItem> _groupMenuItems = new ArrayList<GroupMenuItem>();

    public NamesPane() {
        super();
        int[] off = {0, 0};
        init(off, null);
    }
    
    public NamesPane(int[] offset, EditorInterface parent) {
        super();
        init(offset, parent);
    }

    private void init(int[] offset, EditorInterface parent) {
        setParent(parent);
        _offset = offset;
    }

    private void setupMenus() {
        if(_menusInitialized)
            return;

        _invert = new JMenuItem(INVERT);
        _setMaster = new JMenuItem("Set master sequence");
        _getConsensus = new JMenuItem("Calculate master consensus sequence");
        _deleteSelected = new JMenuItem("Delete selected sequences");
        _invert.addActionListener(this);
        _setMaster.addActionListener(this);
        _getConsensus.addActionListener(this);
        _deleteSelected.addActionListener(this);
        _sortByName.addActionListener(this);
        _sortByHomology.addActionListener(this);
        _sortBySelectionHomology.addActionListener(this);
        _createGroup.addActionListener(this);
        _collapseGroup.addActionListener(this);
        _getGroupConsensus.addActionListener(this);
        _addToGroup.addActionListener(this);
        _groupIdentical.addActionListener(this);
        _sortMenu.add(_sortByHomology);
        _sortMenu.add(_sortBySelectionHomology);
        _sortMenu.add(_sortByName);
        _menu.add(_invert);
        _menu.add(_setMaster);
        if(!_parent.hasMenu()) {
            _menu.add(_getConsensus);
            _menu.add(_getGroupConsensus);
        }
        _menu.add(_deleteSelected);
        if(_parent.hasMenu()) 
            _menu.add(_sortMenu);
        _menu.add(_createGroup);
        _menu.add(_addToGroup);
        if(!_parent.hasMenu())
            _menu.add(_groupIdentical);
//        _menu.add(_collapseGroup);
        updateGroupMenuItems();
        _menusInitialized = true;
    }

    public void setParent(EditorInterface parent) {
        _parent = parent;
        if(_parent != null) {
            addMouseListener(_parent);
            addMouseMotionListener(_parent);
            setupMenus();
            addMouseListener(this);
            addKeyListener(parent);
        }
    }

    public void setOffset(int[] offset) {
        _offset = offset;
    }

    public void updateGroupMenuItems() {
        _addToGroup.removeAll();
        _groupMenuItems = new ArrayList<GroupMenuItem>();
        if(_parent == null) {
            return;
        }
        for(GroupKey group : _parent.getAlignment().getGroups()) {
            _groupMenuItems.add(new GroupMenuItem(group.getName(), group));
        }
        for(GroupMenuItem item : _groupMenuItems) {
            _addToGroup.add(item);
            item.addActionListener(this);
        }
    }
    
    private void drawCollapser(Graphics g, int pos, int lh, boolean collapsed, int cheight) {
        g.setColor(Color.BLACK);
        int yoff = (lh/cheight)/2;
        g.drawLine(1, pos-yoff, 9, pos-yoff);
        g.drawLine(1, pos-yoff, 1, pos-cheight+yoff);
        g.drawLine(1, pos-cheight-yoff, 9, pos-cheight-yoff);
        g.drawLine(9, pos-yoff, 9, pos-cheight+yoff);
        g.drawLine(3, pos-cheight/2-yoff, 7, pos-cheight/2+yoff);
        if(collapsed) {
            g.drawLine(5, pos-2-yoff, 5, pos-cheight+2+yoff);
        }
        g.setColor(Color.GRAY);
        g.drawLine(7, pos-yoff, 7, pos);
        g.setColor(Color.BLACK);
    }
    
    private void drawCollapser(Graphics g, int pos, int lh, boolean collapsed) {
        if(lh < 8) {
            drawCollapser(g, pos, lh, collapsed, lh);
        }
        else {
            drawCollapser(g, pos, lh, collapsed, 8);
        }    
    }
    
    private void drawGroupLine(Graphics g, int pos, int lh, int linelength, boolean last) {
        g.setColor(Color.GRAY);
        if(last) {
            g.drawLine(5, pos-lh+linelength, 5, pos-lh);
        }
        else {
            g.drawLine(5, pos-lh/2, 5, pos-lh);
        }
        g.drawLine(5, pos-lh/2, 9, pos-lh/2);
        g.setColor(Color.BLACK);
    }
    
    @Override
    public void paint(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.BLACK);
        if(_parent == null) {
            return;
        }
        int lh = _parent.getLetterHeight();
        g.setFont(new Font("Arial", Font.PLAIN, lh));
        Alignment align = _parent.getAlignment();
        GroupKey lastGroup = new GroupKey("__NONAME__");
        if(align.getSequence(_offset[1]-1) != null) {
            lastGroup = align.getSequence(_offset[1]-1).group;
        }
        for (int i = _offset[1]; i < align.countAll(); i++) {
            Sequence ls = align.getSequence(i-1);
            Sequence s = align.getSequence(i);
            Sequence ns = align.getSequence(i+1);
            //                ypos += s.above.levels.size() * _parent.annotationHeight;
            if(ls != null && ls.group == s.group && s.collapsed) {
                continue;
            }
            int pos0 = _parent.getScreenY(i);//(i-_offset[1])*lw+_parent.rulerHeight;
            int pos1 = _parent.getScreenY(i+1);//(i-_offset[1]+1)*lw+_parent.rulerHeight;
            if(pos0 > getHeight()) {
                break;
            }
            if(ls != null && s.group == ls.group && s.collapsed) {
                continue;
            }
            if(ls == null || ls.group != s.group) {
                if(_parent.getMasterSequences().get(s.group) != null) {
                    int num = _parent.getAlignment().count(s.group);
                    g.drawString(s.group.getName() + " consensus " + "(" + num + " sequences)", 2, pos0-_parent.getConsOffset());
                }
            }
            if(s.selected) {
                g.setColor(new Color(0xef, 0xef, 0xff));
                g.fillRect(-1, pos0-lh, getWidth()+2, lh);
                g.setColor(new Color(0xdf, 0xdf, 0xff));
                if(ns == null || !ns.selected) {
                    g.drawLine(-1, pos0, getWidth()+2, pos0);                    
                }
                if(ls == null || !ls.selected) {
                    g.drawLine(-1, pos0-lh, getWidth()+2, pos0-lh);
                }
                g.setColor(Color.BLACK);
            }
            g.drawString("(" + s.group.getName() + ")" + s.name, 12, pos0);
            if(s.group != lastGroup) {
                drawCollapser(g, pos0, lh, s.collapsed);
            }
            else {
                if(ns == null || ns.group != s.group) {
                    drawGroupLine(g, pos0, lh, lh, false);
                }
                else {
                    drawGroupLine(g, pos0, lh, pos1-pos0, true);
                }
            }
            lastGroup = s.group;
        }
        if(_parent.getMasterSequenceAll() != null) {
            g.drawString(_parent.getMasterSequenceAll().name, 0, _parent.getRulerHeight() + _parent.getLetterHeight());
        }
    }

    public void groupIdenticalSequences() {
        LinkedList<Integer> seqs = new LinkedList<Integer>();
        for(int i=0; i<_parent.getAlignment().countAll(); i++) {
            seqs.add(i);
        }
        HashMap<Integer, LinkedList<Integer>> groups = new HashMap<Integer, LinkedList<Integer>>();
        int lastLen = _parent.getAlignment().countAll() + 10;
        int groupNum = 1;
        while(lastLen != seqs.size() && seqs.size() > 1) {
            lastLen = seqs.size();
            LinkedList<Integer> currentGroup = new LinkedList<Integer>();
            String currentSeq = _parent.getAlignment().getSequence(seqs.get(0)).seq;
            Iterator<Integer> seqi = seqs.iterator();
            seqi.next();
            while(seqi.hasNext()) {
                int seqnum = seqi.next();
                String seq = _parent.getAlignment().getSequence(seqnum).seq;
                if(seq.equals(currentSeq)) {
                    seqi.remove();
                    currentGroup.add(seqnum);
                }
            }
            if(currentGroup.size() != 0) {
                currentGroup.add(seqs.get(0));
                seqs.remove(0);
                groups.put(groupNum, currentGroup);
                groupNum++;
            }
        }
        for(Integer group : groups.keySet()) {
            GroupKey groupKey = new GroupKey("Group " + group.toString());
            for(Integer seq : groups.get(group)) {
                _parent.getAlignment().getSequence(seq.intValue()).group = groupKey;
            }
        }
        _parent.getAlignment().sortGroups();
        _parent.consensusChange();
        _parent.repaintAll();
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == _invert) {
            for(Sequence seq : _parent.getAlignment().getSequencesAll()) {
                if(seq.selected) {
                    seq.invert();
                }
            }
            _parent.consensusChange();
            _parent.repaintAll();
        }
        else if(e.getSource() == _groupIdentical) {
            groupIdenticalSequences();
        }
        else if(e.getSource() == _createGroup) {
            Collection<GroupKey> groups = _parent.getAlignment().getGroups();
            String name = JOptionPane.showInputDialog(this, "Please enter group name:", "Group name",
                    JOptionPane.PLAIN_MESSAGE);
            if(name == null) {
                return;
            }
            GroupKey group = new GroupKey(name);
            for(Sequence seq : _parent.getAlignment().getSequencesAll()) {
                if(seq.selected) {
                    seq.group = group;
                }
            }
            updateGroupMenuItems();
            _parent.getAlignment().sortGroups();
            _parent.consensusChange();
            _parent.updateFlowgrams(false);
            _parent.repaintAll();
        }
        else if(e.getSource() == _setMaster) {
            _parent.setMasterSequenceAll(new MasterSequence(_parent.getAlignment().getSequence(_parent.getRealPos(_menuPos).y)));
        }
        else if(e.getSource() == _getConsensus) {
            _parent.getMasterConsensus();
        }
        else if(e.getSource() == _getGroupConsensus) {
            _parent.getGroupMasterSequences();
            _parent.repaintAll();
        }
        else if(e.getSource() == _deleteSelected) {
            LinkedList<Sequence> toDel = new LinkedList<Sequence>();
            for(Sequence seq : _parent.getAlignment().getSequencesAll()) {
                if(seq.selected) {
                    toDel.add(seq);
                }
            }            
            for(Sequence seq : toDel) {
                _parent.getAlignment().getSequencesAll().remove(seq);
            }
            _parent.consensusChange();
            _parent.repaintAll();
        }
        else if(e.getSource() == _sortByName) {
            _parent.getAlignment().sortByName();
            _parent.repaintAll();
        }
        else if(e.getSource() == _sortByHomology) {
            _parent.getAlignment().sortByHomology();
            _parent.repaintAll();
        }
        else if(e.getSource() == _sortBySelectionHomology) {
            _parent.getAlignment().sortByHomology(_parent.getSelFrom()[0], _parent.getSelTo()[0]);
            _parent.repaintAll();
        }
        else {
            for(GroupMenuItem item : _groupMenuItems) {
                if(e.getSource() == item) {
                    Sequence firstSeq = _parent.getAlignment().getSequences(item.group).iterator().next();
                    for(Sequence seq : _parent.getAlignment().getSequencesAll()) {
                        if(seq.selected) {
                            seq.group = item.group;
                            seq.collapsed = firstSeq.collapsed;
                        }
                    }
                    _parent.getAlignment().sortGroups();
                    _parent.updateFlowgrams(false);
                    _parent.repaintAll();                    
                }
            }
        }
    }
    
    public void showPopup(MouseEvent e) {
        if (e.getButton() == 3) {
            if(_parent.getAlignment().countAll() == 0)
                return;
            _menuPos[0] = e.getX();
            _menuPos[1] = e.getY();
            _menu.show(this, e.getX(), e.getY());
        }
    }
    
    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
        showPopup(e);
    }

    public void mouseReleased(MouseEvent e) {
        showPopup(e);
    }

    public void mouseEntered(MouseEvent arg0) {
    }

    public void mouseExited(MouseEvent arg0) {
    }
}
