package org.rki.sequenceeditor.view;

import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import org.rki.sequenceeditor.model.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.LinkedList;
import javax.swing.JFileChooser;
import javax.swing.JSplitPane;
import java.util.HashMap;
import java.util.ResourceBundle;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import org.rki.sequenceeditor.model.filters.FastaFilter;
import org.rki.sequenceeditor.model.filters.RSFFilter;
import org.json.JSONArray;
import org.json.JSONObject;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import javax.swing.ToolTipManager;
import org.rki.sequenceeditor.model.filters.BasicFastaFilter;
import org.rki.sequenceeditor.model.filters.GeneiousFilter;
import org.rki.sequenceeditor.model.filters.LoadFilter;
import org.rki.sequenceeditor.model.filters.SaveFilter;

public class EditorApp implements EditorInterface {

    final static ResourceBundle rb = ResourceBundle.getBundle("version");

    public static String VERSION = "0.9.2";
    public String BUILD = "";
    public static int MODE_NORMAL = 0;
    public static int MODE_EDITING = 1;
    public static int MODE_MOVESEL = 2;
    public static int MODE_MOVEANNO = 3;
    public static int MODE_RESIZEANNO_E = 4;
    public static int MODE_RESIZEANNO_W = 5;
    public static int MODE_SELECTING = 6;
    public static int MODE_SELECTING2 = 7;

    public static int MODE_RESIZESEL_E = 8;
    public static int MODE_RESIZESEL_W = 9;
    public static int MODE_RESIZESEL_N = 10;
    public static int MODE_RESIZESEL_S = 11;

    public static int SELECTION_RESIZE_BORDER = 4;

    public static FileFilter[] allFileFilters = {(FileFilter)(new BasicFastaFilter()),
            (FileFilter)(new FastaFilter()), (FileFilter)(new GeneiousFilter()),
            (FileFilter)(new RSFFilter())};

    private EditorWindow _window;
    public Settings settings;
    private static final long serialVersionUID = 1L;
    private int[] _offset = {0, 0};
    public int _mouseButton;
    private double _lw = 12;
    private int minLineHeight = 8;
    private int _width = 1024-40;
    private int _height = 768;
    private int[] _movePos = {0, 0};
    public int consHeight = 10;
    public int groupSep = 5;
    public int[] _lastMousePos = {0, 0};
    public int[] selFrom = {0,0};
    public int[] selTo = {0,0};
    public boolean selConsensus = false;
    public GroupKey consensusGroup = null;
//    public double[] conservation = null;
    public int mode = MODE_NORMAL;
    public Annotation editedAnnotation;
    public Annotation selectedAnnotation;
    private SequencePane _seqPane;
    private NamesPane _namesPane;
    private JSplitPane _sp;
    private JScrollBar _scrollBar = new JScrollBar(JScrollBar.VERTICAL);
    private JScrollBar _hscrollBar = new JScrollBar(JScrollBar.HORIZONTAL);
    private Alignment _align = new Alignment(settings);
    public SequenceInfo _toolTip = new SequenceInfo();
//    public JPopupMenu _toolTip = null;
//    public JMenuItem _toolTipText = null;
    public int rulerHeight = 17;
    public int annotationHeight = (int)_lw+4;
    // Attention! Must be multiple of 3!
    public int tripletDist = 6;
    private int _lastSelected = -1;
    private int[] _lastPos = {0,0};
    private String _lastSearch = "";
    private boolean _lastSearchAllowedGaps = false;
    public GroupKey baseGroup = new GroupKey("All");
    private String _filename = "";
    private String _fullFilename = "";
    private FileFilter _loadFilter = null;

    private LinkedList<int[]> _conservedRegions;
    private int _conservedRegionsIndex = 0;
    private boolean _selectionActive = false;

    public String baseUrl;
    private String fileId;
    private String username;

    private int _primerEditPos = 0;

    public boolean hasMasterSequence = false;
    public boolean hasGroupMasterSequences = false;

    public EditorApp() {
        super();
        init();
    }

    @Override
    public boolean hasMenu() {
        return true;
    }

    public void loadFile(final JFileChooser fc, boolean overwrite) {
        if(overwrite) {
            hasMasterSequence = false;
            hasGroupMasterSequences = false;
        }
        settings.set(Settings.LAST_LOAD_PATH, fc.getSelectedFile().getAbsolutePath());
        _fullFilename = fc.getSelectedFile().getAbsolutePath();
        _loadFilter = fc.getFileFilter();
        _window.setTitle(_filename);
        LoadFilter filter = (LoadFilter) (fc.getFileFilter());
        String filename = fc.getSelectedFile().getAbsolutePath();
        LinkedList<String> recentFiles = new LinkedList<String>();
        boolean addFile = true;
        for(String recentFile : settings.getStringArray(Settings.RECENT_FILES[0], null)) {
            recentFiles.add(recentFile);
            if(recentFile.split("\\|")[0].equals(filename)) {
                addFile = false;
                break;
            }
        }
        if(addFile) {
            recentFiles.addFirst(filename + "|" + filter.getClass().getName());
            while(recentFiles.size() > 10) {
                recentFiles.removeLast();
            }
            String[] recentFilesArr = {};
            settings.set(Settings.RECENT_FILES, recentFiles.toArray(recentFilesArr));
            resetRecentFiles();
        }
        loadFile(overwrite, filter, filename);
    }

    public void resetRecentFiles() {
        _window.recentMenu.removeAll();
        for(String recentFile : settings.getStringArray(Settings.RECENT_FILES[0], null)) {
            String[] data = recentFile.split("\\|");
            for(FileFilter filter : allFileFilters) {
                if(filter instanceof LoadFilter && filter.getClass().getName().equals(data[1])) {
                    JMenuItem jm = new RecentMenuItem(data[0], (LoadFilter)filter);
                    jm.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if(e.getSource() instanceof RecentMenuItem) {
                                RecentMenuItem mi = (RecentMenuItem)e.getSource();
                                loadFile(true, mi.loadFilter, mi.filename);
                            }
                        }
                    });
                    _window.recentMenu.add(jm);
                    break;
                }
            }
        }
    }

    public void loadFile(boolean overwrite, LoadFilter filter, String filename) {
        if (overwrite) {
            _align = new Alignment(settings);
        }
        filter.loadAlignment(filename, _align, baseGroup);
        _window.setTitle(_filename + "(" + _align.countAll() + " sequences)");
        _align.sortGroups();
        _namesPane.updateGroupMenuItems();
        _align.masterSequenceAll = null;
        _align.masterSequences = new HashMap<GroupKey, MasterSequence>();
        _seqPane.setParent(this);
        _seqPane.sequenceLoaded();
        scrollTo(0, 0);
        repaintAll();
    }

    @Override
    public boolean useIUB() {
        return settings.getBoolean(Settings.DISPLAY_IUB);
    }

    @Override
    public boolean showSequenceName() {
        return settings.getBoolean(Settings.DISPLAY_SEQUENCE_NAME);
    }

    public void groupIdenticalSequences() {
        _namesPane.groupIdenticalSequences();
    }

    public String readFromURL(String url) throws MalformedURLException, IOException {
        URL rkitools = new URL(url);
        URLConnection rc = rkitools.openConnection();
        BufferedReader rcr = new BufferedReader(new InputStreamReader(rc.getInputStream()));
        StringBuilder db = new StringBuilder();
        while(true) {
            if(!rcr.ready()) {
                try {
                    Thread.sleep(1000);
                } catch(Exception e) {}
                if(!rcr.ready()) {
                    break;
                }
            }
            db.append(rcr.readLine());
            db.append("\n");
        }
        String data = db.toString();
        return data;
    }

    private void loadFromRKITools(String url, String fileId, boolean overwrite) {
        try {
            String data = readFromURL(url + fileId + "/");
            loadString(data, overwrite);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isSelectionActive() {
        return _selectionActive;
    }

    @Override
    public String seqsToString(boolean pureFASTA) {
        StringBuilder datab = new StringBuilder();
        for(Sequence s : _align.getSequencesAll()) {
            datab.append(">");
            datab.append(s.name);
            if(!pureFASTA) {
                datab.append("%%");
                datab.append(s.group.getName());
                datab.append("%%");
                int anno_counter = 0;
                StringBuilder annotations = new StringBuilder();
                for(Annotation anno : s.above.getAnnotations()) {
                    anno_counter ++;
                }
            }
            datab.append("\n");
            datab.append(s.seq);
            datab.append("\n");
        }
        String data = datab.toString();
        return data;
    }

    @Override
    public void saveToRKITools(String name) {
        if(baseUrl == null || fileId == null) {
        }
        try {
            StringBuilder datab = new StringBuilder();
            for(Sequence s : _align.getSequencesAll()) {
                datab.append(">");
                datab.append(s.name);
                datab.append("%%");
                datab.append(s.group.getName());
                datab.append("%%\n");
                datab.append(s.seq);
                datab.append("\n");
            }
            String data = datab.toString();
            ClientHttpRequest req = null;
            if(name == null) {
                req = new ClientHttpRequest(baseUrl + "save/" + fileId + "/");
                req.setParameter("data", data);
            }
            else {
                req = new ClientHttpRequest(baseUrl + "save_as/" + fileId + "/");
                req.setParameter("data", data);
                req.setParameter("name", name);
            }
            InputStream is = req.post();
/*            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);
            String res = reader.readLine();
            if(name != null) {
                fileId = res.trim();
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveFile() {
        if(!_fullFilename.equals("") && _loadFilter != null && _loadFilter instanceof SaveFilter) {
            ((SaveFilter)_loadFilter).saveAlignment(_align, _fullFilename);
        }
    }

    @Override
    public void saveFileAs() {
        final JFileChooser fc = new JFileChooser(settings.get(Settings.LAST_SAVE_PATH));
        fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
        for(FileFilter filter : allFileFilters) {
            if(filter instanceof SaveFilter) {
                fc.addChoosableFileFilter(filter);
            }
        }
        int returnVal = fc.showOpenDialog(_window);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            settings.set(Settings.LAST_SAVE_PATH, fc.getSelectedFile().getAbsolutePath());
            String filename = fc.getSelectedFile().getAbsolutePath();
            SaveFilter chosenFilter = (SaveFilter)(fc.getFileFilter());
            if(!filename.endsWith(chosenFilter.getExtension())) {
                filename += chosenFilter.getExtension();
            }
            chosenFilter.saveAlignment(_align, filename);
            _window.setTitle(filename + "(" + _align.countAll() + " sequences)");
        }
    }

    private String getColorOpts() {
        String colorScheme = "UPGMA";
        try {
            colorScheme = readFromURL(baseUrl + "color_scheme/" + username + "/");
        } catch(Exception e) {
        }
        return colorScheme.replace("_", " ");
    }

    @Override
    public void setColorOpts(String opts) {
        try {
            readFromURL(baseUrl + "set_color_scheme/" + username + "/" + opts.replace(" ", "_") + "/");
        } catch(Exception e) {
        }
    }

    private void init() {
//        setPreferredSize(new Dimension(1024, 768));
//        pack();
        try {
            BUILD = rb.getString("BUILD").replace(",", "");
        } catch(Exception e) {}
        ToolTipManager.sharedInstance().setInitialDelay(0);
        settings = new Settings();
        _align.setSettings(settings);
        _window = new EditorWindow();
        _window.setApp(this);
        _window.setVisible(true);
        _window.namesPane1.setParent(this);
        _window.namesPane1.setOffset(_offset);
        _window.messageLabel.setText("Welcome to mPSQed " + VERSION + ", build " + BUILD);
        _namesPane = _window.namesPane1;
        _seqPane = _window.sequencePane1;
        _seqPane.setOffset(_offset);
        baseUrl = "";
        fileId = "";
        username = "";

        _window.addFocusListener(this);
        _window.addKeyListener(this);

        _seqPane.addMouseMotionListener(this);
        _seqPane.addMouseListener(this);
        _seqPane.addKeyListener(this);
        for (String name : ColorModels.models.keySet()) {
            JMenuItem jm = new ColorModelMenuItem(name);
            jm.addActionListener(_seqPane);
            _window.colorModelMenu.add(jm);
        }
        resetRecentFiles();
    }

    @Override
    public int getAppletWidth() {
        return(_width);
    }

    @Override
    public int getAppletHeight() {
        return(_height);
    }

    public void scrollHorizontal(int x) {
        scrollTo(x, _offset[1]);
    }

    public void scrollVertical(int y) {
        scrollTo(_offset[0], y);
    }

    public void updateScrollbar() {
        _window.verticalScrollBar.setMaximum(_align.countAll());
        _window.verticalScrollBar.setValue(_offset[1]);
        int max = _align.getLongestAll();
        _window.horizontalScrollBar.setMaximum(max);
        _window.horizontalScrollBar.setValue(_offset[0]);
        int extent = (int)(_seqPane.getWidth()/_lw);
        if(extent < max) {
            _window.horizontalScrollBar.setVisibleAmount(extent);
        }
        else {
            scrollTo(0, _offset[1]);
            _window.horizontalScrollBar.setVisibleAmount(max-1);
        }
    }

    @Override
    public double getLetterWidth() {
        return (_lw);
    }

    @Override
    public int getLetterHeight() {
        if(_lw >= minLineHeight) {
            return((int)_lw);
        }
        return(minLineHeight);
    }

    @Override
    public int getTopOffset() {
        if(_align.masterSequenceAll == null) {
            return(rulerHeight);
        }
        return(rulerHeight + getLetterHeight() + consHeight + 5);
    }

    @Override
    public int getConsOffset() {
        return(rulerHeight + getLetterHeight());
    }

    @Override
    public void setLetterWidth(int lw) {
        _lw = lw;
        annotationHeight = lw+4;
        repaintAll();
    }

    @Override
    public Frame getParentFrame(){
        return (Frame)null;
    }

    @Override
    public void setMasterSequence(MasterSequence masterSeq, GroupKey group) {
//        MasterSequence masterSeq = new MasterSequence(master);
        _align.masterSequences.put(group, masterSeq);
        int allseqs = _align.count(group);
        int len = masterSeq.seq.length();
        char[][] seqs = _align.toCharArray(len, group);
        calcMasterConservation(masterSeq, seqs, allseqs);
    }

    @Override
    public void setMasterSequenceAll(MasterSequence master) {
        _align.masterSequenceAll = master;
        int allseqs = _align.countAll();
        int len = _align.masterSequenceAll.seq.length();
        char[][] seqs = _align.toCharArrayAll(len);
        calcMasterConservation(_align.masterSequenceAll, seqs, allseqs);
        _seqPane.updateCopyMenues();
    }

    private void calcMasterConservation(MasterSequence master, char[][] seqs, int allseqs) {
        char[] masterSeq = master.seq.toCharArray();
        int len = master.seq.length();
        master.conservation = new double[len];
        _conservedRegions = new LinkedList<int[]>();
        int conslen = 0;
        for(int i=0; i<len; i++) {
            int nseq = allseqs;
            int ident = 0;
            for(int j=0; j<allseqs; j++) {
                if(seqs[j][i] == masterSeq[i]) {
                    ident ++;
                }
                if(seqs[j][i] == '\0') {
                    nseq --;
                }
            }
            master.conservation[i] = (double)ident/(double)nseq;
            if(master.conservation[i] == 1) {
                conslen ++;
            }
            else if(conslen != 0) {
                int[] place = {i-conslen, conslen};
                _conservedRegions.add(place);
                conslen = 0;
            }
        }
        repaintAll();
    }

    @Override
    public void getMasterConsensus() {
        int longest = _align.getLongestAll();
        int allseqs = _align.countAll();
        char[][] seqs = _align.toCharArrayAll(longest);
        MasterSequence masterSequence = calculateConsensusSequence(longest, allseqs, seqs);
        setMasterSequenceAll(masterSequence);
        _align.calculateHomology(masterSequence);
        hasMasterSequence = true;
    }

    @Override
    public void getConsensus(GroupKey group) {
        int longest = _align.getLongest(group);
        int allseqs = _align.count(group);
        char[][] seqs = _align.toCharArray(longest, group);
        MasterSequence masterSequence = calculateConsensusSequence(longest, allseqs, seqs);
        masterSequence.group = group;
        setMasterSequence(masterSequence, group);
        _align.calculateHomology(_align.masterSequences.get(group));
    }

    @Override
    public void getGroupMasterSequences() {
        Collection<GroupKey> groups = getAlignment().getGroups();
        for(GroupKey group : groups) {
            getConsensus(group);
        }
        hasGroupMasterSequences = true;
    }

    public void copyMasterConsensusSequence() {
        _seqPane.copyMasterConsensusSequence();
    }

    public void copyGroupConsensusSequence() {
        _seqPane.copyGroupConsensusSequence();
    }

    @Override
    public boolean isSequenceSelected() {
        if(getSelFrom()[0] == getSelTo()[0] || getSelFrom()[1] == getSelTo()[1])
            return false;
        return true;
    }

    @Override
    public boolean canAddAnnotation() {
        return isSequenceSelected() && Math.abs(getSelFrom()[1] - getSelTo()[1]) == 1;
    }

    /**
     * Finds SNPs in the alignment which can be used to differenciate between the defined sequence groups.
     */
    @Override
    public void findSNPs() {
        getMasterConsensus();
        getGroupMasterSequences();
        int ngroups = _align.masterSequences.size();
        for(int pos = 0; pos < getAlignment().getLongestAll(); pos ++) {
            // If the conservation of the master consensus sequence is 100%, then this can not be a SNP
            if(_align.masterSequenceAll.conservation[pos] == 1)
                continue;
            char val = _align.masterSequenceAll.seq.charAt(pos);
            int differ = 0;
            for(MasterSequence seq : _align.masterSequences.values()) {
                if(seq.conservation[pos] != 1) {
                    differ = -1;
                    break;
                }
                if(seq.seq.charAt(pos) != val)
                    differ ++;
            }
            if(differ != -1) {
                getAlignment().setSnp(pos);
            }
        }
    }

    private MasterSequence calculateConsensusSequence(int longest, int allseqs, char[][] seqs) {
        char[] bases = new char[5]; //0: A, 1: G, 2: T, 3: C, 4: -
        char[] consensus = new char[longest];
        char[] IUBconsensus = new char[longest];
        for(int i=0; i<longest; i++) {
            bases = new char[5];
            for(int j=0; j<allseqs; j++) {
                switch(seqs[j][i]) {
                    case 'A':
                        bases[0]++; break;
                    case 'G':
                        bases[1]++; break;
                    case 'T':
                        bases[2]++; break;
                    case 'C':
                        bases[3]++; break;
                    case '-':
                        bases[4]++; break;
                    default:
                }
            }
            if(bases[0]==0 && bases[1] == 0 && bases[2] == 0 && bases[3] == 0)
                IUBconsensus[i] = '-';
            else if(bases[0] != 0 && bases[1] == 0 && bases[2] == 0 && bases[3] == 0)
                IUBconsensus[i] = 'A';
            else if(bases[0] == 0 && bases[1] != 0 && bases[2] == 0 && bases[3] == 0)
                IUBconsensus[i] = 'G';
            else if(bases[0] == 0 && bases[1] == 0 && bases[2] != 0 && bases[3] == 0)
                IUBconsensus[i] = 'T';
            else if(bases[0] == 0 && bases[1] == 0 && bases[2] == 0 && bases[3] != 0)
                IUBconsensus[i] = 'C';
            else if(bases[0] != 0 && bases[1] != 0 && bases[2] == 0 && bases[3] == 0)
                IUBconsensus[i] = 'R';
            else if(bases[0] == 0 && bases[1] == 0 && bases[2] != 0 && bases[3] != 0)
                IUBconsensus[i] = 'Y';
            else if(bases[0] != 0 && bases[1] == 0 && bases[2] == 0 && bases[3] != 0)
                IUBconsensus[i] = 'M';
            else if(bases[0] == 0 && bases[1] != 0 && bases[2] != 0 && bases[3] == 0)
                IUBconsensus[i] = 'K';
            else if(bases[0] != 0 && bases[1] == 0 && bases[2] != 0 && bases[3] == 0)
                IUBconsensus[i] = 'W';
            else if(bases[0] == 0 && bases[1] != 0 && bases[2] == 0 && bases[3] != 0)
                IUBconsensus[i] = 'S';
            else if(bases[0] == 0 && bases[1] != 0 && bases[2] != 0 && bases[3] != 0)
                IUBconsensus[i] = 'B';
            else if(bases[0] != 0 && bases[1] != 0 && bases[2] != 0 && bases[3] == 0)
                IUBconsensus[i] = 'D';
            else if(bases[0] != 0 && bases[1] == 0 && bases[2] != 0 && bases[3] != 0)
                IUBconsensus[i] = 'H';
            else if(bases[0] != 0 && bases[1] != 0 && bases[2] == 0 && bases[3] != 0)
                IUBconsensus[i] = 'V';
            else if(bases[0] != 0 && bases[1] != 0 && bases[2] != 0 && bases[3] != 0)
                IUBconsensus[i] = 'N';

            int pos = 4;
            int max = 0;
            for(int n=0; n<4; n++) {
                if(bases[n] > max) {
                    pos = n;
                    max = bases[n];
                }
                bases[n] = 0;
            }
            switch(pos) {
                case 0:
                    consensus[i] = 'A'; break;
                case 1:
                    consensus[i] = 'G'; break;
                case 2:
                    consensus[i] = 'T'; break;
                case 3:
                    consensus[i] = 'C'; break;
                case 4:
                    consensus[i] = '-'; break;
                default:
                    consensus[i] = '-';
            }
        }
        MasterSequence res = new MasterSequence("Consensus (" + allseqs + " sequences)", new String(consensus), baseGroup, _align);
        res.IUB = new Sequence("IUB Consensus", new String(IUBconsensus), baseGroup, _align);
        return(res);
    }

    @Override
    public int getPrimerEditPos() {
        return _primerEditPos;
    }

    @Override
    public Alignment getAlignment() {
        return (_align);
    }

    @Override
    public HashMap<GroupKey, MasterSequence> getMasterSequences() {
        return _align.masterSequences;
    }

    @Override
    public MasterSequence getMasterSequenceAll() {
        return _align.masterSequenceAll;
    }

    @Override
    public void repaintAll() {
        _seqPane.redrawAll();
        _seqPane.repaint();
        _namesPane.repaint();
    }

    private void setSelectionResize(boolean setMode, boolean setCursor, int[] point) {
        Cursor c = new Cursor(Cursor.DEFAULT_CURSOR);
        int newmode = MODE_MOVESEL;
        if(point[0] <= getScreenX(selFrom[0])+SELECTION_RESIZE_BORDER) {
            c = new Cursor(Cursor.W_RESIZE_CURSOR);
            newmode = MODE_RESIZESEL_W;
        }
        else if(point[0] >= getScreenX(selTo[0])-SELECTION_RESIZE_BORDER) {
            c = new Cursor(Cursor.E_RESIZE_CURSOR);
            newmode = MODE_RESIZESEL_E;
        }
        else if(point[1] <= getScreenY(selFrom[1]-1)+SELECTION_RESIZE_BORDER) {
            c = new Cursor(Cursor.N_RESIZE_CURSOR);
            newmode = MODE_RESIZESEL_N;
        }
        else if(point[1] >= getScreenY(selTo[1]-1)-SELECTION_RESIZE_BORDER) {
            c = new Cursor(Cursor.S_RESIZE_CURSOR);
            newmode = MODE_RESIZESEL_S;
        }
        if(setMode)
            mode = newmode;
        if(setCursor)
            _window.setCursor(c);
    }

    private boolean pointInSelection(int[] point) {
        Position rpoint = getRealPos(point);
        Rectangle r = new Rectangle(selFrom[0], selFrom[1], selTo[0]-selFrom[0], selTo[1]-selFrom[1]);
        Point p = new Point(rpoint.x, rpoint.y);
        if(r.contains(p)) {
            return(true);
        }
        return(false);
    }

    @Override
    public int getRulerHeight() {
        return rulerHeight;
    }

    @Override
    public int[] getSelFrom() {
        return selFrom;
    }

    @Override
    public int[] getSelTo() {
        return selTo;
    }

    @Override
    public int getMode() {
        return mode;
    }

    @Override
    public int getConsHeight() {
        return consHeight;
    }

    @Override
    public int getTripletDist() {
        return tripletDist;
    }

    @Override
    public int getAnnotationHeight() {
        return annotationHeight;
    }

    @Override
    public Annotation getEditedAnnotation() {
        return editedAnnotation;
    }

    @Override
    public void setEditedAnnotation(Annotation anno) {
        editedAnnotation = anno;
    }

    @Override
    public void setSelFrom(int index, int value) {
        selFrom[index] = value;
    }

    @Override
    public void setSelTo(int index, int value) {
        selTo[index] = value;
    }

    @Override
    public int getScreenX(int posx) {
        double res = (posx-_offset[0])*_lw;
        if(getAlignment().tripletGrouping != -1) {
            res += (Math.ceil((double)(posx-getAlignment().tripletGrouping)/3.0))*tripletDist;
        }
        return((int)res);
    }

    public int getScreenY(int posy, boolean consensus) {
        int res = getTopOffset();
        int lh = getLetterHeight();
        res += lh;
        for(int i=_offset[1]; i<=posy; i++) {
            Sequence lastSeq = getAlignment().getSequence(i-1);
            Sequence seq = getAlignment().getSequence(i);
            if(seq == null) {
                break;
            }
            if(lastSeq != null && lastSeq.group == seq.group && seq.collapsed) {
                continue;
            }
            if(lastSeq != null) {
                res += lastSeq.below.levels.size()*annotationHeight;
                if(lastSeq.group != seq.group) {
                    res += groupSep;
                    if(_align.masterSequences.containsKey(seq.group)) {
                        res += getLetterHeight();
                        res += getConsOffset();
                    }
                }
            }
            else if(_align.masterSequences.containsKey(seq.group)) {
                res += getLetterHeight();
                res += getConsOffset();
            }
            res += lh;
            if(consensus && i == posy) {
                return res;
            }
            res += seq.above.levels.size()*annotationHeight;
        }
        return(res);
    }

    @Override
    public int getScreenY(int posy) {
        return getScreenY(posy, false);
    }

    @Override
    public int getScreenYConsensus(int posy) {
        return getScreenY(posy, true);
    }

    @Override
    public int getOffsetY() {
        return(getScreenY(_offset[1]));
    }

    private Annotation onAnnotation(int posX, int posY, LinkedList<LinkedList<Annotation>> levels, int ypos) {
        ypos -= levels.size()*annotationHeight;
        Annotation found = null;
        for(LinkedList<Annotation> level : levels) {
            ypos += annotationHeight;
            if(posY <= ypos && posY >= ypos - annotationHeight) {
                for(Annotation anno : level) {
                    int[] axp = _seqPane.getAnnotationXpos(anno);
                    if(posX > axp[0] && posX < axp[1]) {
                        found = anno;
                        break;
                    }
                }
                if(found != null) {
                    break;
                }
            }
        }
        return(found);
    }

    @Override
    public Position getRealPos(int[] screenPos) {
        int[] res = {0, 0};
        int pos = getLetterHeight();
        int ypos;
        Annotation onAnnotation = null;
        int lh = getLetterHeight();
        boolean onCollapser = false;
        boolean onConsensus = false;
        boolean first = true;
        GroupKey lastGroup = new GroupKey("__NONAME__");
        GroupKey onConsensusGroup = null;
        int numSequences = _align.countAll();
        for(ypos=_offset[1]; pos < screenPos[1] - getTopOffset() && ypos < numSequences; ypos++) {
            first = false;
            if(_align.getSequence(ypos-1) != null) {
                lastGroup = _align.getSequence(ypos-1).group;
            }
            onConsensus = false;
            onCollapser = false;
            Sequence seq = _align.getSequence(ypos);
            Sequence nextSeq = _align.getSequence(ypos + 1);
            if(nextSeq != null && seq.group != nextSeq.group) {
                pos += groupSep;
            }
            if(lastGroup != seq.group) {
                if(_align.masterSequences.containsKey(seq.group)) {
                    pos += getLetterHeight();
                    pos += getConsOffset();
                }
                if(pos >= screenPos[1] - getTopOffset()) {
                    onConsensus = true;
                    onConsensusGroup = seq.group;
                }
            }
            if(lastGroup == seq.group && seq.collapsed) {
                continue;
            }
            if(lastGroup != seq.group && screenPos[0] <= 12) {
                onCollapser = true;
            }
            boolean hit = false;
            if(seq == null) {
                ypos --;
                break;
            }
            pos += seq.above.levels.size()*annotationHeight;
            if(pos >= screenPos[1] - getTopOffset()) {
                onAnnotation = onAnnotation(screenPos[0], screenPos[1] - getTopOffset(), seq.above.levels, pos);
                hit = true;
            }
            pos += lh;
            if(pos >= screenPos[1] - getTopOffset()) {
                hit = true;
            }
            pos += seq.below.levels.size()*annotationHeight;
            if(!hit && pos >= screenPos[1] - getTopOffset()) {
                onAnnotation = onAnnotation(screenPos[0], screenPos[1] - getTopOffset(), seq.below.levels, pos);
            }
            if(pos >= screenPos[1] - getTopOffset()) {
                break;
            }
        }
        res[1] = ypos;
        if(_align.tripletGrouping == -1) {
            res[0] = (int)(screenPos[0]/_lw) + _offset[0];
        }
        else {
            res[0] = (int)(((screenPos[0] + (_offset[0]*_lw) + ((_align.tripletGrouping + _offset[0])%3)*tripletDist/3))/(_lw+tripletDist/3));
        }
        if(first) {
            onConsensus = true;
        }
        return (new Position(res[0], res[1], onAnnotation!=null, onAnnotation, onCollapser, onConsensus, onConsensusGroup));
    }

    @Override
    public void addAnnotation(boolean above, boolean primer) {
        String text = primer?"Primer":"Annotation";
        _align.getSequence(selFrom[1]).addAnnotation(above, selFrom[0], selTo[0], text, primer);
        repaintAll();
    }

    @Override
    public boolean isPrimerSequenceActive() {
        return !settings.getBoolean(Settings.DISPLAY_PRIMER_NAME);
    }

    @Override
    public boolean isPrimerTmEnabled() {
        return settings.getBoolean(Settings.DISPLAY_PRIMER_TEMPERATURE);
    }

    @Override
    public boolean isPrimerLengthEnabled() {
        return settings.getBoolean(Settings.DISPLAY_PRIMER_LENGTH);
    }


    private void unselectAll() {
        for (Sequence seq : _align.getSequencesAll()) {
            seq.selected = false;
        }
    }

    private void scrollSequences(MouseEvent e) {
        boolean rep = false;
        int newx = _offset[0];
        int newy = _offset[1];
        if (Math.abs(_lastMousePos[0] - e.getX()) >= _lw && e.getSource() == _seqPane) {
            newx = (int)(_offset[0] + (_lastMousePos[0] - e.getX()) / _lw);
            _lastMousePos[0] = e.getX();
            if (newx < 0) {
                newx = 0;
            }
            rep = true;
        }
        if (Math.abs(_lastMousePos[1] - e.getY()) >= getLetterHeight()) {
            newy = _offset[1] + (_lastMousePos[1] - e.getY()) / getLetterHeight();
            _lastMousePos[1] = e.getY() - (_lastMousePos[1] - e.getY()) % 8;
            if (newy < 0) {
                newy = 0;
            }
            rep = true;
        }
        if (rep) {
            scrollTo(newx, newy);
//            repaintAll();
        }
    }

    private void selectNames(MouseEvent e) {
        int[] pos = {e.getX(), e.getY()};
        int from = getRealPos(_lastMousePos).y;
        int to = getRealPos(pos).y;
        if (!e.isShiftDown()) {
            unselectAll();
        }
        if (from > to) {
            int tmp = to;
            to = from;
            from = tmp;
        }
        for (int i = from; i <= to && i < _align.countAll(); i++) {
            _align.getSequence(i).selected = true;
        }
        repaintAll();
    }

    private void moveSel(int pos0, int pos1) {
        // Moving disabled until undo is implemented
/*        for(int i=selFrom[1]; i<selTo[1]; i++) {
            _align.getSequence(i).movePart(selFrom[0], selTo[0], pos1-pos0);
        }*/
    }

    public void loadString(String s, boolean overwrite) {
        Alignment align = _align;
        if(overwrite) {
            align = new Alignment(settings);
        }
        String[] lines = s.split(">");
        HashMap<String, GroupKey> loadedGroups = new HashMap<String, GroupKey>();
        if(!overwrite) {
            for(GroupKey key : _align.getGroups()) {
                if(!loadedGroups.containsKey(key.getName())) {
                    loadedGroups.put(key.getName(), key);
                }
            }
        }
        for(String line : lines) {
            if(line.equals("")) continue;
            String[] vals = line.split("\n");
            String name = vals[0];
            vals[0] = "";
            StringBuilder seqBuilder = new StringBuilder();
            for(String val : vals) {
                seqBuilder.append(val.replace("\r", "").replace(" ", ""));
            }
            String seq = seqBuilder.toString().toUpperCase();
            GroupKey group = baseGroup;
            if(loadedGroups.containsKey(baseGroup.getName())) {
                group = loadedGroups.get(baseGroup.getName());
            }
            // Get the name of the group the sequence belongs to from the name
            if(name.contains("%%")) {
                String[] nameVals = name.split("%%");
                name = nameVals[0];
                String groupName = nameVals[1];
                if(loadedGroups.containsKey(groupName)) {
                    group = loadedGroups.get(groupName);
                }
                else {
                    group = new GroupKey(groupName);
                    loadedGroups.put(groupName, group);
                }
            }
            align.addSequence(new Sequence(name, seq, group, _align));
        }
        if(overwrite) {
            _align = align;
        }
        _window.setTitle(_filename + "(" + _align.countAll() + " sequences)");
        _align.sortGroups();
        _namesPane.updateGroupMenuItems();
        repaintAll();
    }

    private void parseTree(JSONArray data, DefaultMutableTreeNode parent) {
        try {
            for(int i = 0; i < data.length(); i ++) {
                JSONObject cdata = data.getJSONObject(i);
                RKIFolder folder = new RKIFolder(cdata.getJSONObject("property").getString("name"), cdata.getJSONObject("property").getString("data"));
                DefaultMutableTreeNode child = new DefaultMutableTreeNode(folder);
                try {
                    parseTree(cdata.getJSONArray("children"), child);
                } catch(Exception e) {}
                parent.add(child);
            }
        } catch(Exception e) {
        }
    }

    private DefaultMutableTreeNode parseTree(JSONArray data) {
        try {
            JSONObject root = data.getJSONObject(0);
            RKIFolder folder = new RKIFolder(root.getJSONObject("property").getString("name"), root.getJSONObject("property").getString("data"));
            DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(folder);
            parseTree(root.getJSONArray("children"), rootNode);
            return rootNode;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean loadFileRKI(boolean overwrite) {
        return false;
    }

    @Override
    public void loadFile(boolean overwrite) {
        final JFileChooser fc = new JFileChooser(settings.get(Settings.LAST_LOAD_PATH));
        fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
        for(FileFilter filter : allFileFilters) {
            if(filter instanceof LoadFilter) {
                fc.addChoosableFileFilter(filter);
            }
        }
        int returnVal = fc.showOpenDialog(_window);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            _filename = fc.getSelectedFile().getName();
            loadFile(fc, overwrite);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (_mouseButton == 2) {
            scrollSequences(e);
        }
        else if (_mouseButton == 1 && e.getSource() == _namesPane) {
            if(mode == MODE_MOVESEL) {
                int[] screenPos = {e.getX(), e.getY()};
                Position newPos = getRealPos(screenPos);
                if(newPos.y != _movePos[1]) {
                    int dist = newPos.y - _movePos[1];
                    _movePos[1] = newPos.y;
                    _align.moveSelected(dist);
                    repaintAll();
                }
            }
//            selectNames(e);
        }
        else if (_mouseButton == 1 && e.getSource() == _seqPane && editedAnnotation == null) {
            int[] screenPos = {e.getX(), e.getY()};
            if(mode == MODE_MOVESEL) {
                Position newPos = getRealPos(screenPos);
                if(newPos.x != _movePos[0]) {
                    int diff = newPos.x - _movePos[0];
                    if(selFrom[0] + diff >= 0 && selTo[0] + diff <= _align.getLongestAll()) {
                        selFrom[0] += diff;
                        selTo[0] += diff;
                    }
                    _movePos[0] = newPos.x;
                    repaintAll();
                }
                if(newPos.y != _movePos[1]) {
                    int diff = newPos.y - _movePos[1];
                    if(selFrom[1] + diff >= 0 && selTo[1] + diff <= _align.countAll()) {
                        selFrom[1] += diff;
                        selTo[1] += diff;
                    }
                    _movePos[1] = newPos.y;
                    repaintAll();
                }
            }
            else if(mode == MODE_RESIZESEL_E || mode == MODE_RESIZESEL_W || mode == MODE_RESIZESEL_N || mode == MODE_RESIZESEL_S) {
                Position newPos = getRealPos(screenPos);
                int diffx = newPos.x - _movePos[0];
                int diffy = newPos.y - _movePos[1];
                if(diffx != 0) {
                    if(mode == MODE_RESIZESEL_E) {
                        selTo[0] += diffx;
                    }
                    if(mode == MODE_RESIZESEL_W) {
                        selFrom[0] += diffx;
                    }
                    _movePos[0] = newPos.x;
                    _seqPane.repaint();
                }
                if(diffy != 0) {
                    if(mode == MODE_RESIZESEL_S) {
                        selTo[1] += diffy;
                    }
                    if(mode == MODE_RESIZESEL_N) {
                        selFrom[1] += diffy;
                    }
                    _movePos[1] = newPos.y;
                    _seqPane.repaint();
                }
            }
            else if(mode == MODE_MOVEANNO || mode == MODE_RESIZEANNO_E || mode == MODE_RESIZEANNO_W) {
                Position newPos = getRealPos(screenPos);
                int diff = newPos.x - _movePos[0];
                if(diff != 0) {
                    if(mode == MODE_RESIZEANNO_E || mode == MODE_MOVEANNO) {
                        selectedAnnotation.to += diff;
                    }
                    if(mode == MODE_RESIZEANNO_W || mode == MODE_MOVEANNO) {
                        selectedAnnotation.from += diff;
                    }
                    _movePos[0] = newPos.x;
                    if(selectedAnnotation.from < 0)
                        selectedAnnotation.from = 0;
                    if(selectedAnnotation.to >= _align.getLongestAll())
                        selectedAnnotation.from = _align.getLongestAll()-selectedAnnotation.from-1;
                    if(selectedAnnotation instanceof Primer) {
                        ((Primer)(selectedAnnotation)).updateScores();
                        selectedAnnotation.list.sequence.updateProducts();
                    }
                    _seqPane.repaint();
                    updateFlowgrams(false);
                }
            }
            else {
                int[] oldTo = {selTo[0], selTo[1]};
                Position p = getRealPos(screenPos);
                selTo[0] = p.x;
                if(selConsensus) {
                    if(consensusGroup == null) {
                        selFrom[1]=0;
                        selTo[1] = _align.countAll();
                    }
                    else {
                        selFrom[1] = _align.getGroupFirst(consensusGroup);
                        selTo[1] = selFrom[1] + _align.count(consensusGroup);
                    }
                }
                else {
                    selTo[1] = p.y+1;
                    if(selTo[1] == selFrom[1]) {
                        selFrom[1] -= 1;
                    }
                }
                if(oldTo[0] != selTo[0] || oldTo[1] != selTo[1]) {
                    repaintAll();
                }
            }
        }
    }

    public SequenceInfo getToolTip() {
        if(_toolTip == null) {
            _toolTip = new SequenceInfo();
            _toolTip.text = "Test";
        }
        return(_toolTip);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if(mode != MODE_EDITING) {
            int[] screenPos = {e.getX(), e.getY()};
            Position realPos = getRealPos(screenPos);
            if(realPos.x != _lastPos[0] || realPos.y != _lastPos[1]) {
                _seqPane.redrawCursor(realPos.x, realPos.y);
                _seqPane.repaint();
                _lastPos[0] = realPos.x;
                _lastPos[1] = realPos.y;
            }
            if(e.getSource() == _seqPane || e.getSource() == _namesPane) {
                _seqPane.seqNum = realPos.y;
                if(realPos.onConsensus) {
                    if(_align.getSequence(realPos.y) == null) {
                        return;
                    }
                    _seqPane.toolTipText = "Consensus for " + _align.getSequence(realPos.y).group.getName();
                    _window.messageLabel.setText(_seqPane.toolTipText);
                }
                else {
                    if(_align.getSequence(realPos.y) == null) {
                        return;
                    }
                    _seqPane.toolTipText = _align.getSequence(realPos.y).name;
                    _window.messageLabel.setText(_seqPane.toolTipText);
                }
                if(realPos.onAnnotation) {
                    if(realPos.x < realPos.annotation.from+1) {
                        _window.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
                    }
                    else if(realPos.x >= realPos.annotation.to-1) {
                        _window.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                    }
                    else {
                        _window.setCursor(new Cursor(Cursor.TEXT_CURSOR));
                    }
                    _selectionActive = false;
                }
                else if(pointInSelection(screenPos)) {
                    setSelectionResize(false, true, screenPos);
                    _selectionActive = true;
                }
                else {
                    _selectionActive = false;
                    _window.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }
        }
        if(e.getSource() != _seqPane) {
            getToolTip().setVisible(false);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getButton() == 3) {
            return;
        }
        if (e.getSource() == _namesPane) {
            if (!e.isShiftDown() && !e.isControlDown()) {
                unselectAll();
            }
            int[] pos = {e.getX(), e.getY()};
            Position realPos = getRealPos(pos);
            if(realPos.onCollapser) {
                Sequence selectedSeq = _align.getSequence(realPos.y);
                selectedSeq.collapsed = !selectedSeq.collapsed;
                for(Sequence seq : _align.getSequences(selectedSeq.group)) {
                    seq.collapsed = selectedSeq.collapsed;
                }
            }
            else {
                if(!e.isShiftDown() || _lastSelected == -1) {
                    if(_align.getSequence(realPos.y) == null)
                        return;
                    _align.getSequence(realPos.y).selected ^= true;
                    if(_align.getSequence(realPos.y).selected) {
                        _lastSelected = realPos.y;
                    }
                }
                else {
                    if(_lastSelected != -1) {
                        for(int i=Math.min(_lastSelected, realPos.y); i <= Math.max(_lastSelected, realPos.y); i++) {
                            _align.getSequence(i).selected = true;
                        }
                    }
                }
            }
            repaintAll();
        }
        else {
            int[] coords = {e.getX(), e.getY()};
            Position pos = getRealPos(coords);
            if(pos.onAnnotation) {
                editedAnnotation = pos.annotation;
                if(editedAnnotation instanceof Primer && isPrimerSequenceActive()) {
                    _primerEditPos = pos.x - editedAnnotation.from;
                }
            }
            else if(editedAnnotation == null) {
                if(mode == MODE_SELECTING) {
                    mode = MODE_EDITING;
                }
                else {
                    mode = MODE_NORMAL;
                }
                selFrom[0] = 0;
                selFrom[1] = 0;
                selTo[0] = 0;
                selTo[1] = 0;
            }
            _seqPane.repaint();
        }
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        getToolTip().setVisible(false);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        _mouseButton = e.getButton();
        _lastMousePos[0] = e.getX();
        _lastMousePos[1] = e.getY();
        if(_mouseButton == MouseEvent.BUTTON2) {
            _window.setCursor(new Cursor(Cursor.MOVE_CURSOR));
        }
        else if(_mouseButton == MouseEvent.BUTTON1 && e.getSource() == _namesPane) {
            int[] screenPos = {e.getX(), e.getY()};
            Position realPos = getRealPos(screenPos);
            if(_align.getSequence(realPos.y) == null)
                return;
            if(_align.getSequence(realPos.y).selected) {
                _window.setCursor(new Cursor(Cursor.MOVE_CURSOR));
                mode = MODE_MOVESEL;
                _movePos[0] = getRealPos(screenPos).x;
                _movePos[1] = getRealPos(screenPos).y;
            }
        }
        else if(_mouseButton == MouseEvent.BUTTON1 && e.getSource() == _seqPane && editedAnnotation == null) {
            int[] screenPos = {e.getX(), e.getY()};
            Position realPos = getRealPos(screenPos);
            if(pointInSelection(screenPos)) {
                setSelectionResize(true, false, screenPos);
//                mode = MODE_MOVESEL;
                _movePos[0] = getRealPos(screenPos).x;
                _movePos[1] = getRealPos(screenPos).y;
                _window.setCursor(new Cursor(Cursor.MOVE_CURSOR));
                editedAnnotation = null;
            }
            else if(realPos.onAnnotation) {
                if(realPos.x < realPos.annotation.from+1) {
                    mode = MODE_RESIZEANNO_W;
                }
                else if(realPos.x >= realPos.annotation.to-1) {
                    mode = MODE_RESIZEANNO_E;
                }
                else {
                    mode = MODE_MOVEANNO;
                    _window.setCursor(new Cursor(Cursor.MOVE_CURSOR));
                }
                editedAnnotation = null;
                _movePos[0] = getRealPos(screenPos).x;
                _movePos[1] = getRealPos(screenPos).y;
                selectedAnnotation = realPos.annotation;
            }
            else {
                if(mode == MODE_EDITING) {
                    mode = MODE_SELECTING2;
                    editedAnnotation = null;
                }
                else {
                    mode = MODE_SELECTING;
                    editedAnnotation = null;
                }
                Position pos = getRealPos(screenPos);
                selFrom = pos.getCoordinates();
                selTo = pos.getCoordinates();
                selConsensus = pos.onConsensus;
                consensusGroup = pos.consensusGroup;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        _window.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if(mode == MODE_MOVEANNO || mode == MODE_RESIZEANNO_E || mode == MODE_RESIZEANNO_W) {
            if(selectedAnnotation != null) {
                selectedAnnotation.list.resetAnnotations();
                selectedAnnotation.list.sequence.updateProducts();
                selectedAnnotation = null;
                repaintAll();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if(mode!=MODE_EDITING) {
            if(e.getKeyChar() == '-') {
                doZoom(1);
            }
            else if(e.getKeyChar() == '+') {
                doZoom(-1);
            }
        }
    }

    public void scrollTo(int x, int y) {
        if(_offset[0] == x && _offset[1] == y) {
            repaintAll();
            return;
        }
        _offset[0] = x;
        _offset[1] = y;
        updateScrollbar();
        repaintAll();
//        _hscrollBar.setValue(x);
//        _scrollBar.setValue(y);
    }

    public void searchForSequence(String seq, boolean allowGaps) {
        int[] pos = _align.findSubSequence(selFrom[1], selTo[0], seq, allowGaps);
        if(pos[0] != -1) {
            selFrom[1] = pos[0];
            selTo[1] = pos[0]+1;
            selFrom[0] = pos[1];
            selTo[0] = pos[1] + pos[2];
            scrollTo(selFrom[0], selFrom[1]);
            repaintAll();
        }
        else if(selFrom[0] == 0 && selTo[0] == 0 && selFrom[1] == 0 && selTo[1] == 0) {
            JOptionPane.showMessageDialog(null, "Sequence not found.", "Sequence not found", JOptionPane.WARNING_MESSAGE);
        }
        else {
            String[] opts = {"Yes", "No"};
            int res = JOptionPane.showOptionDialog(null, "Sequence not found. Start again from beginning?", "Sequence not found", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opts, opts[0]);
            if(res==0) {
                selFrom[0] = 0;
                selFrom[1] = 0;
                selTo[0] = 0;
                selTo[1] = 0;
                repaintAll();
                searchForSequence(seq, allowGaps);
            }
        }
    }

    public void copySequence(boolean withName) {
        _seqPane.copySequences(withName);
    }

    private Frame findParentFrame() {
        return((Frame)null);
    }


    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_F && (e.getModifiers() & KeyEvent.CTRL_MASK) != 0 ) {
            FindDialog d = new FindDialog(findParentFrame());
            d.setText(_lastSearch);
            d.allowGaps(_lastSearchAllowedGaps);
            d.setVisible(true);
            if(d.isAccepted()) {
                _lastSearch = Sequence.cleanUpSequence(d.getSeq());
                _lastSearchAllowedGaps = d.allowGaps();
                mode = MODE_NORMAL;
                searchForSequence(_lastSearch, _lastSearchAllowedGaps);
            }
        }
        else if(key == KeyEvent.VK_C && (e.getModifiers() & KeyEvent.CTRL_MASK) != 0) {
            Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
            StringBuilder seqStringBuilder = new StringBuilder();
            for(int i=getSelFrom()[1]; i<getSelTo()[1]; i++) {
                Sequence seq = getAlignment().getSequence(i);
                seqStringBuilder.append(seq.seq.substring(getSelFrom()[0], getSelTo()[0]));
                seqStringBuilder.append("\n");
            }
            StringSelection contents = new StringSelection(seqStringBuilder.toString());
            clip.setContents(contents, null);

        }
        else if(key == KeyEvent.VK_F3) {
            if(!_lastSearch.equals("")) {
                mode = MODE_NORMAL;
                searchForSequence(_lastSearch, _lastSearchAllowedGaps);
            }
        }
        else if(mode == MODE_EDITING) {
            boolean seqChanged = false;
            if(key == KeyEvent.VK_LEFT) {
                _lastPos[0] -= 1;
            }
            else if(key == KeyEvent.VK_RIGHT) {
                _lastPos[0] += 1;
            }
            else if(key == KeyEvent.VK_UP && _lastPos[1] >= 1) {
                _lastPos[1] -= 1;
            }
            else if(key == KeyEvent.VK_DOWN && _lastPos[1] < _align.countAll()-1) {
                _lastPos[1] += 1;
            }
            else if(charIsInAlphabet(e.getKeyChar())) {
                _align.getSequence(_lastPos[1]).setCharAt(_lastPos[0], Character.toUpperCase(e.getKeyChar()));
                _lastPos[0] += 1;
                seqChanged = true;
            }
            consensusChange();
            _seqPane.redrawCursor(_lastPos[0], _lastPos[1]);
            _seqPane.redrawAll();
            _seqPane.repaint();
        }
        else if(editedAnnotation instanceof Primer && editedAnnotation != null) {
            Primer primer = (Primer)editedAnnotation;
            char k = e.getKeyChar();
            if(key == KeyEvent.VK_LEFT && _primerEditPos != 0) {
                _primerEditPos -= 1;
            }
            else if(key == KeyEvent.VK_RIGHT && _primerEditPos+1 < primer.getSequence().length()) {
                _primerEditPos += 1;
            }
            else if(charIsInAlphabet(k)) {
                primer.lockBase(_primerEditPos, Character.toUpperCase(e.getKeyChar()));
                if(_primerEditPos+1 < primer.getSequence().length())
                    _primerEditPos += 1;
                primer.updateScores();
            }
            repaintAll();
        }
        else if(key == KeyEvent.VK_N) {
            if(_conservedRegionsIndex < _conservedRegions.size() - 1) {
                _conservedRegionsIndex += 1;
                scrollTo(_conservedRegions.get(_conservedRegionsIndex)[0], _offset[1]);
                selFrom[0] = _offset[0];
                selFrom[1] = 0;
                selTo[0] = selFrom[0] + _conservedRegions.get(_conservedRegionsIndex)[1];
                selTo[1] = _align.countAll();
                repaintAll();
            }
        }
    }

    @Override
    public void consensusChange() {
        if(_window.keepConsensusUpdated.isSelected()) {
            if(hasMasterSequence)
                getMasterConsensus();
            if(hasGroupMasterSequences)
                getGroupMasterSequences();
        }
    }

    private boolean charIsInAlphabet(char oc) {
        char c = Character.toLowerCase(oc);
        if(c == 'a' || c == 'g' || c == 't' || c == 'c' || c == '-' || c == 'r' || c == 'y' ||
                c == 'm' || c == 'k' || c == 'w' || c == 's' || c == 'b' || c == 'd' ||
                c == 'h' || c == 'v' || c == 'n') {
            return true;
        }
        return false;
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
    }

    @Override
    public void focusGained(FocusEvent arg0) {
    }

    @Override
    public void focusLost(FocusEvent arg0) {
    }

    public void doZoom(int direction) {
        if(direction < 0) {
            if(_lw == 10 || _lw == 8) {
                _lw += 2;
            }
            else if(_lw < 8) {
                _lw *= 2;
            }
        }
        else if(direction > 0) {
            if(_lw == 12 || _lw == 10) {
                _lw -= 2;
            }
            else if(_lw <= 8) {
                _lw /= 2;
            }
        }
        updateScrollbar();
        repaintAll();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        int notches = e.getWheelRotation();
        if(e.isControlDown()) {
            doZoom(notches);
        }
        else if(e.isShiftDown()) {
            if(_offset[0] + notches >= 0 && _offset[0] + notches < _align.getLongestAll())
                scrollHorizontal(_offset[0] + notches);
        }
        else {
            if(_offset[1] + notches >= 0 && _offset[1] + notches < _align.countAll())
                scrollVertical(_offset[1] + notches);
        }
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {
        if(e.getSource() == _scrollBar) {
            scrollTo(_offset[0], e.getValue());
        }
        else if(e.getSource() == _hscrollBar) {
            scrollTo(e.getValue(), _offset[1]);
        }
        repaintAll();
    }

    @Override
    public boolean isApplication() {
        return true;
    }

    public void settingsClicked() {
        new OptionsFrame(this).setVisible(true);
    }

    public void updatePrimerTMs() {
        for(Sequence seq : _align.getSequencesAll()) {
            for(Annotation anno : seq.above.getAnnotations()) {
                if(anno instanceof Primer)
                    ((Primer)anno).updateTm();
            }
            for(Annotation anno : seq.below.getAnnotations()) {
                if(anno instanceof Primer)
                    ((Primer)anno).updateTm();
            }
        }
        repaintAll();
    }

    @Override
    public Settings getSettings() {
        return settings;
    }

    @Override
    public void updateFlowgrams(boolean fromWindow) {
        _seqPane.updateFlowgrams(fromWindow);
    }
}
