package org.rki.sequenceeditor.view;

import java.applet.Applet;
import java.awt.event.AdjustmentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import org.rki.sequenceeditor.*;
import org.rki.sequenceeditor.model.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.LinkedList;
import javax.swing.JFileChooser;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.util.HashMap;
import javax.swing.JScrollBar;
import javax.swing.tree.DefaultMutableTreeNode;
import org.rki.sequenceeditor.model.filters.FastaFilter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.rki.sequenceeditor.model.filters.LoadFilter;

public class Editor extends Applet implements EditorInterface {
    
    public static String VERSION = "0.9.2";
    public static int MODE_NORMAL = 0;
    public static int MODE_EDITING = 1;
    public static int MODE_MOVESEL = 2;
    public static int MODE_MOVEANNO = 3;
    public static int MODE_SELECTING = 4;
    public static int MODE_SELECTING2 = 5;
    
    private static final long serialVersionUID = 1L;
    private int[] _offset = {0, 0};
    public int _mouseButton;
    private double _lw = 12;
    private int minLineHeight = 8;
    private int _width = 1024-40;
    private int _height = 768;
    private int _movePos = 0;
    public int consHeight = 10;
    public int groupSep = 5;
    public int[] _lastMousePos = {0, 0};
    public int[] selFrom = {0,0};
    public int[] selTo = {0,0};
    public boolean selConsensus = false;
    public GroupKey consensusGroup = null;
//    public double[] conservation = null;
    public int mode = MODE_NORMAL;
    public MasterSequence masterSequenceAll = null;
    public HashMap<GroupKey, MasterSequence> masterSequences = new HashMap<GroupKey, MasterSequence>();
    public Annotation editedAnnotation;
    public Annotation selectedAnnotation;
    private SequencePane _seqPane;
    private NamesPane _namesPane;
    private JSplitPane _sp;
    private JScrollBar _scrollBar = new JScrollBar(JScrollBar.VERTICAL);
    private JScrollBar _hscrollBar = new JScrollBar(JScrollBar.HORIZONTAL);
    private Alignment _align = new Alignment(null);
    public SequenceInfo _toolTip = new SequenceInfo();
//    public JPopupMenu _toolTip = null;
//    public JMenuItem _toolTipText = null;
    public int rulerHeight = 17;
    public int annotationHeight = (int)_lw+4;
    // Attention! Must be multiple of 3!
    public int tripletDist = 6;
    private int _lastSelected = -1;
    private int[] _lastPos = {0,0};
    private String _lastSearch = "";
    private boolean _lastSearchAllowedGaps = false;
    public GroupKey baseGroup = new GroupKey("All");
    
    private LinkedList<int[]> _conservedRegions;
    private int _conservedRegionsIndex = 0;
    
    public String baseUrl;
    private String fileId;
    private String username;

    private Settings _settings = new Settings();
    
    public Editor() {
        super();
    }

    public boolean hasMenu() {
        return false;
    }

    public boolean useIUB() {
        return false;
    }

    public boolean showSequenceName() {
        return true;
    }
    public String readFromURL(String url) throws MalformedURLException, IOException {
        System.out.println(url);
        URL rkitools = new URL(url);
        URLConnection rc = rkitools.openConnection();
        BufferedReader rcr = new BufferedReader(new InputStreamReader(rc.getInputStream()));
        StringBuilder db = new StringBuilder();
        while(true) {
            if(!rcr.ready()) {
                try {
                    Thread.sleep(1000);
                } catch(Exception e) {}
                if(!rcr.ready()) {
                    break;
                }
            }
            db.append(rcr.readLine());
            db.append("\n");
        }
        String data = db.toString();
        return data;
    }
    
    private void loadFromRKITools(String url, String fileId, boolean overwrite) {
        try {
            String data = readFromURL(url + fileId + "/");
            loadString(data, overwrite);
        } catch (MalformedURLException e) {
            System.out.println("Error:" + e.getMessage() + e.toString());
            e.printStackTrace();
        }
        catch (IOException e) {
            System.out.println("Error:" + e.getMessage() + e.toString());            
            e.printStackTrace();
        }
        requestFocusInWindow();
    }

    @Override
    public boolean isSelectionActive() {
        return false;
    }

    @Override
    public String seqsToString(boolean pureFASTA) {
        StringBuilder datab = new StringBuilder();
        for(Sequence s : _align.getSequencesAll()) {
            datab.append(">" + s.name);
            if(!pureFASTA) {
                datab.append("%%" + s.group.getName()+ "%%");
            }
            datab.append("\n");
            datab.append(s.seq);
            datab.append("\n");
        }
        String data = datab.toString();
        return data;
    }

    public void saveToRKITools(String name) {
        if(baseUrl == null || fileId == null) {
            System.out.println("Oops");
        }
        try {
            System.out.println("Saving...");
            StringBuilder datab = new StringBuilder();
            for(Sequence s : _align.getSequencesAll()) {
                datab.append(">" + s.name + "%%" + s.group.getName()+ "%%" + "\n");
                datab.append(s.seq);
                datab.append("\n");
            }
            System.out.println("  step 1 done");
            String data = datab.toString();
            ClientHttpRequest req = null;
            if(name == null) {
                System.out.println("  step 2 done");
                req = new ClientHttpRequest(baseUrl + "save/" + fileId + "/");
                req.setParameter("data", data);
                System.out.println("  step 3 done");
            }
            else {
                System.out.println("  step 4 done");
                req = new ClientHttpRequest(baseUrl + "save_as/" + fileId + "/");
                req.setParameter("data", data);
                req.setParameter("name", name);
                System.out.println("  step 5 done");
            }
            System.out.println("  step 6 done");
            InputStream is = req.post();
            System.out.println("  step 7 done");
/*            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);
            String res = reader.readLine();
            if(name != null) {
                fileId = res.trim();
            }*/
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        requestFocusInWindow();
    }

    @Override
    public void saveFile() {

    }

    @Override
    public void saveFileAs() {

    }

    private String getColorOpts() {
        String colorScheme = "UPGMA";
        try {
            colorScheme = readFromURL(baseUrl + "color_scheme/" + username + "/");
        } catch(Exception e) {
        }
        return colorScheme.replace("_", " ");
    }

    public void setColorOpts(String opts) {
        try {
            readFromURL(baseUrl + "set_color_scheme/" + username + "/" + opts.replace(" ", "_") + "/");
        } catch(Exception e) {
        }
    }

    @Override
    public void init() {
        _width = getSize().width - 40;
        _height = getSize().height;
        System.out.println("AppletViewer version " + VERSION + " loading...");
        baseUrl = this.getParameter("baseurl");
        fileId = this.getParameter("fileId");
        username = this.getParameter("username");

        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        GridBagConstraints constraints = new GridBagConstraints();
        addFocusListener(this);
        addKeyListener(this);
        // The mouse wheel listener must be added to the topmost parent due to this java bug:
        // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6480024
//        getParent().addMouseWheelListener(this);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        _namesPane = new NamesPane(_offset, this);
        _seqPane = new SequencePane(_offset, this);
        _sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, _namesPane, _seqPane);
        _sp.setPreferredSize(new Dimension(_width, _height));
        _scrollBar.setPreferredSize(new Dimension(20, _height));
        String colorOpts = getColorOpts().trim();
        if(ColorModels.models.containsKey(colorOpts)) {
            ColorModels.currentModel = colorOpts;
        }
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        constraints.weightx = 1;
        constraints.weighty = 1;
        this.add(_sp, constraints);
        constraints.gridx = 1;
        constraints.weightx = 0;
        this.add(_scrollBar, constraints);
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.weighty = 0;
        this.add(_hscrollBar, constraints);
        _scrollBar.addAdjustmentListener(this);
        _hscrollBar.addAdjustmentListener(this);
        _sp.setDividerLocation(50);
        SwingUtilities.updateComponentTreeUI(this);
        if(baseUrl != null && fileId != null) {
            loadFromRKITools(baseUrl + "download/", fileId, true);
        }
        else {
            Data.loadDefaultAlignment(_align);
        }
        updateScrollbar();
        repaintAll();
        requestFocusInWindow();
    }

    public int getAppletWidth() {
        return(_width);
    }
    
    public int getAppletHeight() {
        return(_height);
    }
    
    public void updateScrollbar() {
        _scrollBar.setMaximum(_align.countAll());
        _scrollBar.setValue(_offset[1]);
        int max = _align.getLongestAll();
        _hscrollBar.setMaximum(max);
        _hscrollBar.setValue(_offset[0]);
        int extent = (int)(_seqPane.getImageWidth()/_lw);
        if(extent < max) {
            _hscrollBar.setVisibleAmount(extent);
        }
        else {
            scrollTo(0, _offset[1]);
            _hscrollBar.setVisibleAmount(max-1);
        }
    }

    @Override
    public int[] getSelFrom() {
        return selFrom;
    }

    @Override
    public int[] getSelTo() {
        return selTo;
    }

    @Override
    public int getMode() {
        return mode;
    }

    @Override
    public int getConsHeight() {
        return consHeight;
    }

    @Override
    public int getTripletDist() {
        return tripletDist;
    }

    @Override
    public int getAnnotationHeight() {
        return annotationHeight;
    }

    @Override
    public Annotation getEditedAnnotation() {
        return editedAnnotation;
    }

    @Override
    public void setEditedAnnotation(Annotation anno) {
        editedAnnotation = anno;
    }

    @Override
    public void setSelFrom(int index, int value) {
        selFrom[index] = value;
    }

    @Override
    public void setSelTo(int index, int value) {
        selTo[index] = value;
    }

    @Override
    public double getLetterWidth() {
        return (_lw);
    }
    
    @Override
    public int getLetterHeight() {
        if(_lw >= minLineHeight) {
            return((int)_lw);
        }
        return(minLineHeight);
    }
    
    public int getTopOffset() {
        if(masterSequenceAll == null) {
            return(rulerHeight);
        }
        return(rulerHeight + getLetterHeight() + consHeight + 5);
    }
    
    public int getConsOffset() {
        return(rulerHeight + getLetterHeight());
    }

    public void setLetterWidth(int lw) {
        _lw = lw;
        annotationHeight = lw+4;
        repaintAll();
    }

    public Frame getParentFrame(){ 
        Container c = this; 
        while(c != null){ 
            if (c instanceof Frame) 
                return (Frame)c; 

            c = c.getParent(); 
        } 
        return (Frame)null; 
    }

    @Override
    public void setMasterSequence(MasterSequence master, GroupKey group) {
        MasterSequence masterSeq = new MasterSequence(master);
        masterSequences.put(group, masterSeq);
        int allseqs = _align.count(group);
        int len = masterSeq.seq.length();
        char[][] seqs = _align.toCharArray(len, group);
        calcMasterConservation(masterSeq, seqs, allseqs);
    }
    
    @Override
    public void setMasterSequenceAll(MasterSequence master) {
        masterSequenceAll = new MasterSequence(master);
        int allseqs = _align.countAll();
        int len = master.seq.length();
        char[][] seqs = _align.toCharArrayAll(len);
        calcMasterConservation(masterSequenceAll, seqs, allseqs);
        _seqPane.updateCopyMenues();
    }

    private void calcMasterConservation(MasterSequence master, char[][] seqs, int allseqs) {
        char[] masterSeq = master.seq.toCharArray();
        int len = master.seq.length();
        master.conservation = new double[len];
        _conservedRegions = new LinkedList<int[]>();
        int conslen = 0;
        for(int i=0; i<len; i++) {
            int nseq = allseqs;
            int ident = 0;
            for(int j=0; j<allseqs; j++) {
                if(seqs[j][i] == masterSeq[i]) {
                    ident ++;
                }
                if(seqs[j][i] == '\0') {
                    nseq --;
                }
            }
            master.conservation[i] = (double)ident/(double)nseq;
            if(master.conservation[i] == 1) {
                conslen ++;
            }
            else if(conslen != 0) {
                int[] place = {i-conslen, conslen};
                _conservedRegions.add(place);
                conslen = 0;
            }
        }
        repaintAll();
    }

    public void getMasterConsensus() {
        int longest = _align.getLongestAll();
        int allseqs = _align.countAll();
        char[][] seqs = _align.toCharArrayAll(longest);
        MasterSequence masterSequence = calculateConsensusSequence(longest, allseqs, seqs);
        setMasterSequenceAll(masterSequence);        
        _align.calculateHomology(masterSequence);
    }
    
    public void getConsensus(GroupKey group) {
        int longest = _align.getLongest(group);
        int allseqs = _align.count(group);
        char[][] seqs = _align.toCharArray(longest, group);
        MasterSequence masterSequence = calculateConsensusSequence(longest, allseqs, seqs);
        masterSequence.group = group;
        setMasterSequence(masterSequence, group);        
        _align.calculateHomology(masterSequences.get(group));
    }

    public void getGroupMasterSequences() {
        Collection<GroupKey> groups = getAlignment().getGroups();
        for(GroupKey group : groups) {
            getConsensus(group);
        }
    }

    /**
     * Finds SNPs in the alignment which can be used to differenciate between the defined sequence groups.
     */
    public void findSNPs() {
        getMasterConsensus();
        getGroupMasterSequences();
        int ngroups = masterSequences.size();
        for(int pos = 0; pos < getAlignment().getLongestAll(); pos ++) {
            // If the conservation of the master consensus sequence is 100%, then this can not be a SNP
            if(masterSequenceAll.conservation[pos] == 1)
                continue;
            char val = masterSequenceAll.seq.charAt(pos);
            int differ = 0;
            for(MasterSequence seq : masterSequences.values()) {
                if(seq.conservation[pos] != 1) {
                    differ = -1;
                    break;
                }
                if(seq.seq.charAt(pos) != val)
                    differ ++;
            }
            if(differ != -1) {
                getAlignment().toggleSnp(pos);
            }
        }
    }

    private MasterSequence calculateConsensusSequence(int longest, int allseqs, char[][] seqs) {
        char[] bases = new char[5]; //0: A, 1: G, 2: T, 3: C, 4: -
        char[] consensus = new char[longest];
        for(int i=0; i<longest; i++) {
            for(int j=0; j<allseqs; j++) {
                switch(seqs[j][i]) {
                    case 'A':
                        bases[0]++; break;
                    case 'G':
                        bases[1]++; break;
                    case 'T':
                        bases[2]++; break;
                    case 'C':
                        bases[3]++; break;
                    case '-':
                        bases[4]++; break;
                    default:
                }       
            }
            int pos = 4;
            int max = 0;
            for(int n=0; n<4; n++) {
                if(bases[n] > max) {
                    pos = n;
                    max = bases[n];
                }
                bases[n] = 0;
            }
            switch(pos) {
                case 0:
                    consensus[i] = 'A'; break;
                case 1:
                    consensus[i] = 'G'; break;
                case 2:
                    consensus[i] = 'T'; break;
                case 3:
                    consensus[i] = 'C'; break;
                case 4:
                    consensus[i] = '-'; break;
                default:
                    consensus[i] = '-';
            }
        }
        return(new MasterSequence("Consensus (" + allseqs + " sequences)", new String(consensus), baseGroup, _align));
    }
    
    @Override
    public int getPrimerEditPos() {
        return 0;
    }

    @Override
    public boolean isPrimerTmEnabled() {
        return false;
    }

    @Override
    public boolean isPrimerLengthEnabled() {
        return false;
    }

    @Override
    public Alignment getAlignment() {
        return (_align);
    }

    @Override
    public HashMap<GroupKey, MasterSequence> getMasterSequences() {
        return masterSequences;
    }

    @Override
    public MasterSequence getMasterSequenceAll() {
        return masterSequenceAll;
    }

    public void repaintAll() {
        requestFocus();
        requestFocusInWindow();
        requestFocus();
        _seqPane.redrawAll();
        _seqPane.repaint();
        _namesPane.repaint();
        requestFocusInWindow();
    }

    @Override
    public void stop() {
    }

    private boolean pointInSelection(int[] point) {
        Position rpoint = getRealPos(point);
        Rectangle r = new Rectangle(selFrom[0], selFrom[1], selTo[0], selTo[1]);
        Point p = new Point(rpoint.x, rpoint.y);
        if(r.contains(p)) {
            return(true);
        }
        return(false);
    }

    @Override
    public int getRulerHeight() {
        return rulerHeight;
    }

    
    public int getScreenX(int posx) {
        double res = (posx-_offset[0])*_lw;
        if(getAlignment().tripletGrouping != -1) {
            res += (((getAlignment().tripletGrouping+_offset[0])%3+posx)/3)*tripletDist;
        }
        return((int)res);
    }

    public int getScreenY(int posy, boolean consensus) {
        int res = getTopOffset();
        int lh = getLetterHeight();
        res += lh;
        for(int i=_offset[1]; i<=posy; i++) {
            Sequence lastSeq = getAlignment().getSequence(i-1);
            Sequence seq = getAlignment().getSequence(i);
            if(seq == null) {
                break;
            }
            if(lastSeq != null && lastSeq.group == seq.group && seq.collapsed) {
                continue;
            }
            if(lastSeq != null) {
                res += lastSeq.below.levels.size()*annotationHeight;
                if(lastSeq.group != seq.group) {
                    res += groupSep;
                    if(masterSequences.containsKey(seq.group)) {
                        res += getLetterHeight();
                        res += getConsOffset();
                    }
                }
            }
            else if(masterSequences.containsKey(seq.group)) {
                res += getLetterHeight();
                res += getConsOffset();
            }
            if(consensus && i == posy) {
                return res;
            }
            res += lh;
            res += seq.above.levels.size()*annotationHeight;
        }
        return(res);
    }

    @Override
    public int getScreenY(int posy) {
        return getScreenY(posy, false);
    }

    @Override
    public int getScreenYConsensus(int posy) {
        return getScreenY(posy, true);
    }

    public int getOffsetY() {
        return(getScreenY(_offset[1]));
    }
    
    private Annotation onAnnotation(int posX, int posY, LinkedList<LinkedList<Annotation>> levels, int ypos) {
        ypos -= levels.size()*annotationHeight;
        Annotation found = null;
        for(LinkedList<Annotation> level : levels) {
            ypos += annotationHeight;
            if(posY <= ypos) {
                for(Annotation anno : level) {
                    int[] axp = _seqPane.getAnnotationXpos(anno);
                    if(posX > axp[0] && posX < axp[1]) {
                        found = anno;
                        break;
                    }
                }
                if(found != null) {
                    break;
                }        
            }
        }
        return(found);
    }
    
    public Position getRealPos(int[] screenPos) {
        int[] res = {0, 0};
        int pos = getLetterHeight();
        int ypos;
        Annotation onAnnotation = null;
        int lh = getLetterHeight();
        boolean onCollapser = false;
        boolean onConsensus = false;
        boolean first = true;
        GroupKey lastGroup = new GroupKey("__NONAME__");
        GroupKey onConsensusGroup = null;
        int numSequences = _align.countAll();
        for(ypos=_offset[1]; pos < screenPos[1] - getTopOffset() && ypos < numSequences; ypos++) {
            first = false;
            if(_align.getSequence(ypos-1) != null) {
                lastGroup = _align.getSequence(ypos-1).group;
            }
            onConsensus = false;
            onCollapser = false;
            Sequence seq = _align.getSequence(ypos);
            Sequence nextSeq = _align.getSequence(ypos + 1);
            if(nextSeq != null && seq.group != nextSeq.group) {
                pos += groupSep;
            }
            if(lastGroup != seq.group) {
                if(masterSequences.containsKey(seq.group)) {
                    pos += getLetterHeight();
                    pos += getConsOffset();
                }
                if(pos >= screenPos[1] - getTopOffset()) {
                    onConsensus = true;
                    onConsensusGroup = seq.group;
                }
            }
            if(lastGroup == seq.group && seq.collapsed) {
                continue;
            }
            if(lastGroup != seq.group && screenPos[0] <= 12) {
                onCollapser = true;
            }
            boolean hit = false;
            if(seq == null) {
                ypos --;
                break;
            }
            pos += seq.above.levels.size()*annotationHeight;
            if(pos >= screenPos[1] - getTopOffset()) {
                onAnnotation = onAnnotation(screenPos[0], screenPos[1] - getTopOffset(), seq.above.levels, pos);
                hit = true;
            }
            pos += lh;
            if(pos >= screenPos[1] - getTopOffset()) {
                hit = true;
            }
            pos += seq.below.levels.size()*annotationHeight;
            if(!hit && pos >= screenPos[1] - getTopOffset()) {
                onAnnotation = onAnnotation(screenPos[0], screenPos[1] - getTopOffset(), seq.below.levels, pos);
            }
            if(pos >= screenPos[1] - getTopOffset()) {
                break;
            }
        }
        res[1] = ypos;
        if(_align.tripletGrouping == -1) {
            res[0] = (int)(screenPos[0]/_lw) + _offset[0];
        }
        else {
            res[0] = (int)(((screenPos[0] + (_offset[0]*_lw) + ((_align.tripletGrouping + _offset[0])%3)*tripletDist/3))/(_lw+tripletDist/3));
        }
        if(first) {
            onConsensus = true;
        }
        return (new Position(res[0], res[1], onAnnotation!=null, onAnnotation, onCollapser, onConsensus, onConsensusGroup));
    }

    @Override
    public void addAnnotation(boolean above, boolean primer) {
        String text = primer?"Primer":"Annotation";
        _align.getSequence(selFrom[1]).addAnnotation(above, selFrom[0], selTo[0], text, primer);
        repaintAll();
    }

    @Override
    public boolean isPrimerSequenceActive() {
        return false;
    }

    private void unselectAll() {
        for (Sequence seq : _align.getSequencesAll()) {
            seq.selected = false;
        }
    }

    private void scrollSequences(MouseEvent e) {
        boolean rep = false;
        if (Math.abs(_lastMousePos[0] - e.getX()) >= _lw && e.getSource() == _seqPane) {
            _offset[0] += (_lastMousePos[0] - e.getX()) / _lw;
            _lastMousePos[0] = e.getX();
            if (_offset[0] < 0) {
                _offset[0] = 0;
            }
            rep = true;
        }
        if (Math.abs(_lastMousePos[1] - e.getY()) >= getLetterHeight()) {
            _offset[1] += (_lastMousePos[1] - e.getY()) / getLetterHeight();
            _lastMousePos[1] = e.getY() - (_lastMousePos[1] - e.getY()) % 8;
            if (_offset[1] < 0) {
                _offset[1] = 0;
            }
            rep = true;
        }
        if (rep) {
            scrollTo(_offset[0], _offset[1]);
            repaintAll();
        }
    }

    private void selectNames(MouseEvent e) {
        int[] pos = {e.getX(), e.getY()};
        int from = getRealPos(_lastMousePos).y;
        int to = getRealPos(pos).y;
        if (!e.isShiftDown()) {
            unselectAll();
        }
        if (from > to) {
            int tmp = to;
            to = from;
            from = tmp;
        }
        for (int i = from; i <= to && i < _align.countAll(); i++) {
            _align.getSequence(i).selected = true;
        }
        repaintAll();
    }

    private void moveSel(int pos0, int pos1) {
        // Moving disabled until undo is implemented
        return;
/*        for(int i=selFrom[1]; i<selTo[1]; i++) {
            _align.getSequence(i).movePart(selFrom[0], selTo[0], pos1-pos0);
        }*/
    }
    
    public void loadString(String s, boolean overwrite) {
        System.out.println("Loading...");
        Alignment align = _align;
        if(overwrite) {
            align = new Alignment(null);
        }
        String[] lines = s.split(">");
        HashMap<String, GroupKey> loadedGroups = new HashMap<String, GroupKey>();
        if(!overwrite) {
            for(GroupKey key : _align.getGroups()) {
                if(!loadedGroups.containsKey(key.getName())) {
                    loadedGroups.put(key.getName(), key);
                }
            }
        }
        for(String line : lines) {
            if(line.equals("")) continue;
            String[] vals = line.split("\n");
            String name = vals[0];
            vals[0] = "";
            StringBuilder seqBuilder = new StringBuilder();
            for(String val : vals) {
                seqBuilder.append(val.replace("\r", "").replace(" ", ""));
            }
            String seq = seqBuilder.toString().toUpperCase();
            GroupKey group = baseGroup;
            if(loadedGroups.containsKey(baseGroup.getName())) {
                group = loadedGroups.get(baseGroup.getName());
            }
            // Get the name of the group the sequence belongs to from the name
            if(name.contains("%%")) {
                String[] nameVals = name.split("%%");
                name = nameVals[0];
                String groupName = nameVals[1];
                if(loadedGroups.containsKey(groupName)) {
                    group = loadedGroups.get(groupName);
                }
                else {
                    group = new GroupKey(groupName);
                    loadedGroups.put(groupName, group);
                }
            }
            align.addSequence(new Sequence(name, seq, group, align));
        }
        if(overwrite) {
            _align = align;
        }
        for(GroupKey group : loadedGroups.values()) {
            System.out.println(group.getName());
        }
        _align.sortGroups();
        _namesPane.updateGroupMenuItems();
        repaintAll();
        requestFocusInWindow();
    }

    private void parseTree(JSONArray data, DefaultMutableTreeNode parent) {
        try {
            for(int i = 0; i < data.length(); i ++) {
                JSONObject cdata = data.getJSONObject(i);
                RKIFolder folder = new RKIFolder(cdata.getJSONObject("property").getString("name"), cdata.getJSONObject("property").getString("data"));
                DefaultMutableTreeNode child = new DefaultMutableTreeNode(folder);
                try {
                    parseTree(cdata.getJSONArray("children"), child);
                } catch(Exception e) {}
                parent.add(child);
            }
        } catch(Exception e) {
        }
    }

    private DefaultMutableTreeNode parseTree(JSONArray data) {
        try {
            JSONObject root = data.getJSONObject(0);
            RKIFolder folder = new RKIFolder(root.getJSONObject("property").getString("name"), root.getJSONObject("property").getString("data"));
            DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(folder);
            parseTree(root.getJSONArray("children"), rootNode);
            return rootNode;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean loadFileRKI(boolean overwrite) {
        try {
            String data = readFromURL(baseUrl + "tree/" + username + "/");
            JSONArray tree = new JSONArray(data);
            DefaultMutableTreeNode root = parseTree(tree);
            RKIFilesDialog dialog = new RKIFilesDialog(this.findParentFrame(), "test", root, this);
            dialog.setVisible(true);
            if(dialog.accepted) {
//                System.out.println(dialog.getSelectedFile().name);
                loadFromRKITools(baseUrl + "download/", dialog.getSelectedFile().id, overwrite);
                updateScrollbar();
            }
        }
        catch(Exception e) {
            System.out.println("booh");
        }
        return true;
    }

    public void loadFile(boolean overwrite) {
        final JFileChooser fc = new JFileChooser();
        fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
        fc.addChoosableFileFilter(new FastaFilter());
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                BufferedReader input = new BufferedReader(new FileReader(file));
                StringBuilder s = new StringBuilder();
                while(input.ready()) {
                    s.append(input.readLine());
                    s.append("\n");
                }
                loadString(s.toString(), overwrite);
            }
            catch(Exception e) {
                System.out.println(e.getMessage());
            }
        }
        masterSequenceAll = null;
        masterSequences = new HashMap<GroupKey, MasterSequence>();
        repaintAll();
        requestFocusInWindow();
    }
        
    public void mouseDragged(MouseEvent e) {
        requestFocusInWindow();
        if (_mouseButton == 2) {
            scrollSequences(e);
        }
        else if (_mouseButton == 1 && e.getSource() == _namesPane) {
            if(mode == MODE_MOVESEL) {
                int[] screenPos = {e.getX(), e.getY()};
                Position newPos = getRealPos(screenPos);
                if(newPos.y != _movePos) {
                    int dist = newPos.y - _movePos;
                    _movePos = newPos.y;
                    _align.moveSelected(dist);
                    repaintAll();
                }
            }
//            selectNames(e);
        }
        else if (_mouseButton == 1 && e.getSource() == _seqPane) {
            int[] screenPos = {e.getX(), e.getY()};
            // Moving disabled until undo is implemented
            if(mode == MODE_MOVESEL && 1==0) {
                Position newPos = getRealPos(screenPos);
                int diff = newPos.x - _movePos;
                if(selFrom[0] + diff < 0 || selFrom[1] + diff < 0)
                    diff = 0 - Math.min(selFrom[0], selFrom[1]);
                if(newPos.x != _movePos) {
                    moveSel(_movePos, newPos.x);
                    selFrom[0] += diff;
                    selTo[0] += diff;
                    _movePos = newPos.x;
                    repaintAll();
                }
            }
            else if(mode == MODE_MOVEANNO) {
                Position newPos = getRealPos(screenPos);
                int diff = newPos.x - _movePos;
                if(diff != 0) {
                    selectedAnnotation.from += diff;
                    selectedAnnotation.to += diff;
                    _movePos = newPos.x;
                    _seqPane.repaint();
                    updateFlowgrams(false);
                }
            }
            else {
                int[] oldTo = {selTo[0], selTo[1]};
                Position p = getRealPos(screenPos);
                selTo[0] = p.x;
                if(selConsensus) {
                    if(consensusGroup == null) {
                        selFrom[1]=0;
                        selTo[1] = _align.countAll();
                    }
                    else {
                        System.out.println(consensusGroup.getName());
                        selFrom[1] = _align.getGroupFirst(consensusGroup);
                        selTo[1] = selFrom[1] + _align.count(consensusGroup);
                    }
                }
                else {
                    selTo[1] = p.y+1;
                    if(selTo[1] == selFrom[1]) {
                        selFrom[1] -= 1;
                    }
                }
                if(oldTo[0] != selTo[0] || oldTo[1] != selTo[1]) {
                    repaintAll();
                }
            }
        }
    }

    public SequenceInfo getToolTip() {
        if(_toolTip == null) {
            _toolTip = new SequenceInfo();
            _toolTip.text = "Test";
        }
        return(_toolTip);
    }
    
    public void mouseMoved(MouseEvent e) {
        requestFocusInWindow();
        if(mode != MODE_EDITING) {
            int[] screenPos = {e.getX(), e.getY()};
            Position realPos = getRealPos(screenPos);
            if(realPos.x != _lastPos[0] || realPos.y != _lastPos[1]) {
                _seqPane.redrawCursor(realPos.x, realPos.y);
                _seqPane.repaint();
                _lastPos[0] = realPos.x;
                _lastPos[1] = realPos.y;
            }
            if(e.getSource() == _seqPane) {
                _seqPane.seqNum = realPos.y;
//                _toolTip = getToolTip();
//                _toolTip.setLocation(_seqPane.getLocationOnScreen().x + e.getX() - 50, _seqPane.getLocationOnScreen().y + e.getY() + 15);
                if(realPos.onConsensus) {
                    _seqPane.toolTipText = "Consensus for " + _align.getSequence(realPos.y).group.getName();
//                    _toolTip.text = "Consensus for " + _align.getSequence(realPos.y).group.getName();
//                    _toolTipText.setText("Consensus for " + _align.getSequence(realPos.y).group.getName());
                }
                else {
                    _seqPane.toolTipText = _align.getSequence(realPos.y).name;
//                    _toolTip.text = _align.getSequence(realPos.y).name;
//                    _toolTipText.setText(_align.getSequence(realPos.y).name);
                }
//                _toolTip.setVisible(true);
//                _toolTip.repaint();
            }
        }
        if(e.getSource() != _seqPane) {
            getToolTip().setVisible(false);
        }
    }

    public void mouseClicked(MouseEvent e) {
        requestFocusInWindow();
        if(e.getButton() == 3) {
            return;
        }
        if (e.getSource() == _namesPane) {
            if (!e.isShiftDown() && !e.isControlDown()) {
                System.out.println("Unselecting");
                unselectAll();
            }
            int[] pos = {e.getX(), e.getY()};
            Position realPos = getRealPos(pos);
            if(realPos.onCollapser) {
                Sequence selectedSeq = _align.getSequence(realPos.y);
                selectedSeq.collapsed = !selectedSeq.collapsed;
                for(Sequence seq : _align.getSequences(selectedSeq.group)) {
                    seq.collapsed = selectedSeq.collapsed;
                }
            }
            else {
                if(!e.isShiftDown() || _lastSelected == -1) {
                    System.out.println("Hello2" + _align.getSequence(realPos.y).selected);
                    _align.getSequence(realPos.y).selected ^= true;
                    if(_align.getSequence(realPos.y).selected) {
                        _lastSelected = realPos.y;                        
                    }
                }
                else {
                    if(_lastSelected != -1) {
                        for(int i=Math.min(_lastSelected, realPos.y); i <= Math.max(_lastSelected, realPos.y); i++) {
                            _align.getSequence(i).selected = true;
                        }
                    }
                }
            }
            repaintAll();
        }
        else {
            int[] coords = {e.getX(), e.getY()};
            Position pos = getRealPos(coords);
            if(pos.onAnnotation) {
                editedAnnotation = pos.annotation;
            }
            else {
                if(mode == MODE_SELECTING) {
                    mode = MODE_EDITING;
                }
                else {
                    mode = MODE_NORMAL;
                }
                selFrom[0] = 0;
                selFrom[1] = 0;
                selTo[0] = 0;
                selTo[1] = 0;
            }
            _seqPane.repaint();
        }
    }

    public void mouseEntered(MouseEvent arg0) {
        requestFocusInWindow();
    }

    public void mouseExited(MouseEvent arg0) {
        getToolTip().setVisible(false);
    }

    public void mousePressed(MouseEvent e) {
        requestFocusInWindow();
        _mouseButton = e.getButton();
        _lastMousePos[0] = e.getX();
        _lastMousePos[1] = e.getY();
        if(_mouseButton == MouseEvent.BUTTON2) {
            setCursor(new Cursor(Cursor.MOVE_CURSOR));
        }
        else if(_mouseButton == MouseEvent.BUTTON1 && e.getSource() == _namesPane) {
            int[] screenPos = {e.getX(), e.getY()};            
            Position realPos = getRealPos(screenPos);
            if(_align.getSequence(realPos.y).selected) {
                setCursor(new Cursor(Cursor.MOVE_CURSOR));
                mode = MODE_MOVESEL;
                _movePos = getRealPos(screenPos).y;
            }
        }
        else if(_mouseButton == MouseEvent.BUTTON1 && e.getSource() == _seqPane) {
            int[] screenPos = {e.getX(), e.getY()};            
            Position realPos = getRealPos(screenPos);
            if(pointInSelection(screenPos)) {
                mode = MODE_MOVESEL;
                _movePos = getRealPos(screenPos).x;
                setCursor(new Cursor(Cursor.MOVE_CURSOR));
            }
            else if(realPos.onAnnotation) {
                mode = MODE_MOVEANNO;
                _movePos = getRealPos(screenPos).x;
                selectedAnnotation = realPos.annotation;
                setCursor(new Cursor(Cursor.MOVE_CURSOR));
            }
            else {
                if(mode == MODE_EDITING) {
                    mode = MODE_SELECTING2;
                }
                else {
                    mode = MODE_SELECTING;
                }
                Position pos = getRealPos(screenPos);
                selFrom = pos.getCoordinates();
                selTo = pos.getCoordinates();
                selConsensus = pos.onConsensus;
                consensusGroup = pos.consensusGroup;
            }
        }
    }

    public void mouseReleased(MouseEvent arg0) {
        requestFocusInWindow();
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        if(mode == MODE_MOVEANNO) {
            selectedAnnotation.list.resetAnnotations();
            selectedAnnotation = null;
            repaintAll();
        }
    }

    public void keyTyped(KeyEvent e) {
        if(e.getKeyChar() == '-') {
            doZoom(1);
        }
        else if(e.getKeyChar() == '+') {
            doZoom(-1);
        }
    }

    public void scrollTo(int x, int y) {
        _offset[0] = x;
        _offset[1] = y;
        _hscrollBar.setValue(x);
        _scrollBar.setValue(y);
    }
    
    public void searchForSequence(String seq, boolean allowGaps) {
        int[] pos = _align.findSubSequence(selFrom[1], selTo[0], seq, allowGaps);
        if(pos[0] != -1) {
            selFrom[1] = pos[0];
            selTo[1] = pos[0]+1;
            selFrom[0] = pos[1];
            selTo[0] = pos[1] + seq.length();
            scrollTo(selFrom[0], selFrom[1]);
            repaintAll();
        }        
    }
    
    private Frame findParentFrame() {
        Container c = this;
        while(c != null) {
            if(c instanceof Frame) {
                return((Frame)c);
            }
            c = c.getParent();
        }
        return((Frame)null);
    }
    
    
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == e.VK_F && (e.getModifiers() & e.CTRL_MASK) != 0 ) {
            FindDialog d = new FindDialog(findParentFrame());
            d.setVisible(true);
            if(d.isAccepted()) {
//                _lastSearch = d.getSeq().toUpperCase().replaceAll(" ", "");
                _lastSearch = Sequence.cleanUpSequence(d.getSeq());
                _lastSearchAllowedGaps = d.allowGaps();
                searchForSequence(_lastSearch, _lastSearchAllowedGaps);
            }
            requestFocusInWindow();
//            System.out.println("Hello");
/*            _toolTip.setVisible(false);
            String sequence = (String)JOptionPane.showInputDialog(this, "Please enter sequence:", "Sequence search",
                    JOptionPane.PLAIN_MESSAGE, null, null, _lastSearch);
            if(sequence == null) {
                return;
            }
            _lastSearch = sequence.toUpperCase().replaceAll(" ", "");
            searchForSequence(_lastSearch);
            requestFocusInWindow();*/
        }
        else if(key == e.VK_F3) {
            if(!_lastSearch.equals("")) {
                searchForSequence(_lastSearch, _lastSearchAllowedGaps);
                requestFocusInWindow();
            }
        }
        else if(mode == MODE_EDITING) {
            boolean seqChanged = false;
            if(key == KeyEvent.VK_LEFT) {
                _lastPos[0] -= 1;
            }
            else if(key == KeyEvent.VK_RIGHT) {
                _lastPos[0] += 1;
            }
            else if(key == KeyEvent.VK_UP) {
                _lastPos[1] -= 1;
            }
            else if(key == KeyEvent.VK_DOWN) {
                _lastPos[1] += 1;
            }
            else if(e.getKeyChar() == 'a' || e.getKeyChar() == 'g' || e.getKeyChar() == 't' || e.getKeyChar() == 'c') {
                _align.getSequence(_lastPos[1]-1).setCharAt(_lastPos[0], Character.toUpperCase(e.getKeyChar()));
                _lastPos[0] += 1;
                seqChanged = true;
            }
            _seqPane.redrawCursor(_lastPos[0], _lastPos[1]);
            _seqPane.redrawAll();
            _seqPane.repaint();
        }
        else if(key == KeyEvent.VK_N) {
            if(_conservedRegionsIndex < _conservedRegions.size() - 1) {
                _conservedRegionsIndex += 1;
                scrollTo(_conservedRegions.get(_conservedRegionsIndex)[0], _offset[1]);
                selFrom[0] = _offset[0];
                selFrom[1] = 0;
                selTo[0] = selFrom[0] + _conservedRegions.get(_conservedRegionsIndex)[1];
                selTo[1] = _align.countAll();
                repaintAll();
            }
        }
    }

    public void keyReleased(KeyEvent arg0) {
    }

    public void focusGained(FocusEvent arg0) {
    }

    public void focusLost(FocusEvent arg0) {
    }
    
    public void doZoom(int direction) {
        if(direction < 0) {
            if(_lw == 10 || _lw == 8) {
                _lw += 2;
            }
            else if(_lw < 8) {
                _lw *= 2;
            }
        }
        else if(direction > 0) {
            if(_lw == 12 || _lw == 10) {
                _lw -= 2;
            }
            else if(_lw <= 8) {
                _lw /= 2;
            }
        }
        updateScrollbar();
        repaintAll();
    }
    
    public void mouseWheelMoved(MouseWheelEvent e) {
/*        if(!e.isControlDown())
            return;
        int notches = e.getWheelRotation();
        doZoom(notches);*/
    }

    public void adjustmentValueChanged(AdjustmentEvent e) {
        if(e.getSource() == _scrollBar) {
            scrollTo(_offset[0], e.getValue());
        }
        else if(e.getSource() == _hscrollBar) {
            scrollTo(e.getValue(), _offset[1]);
        }        
        repaintAll();
    }
    @Override
    public boolean isApplication() {
        return true;
    }

    @Override
    public boolean isSequenceSelected() {
        if(getSelFrom()[0] == getSelTo()[0] || getSelFrom()[1] == getSelTo()[1])
            return false;
        return true;
    }

    @Override
    public boolean canAddAnnotation() {
        return isSequenceSelected() && Math.abs(getSelFrom()[1] - getSelTo()[1]) == 1;
    }

    @Override
    public void consensusChange() {
    }

    @Override
    public Settings getSettings() {
        return _settings;
    }

    @Override
    public void updateFlowgrams(boolean fromWindow) {
        findSNPs();
        _seqPane.updateFlowgrams(fromWindow);
    }

    @Override
    public void loadFile(boolean overwrite, LoadFilter filter, String filename) {
        throw new UnsupportedOperationException("Not supported in app (you should never see this, kill Wojtek).");
    }
}
