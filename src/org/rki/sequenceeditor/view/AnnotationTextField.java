package org.rki.sequenceeditor.view;

import javax.swing.JTextField;

public class AnnotationTextField extends JTextField {
    public boolean correctText = false;
    public boolean isAdded = false;
    
    public AnnotationTextField() {
        super();
    }
    
    public AnnotationTextField(String name) {
        super(name);
    }

}
