/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import javax.swing.JPanel;
import org.rki.sequenceeditor.model.Flowgram;
import org.rki.sequenceeditor.model.MasterSequence;
import org.rki.sequenceeditor.model.Primer;

/**
 *
 * @author dabrowskiw
 */
public class FlowPanel extends JPanel{

    private EditorInterface _parent;
    private Collection<Primer> _primers;
    private LinkedList<Flowgram> _fgrams = new LinkedList<Flowgram>();
    private HashMap<Flowgram, LinkedList<Flowgram>> _identical = new HashMap<Flowgram, LinkedList<Flowgram>>();
    public int length;

    public FlowPanel() {
        super();
        _parent = null;
    }

    public FlowPanel(EditorInterface parent) {
        super();
        _parent = parent;
        setPreferredSize(new Dimension(600, 600));
    }

    public void setParent(EditorInterface parent) {
        _parent = parent;
    }

    public void recalc() {
        if(_parent == null)
            return;
        LinkedList<Flowgram> flows = new LinkedList<Flowgram>();
        _identical.clear();
        _fgrams.clear();
        _primers = _parent.getAlignment().getPyroPrimers();
        for(MasterSequence seq : _parent.getMasterSequences().values()) {
            Flowgram flow = null;
            for(Primer primer : _primers) {
                if(flow == null) {
                    flow = primer.getFlowgram(seq, length, seq.group.getName());
                }
                else {
                    flow.add(primer.getFlowgram(seq, length, seq.group.getName()));
                }
            }
            flows.add(flow);
        }
        for(Flowgram flow : flows) {
            if(_identical.containsKey(flow)) {
                LinkedList<Flowgram> idlist = _identical.get(flow);
                idlist.add(flow);
                _identical.put(flow, idlist);
            }
            else {
                _identical.put(flow, new LinkedList<Flowgram>());
            }
        }
        for(Flowgram flow : _identical.keySet()) {
            _fgrams.add(flow);
        }
        Collections.sort(_fgrams);
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
        if(_parent == null) {
            return;
        }
        g.setFont(new Font("Courier", 0, 12));
        if(_primers.isEmpty()) {
            g.setColor(Color.BLACK);
            g.drawString("No pyrosequencing primers.", 5, 15);
            g.drawString("Please create at least one:", 5, 30);
            g.drawString("- Select part of a single sequence", 15, 45);
            g.drawString("- Right click and select 'Add Primer->Forward'", 15, 60);
            g.drawString("- Right click the primer", 15, 75);
            g.drawString("- Select 'is pyrosequencing primer'", 15, 90);
            return;
        }
        g.setColor(Color.BLACK);
        g.drawString(Integer.toString(_primers.size()) + " Primer" + (_primers.size()==1?"":"s"), 5, 15);
        int pos = 1;
        int offset = 0;
        for(Flowgram flow : _fgrams) {
            offset += flow.draw(g, 10, 50*pos+offset, _identical.get(flow));
            pos ++;
        }
/*        int npos = 0;
        g.setColor(Color.BLACK);
        for(Flowgram flow : _fgrams) {
            StringBuilder groupb = new StringBuilder();
            groupb.append(flow.getName());
            for(Flowgram f : _identical.get(flow)) {
                groupb.append(", ");
                groupb.append(f.getName());
            }
            g.drawString(groupb.toString(), 10, 50*pos + 10*npos);
            npos ++;
        }*/
    }

}
