/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.rki.sequenceeditor.model.Sequence;

/**
 *
 * @author dabrowskiw
 */
public class FindDialog extends JDialog implements ActionListener {
    private JLabel _titleText = new JLabel("Please enter sequence");
    private JTextField _seq = new JTextField();
    private JButton _ok = new JButton("OK");
    private JButton _cancel = new JButton("Cancel");
    private JButton _revert = new JButton("Reverse complement");
    private JCheckBox _allowGaps = new JCheckBox("Allow gaps");
    private boolean _accepted = false;
    
    public FindDialog(Frame parent) {
        super(parent, true);
        init();
        pack();
    }

    public void setText(String text) {
        _seq.setText(text);
    }

    public void allowGaps(boolean allow) {
        _allowGaps.setSelected(allow);
    }

    public void reset() {
        _seq.setText("");
        _accepted = false;
    }
    
    private void init() {
        BorderLayout layout = new BorderLayout();
        getContentPane().setLayout(layout);
        getContentPane().add(_titleText, BorderLayout.NORTH);
        getContentPane().add(_seq, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(_allowGaps);
        buttonPanel.add(_ok);
        buttonPanel.add(_cancel);
        buttonPanel.add(_revert);
        _ok.addActionListener(this);
        _cancel.addActionListener(this);
        _revert.addActionListener(this);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }
    
    public boolean isAccepted() {
        return(_accepted);
    }
    
    public String getSeq() {
        return(_seq.getText());
    }

    public boolean allowGaps() {
        return _allowGaps.isSelected();
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == _ok) {
            _accepted = true;
            hideSelf();
        }
        else if(e.getSource() == _cancel) {
            _accepted = false;
            hideSelf();
        }
        else if(e.getSource() == _revert) {
            String seq = Sequence.cleanUpSequence(_seq.getText());
            seq = Sequence.invert(seq);
            _seq.setText(seq);
        }
    }
    
    private void hideSelf() {
        this.setVisible(false);
    }
}
