/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import org.rki.sequenceeditor.model.Sequence;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import org.netbeans.lib.awtextra.AbsoluteConstraints;
import org.netbeans.lib.awtextra.AbsoluteLayout;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.Annotation;
import org.rki.sequenceeditor.model.BaseColorModel;
import org.rki.sequenceeditor.model.ColorModels;
import org.rki.sequenceeditor.model.GroupKey;
import org.rki.sequenceeditor.model.Letters;
import org.rki.sequenceeditor.model.MasterSequence;
import org.rki.sequenceeditor.model.Position;
import org.rki.sequenceeditor.model.Primer;
import org.rki.sequenceeditor.model.SNP;
import org.rki.sequenceeditor.model.Settings;
import org.rki.sequenceeditor.model.SizeMenuItem;
import org.rki.sequenceeditor.model.TripletMenuItem;

/**
 *
 * @author dabrowskiw
 */
public class SequencePane extends JPanel implements MouseListener, MouseWheelListener, ActionListener, ClipboardOwner {

    private BufferedImage _logoText;
    private BufferedImage _logoRKI;

    private int _width;
    private int _height;
    private int[] _pixels;
    private int[] _overlayPixels;
    private Image _disp;// = new BufferedImage(_width, _height, BufferedImage.TYPE_INT_RGB);
    private MemoryImageSource _source;
    private MemoryImageSource _overlay;
    private int[] _offset;
    private EditorInterface _parent;
    private int _cursorX=0, _cursorY=0;
    private JPopupMenu _menu = new JPopupMenu();
    private JMenuItem _deleteSelected = new JMenuItem("Delete selected");
    private JMenu _annotation = new JMenu("Add annotation");
    private JMenuItem _annotationAbove = new JMenuItem("Above sequence");
    private JMenuItem _annotationBelow = new JMenuItem("Below sequence");
    private JMenu _primer = new JMenu("Add primer");
    private JMenuItem _primerAbove = new JMenuItem("Forward");
    private JMenuItem _primerBelow = new JMenuItem("Reverse");
    private JMenuItem _loadFile = new JMenuItem("Load file");
    private JMenuItem _addSequences = new JMenuItem("Append sequences from file");    
    private JMenuItem _loadFileRKI = new JMenuItem("Load file from RKI-Tools");
    private JMenuItem _addSequencesRKI = new JMenuItem("Append sequences from RKI-Tools");
    private JMenuItem _saveFileRKI = new JMenuItem("Save file to RKI Tools");
    private JMenuItem _saveFileRKIas = new JMenuItem("Save file to RKI Tools as...");
    private JPopupMenu _annotationMenu = new JPopupMenu();
    private JPopupMenu _primerMenu = new JPopupMenu();
    private JMenuItem _removeAnnotation = new JMenuItem("Remove");
    private JMenuItem _recolorAnnotation = new JMenuItem("Change color");
    private JMenuItem _degeneratePrimer = new JMenuItem("Degenerate primer");
    private JMenuItem _degeneratePrimerGroup = new JMenuItem("Degenerate primer for group");
    private JMenuItem _copyPrimerSequence = new JMenuItem("Copy primer sequence");
    private JMenuItem _copyPrimer = new JMenuItem("Copy primer");
    private JMenuItem _copyPrimers = new JMenuItem("Copy all primers");
    private JMenu _copyProduct = new JMenu("Copy product with...");
    private JMenuItem _copyProductWithPrimersGaps = new JMenuItem("primers and gaps");
    private JMenuItem _copyProductWithoutPrimersGaps = new JMenuItem("no primers and gaps");
    private JMenuItem _copyProductWithPrimersWithoutGaps = new JMenuItem("primers and no gaps");
    private JMenuItem _copyProductWithoutPrimersWithoutGaps = new JMenuItem("no primers and no gaps");
    private JCheckBoxMenuItem _sequencingPrimer = new JCheckBoxMenuItem("Is pyrosequencing primer", false);
    private JMenu _copy = new JMenu("Copy");
    private JMenuItem _copySequences = new JMenuItem("Sequences");
    private JMenuItem _copySequencesWithNames = new JMenuItem("Sequences with names");
    private JMenuItem _copyMaster = new JMenuItem("Master consensus sequence");
    private JMenuItem _copyGroup = new JMenuItem("Group consensus sequence");
    private JMenuItem _snpToggle = new JMenuItem("Toggle SNP");
    private boolean _redrawAll = true;
    private boolean _redrawAllBlocked = false;
    private AnnotationTextField _annoEdit = new AnnotationTextField();
    private int[] _editPos = {0, 0};
    private Position _rightClickPos;
    private Annotation _currentAnnotation = null;
    private JColorChooser _chooser = new JColorChooser();

    private FlowFrame _flowViewer;

    public String toolTipText = "Test";
    public int seqNum = 1;

    public SequencePane() {
        super();
        int[] off = {0, 0};
        init(off, null);
    }

    public SequencePane(int[] offset, EditorInterface parent) {
        super();
        init(offset, parent);
    }

    public void setParent(EditorInterface parent) {
        _parent = parent;
        _flowViewer.flowPanel.setParent(parent);
    }

    public void setOffset(int[] offset) {
        _offset = offset;
    }

    public void sequenceLoaded() {
        _width = getWidth();
        _height = getHeight();
        _pixels = new int[_width * _height];
        _overlayPixels = new int[_width * _height];
        _source = new MemoryImageSource(_width, _height, _pixels, 0, _width);
        _source.setAnimated(true);
        _overlay = new MemoryImageSource(_width, _height, _overlayPixels, 0, _width);
        _overlay.setAnimated(true);
        _disp = Toolkit.getDefaultToolkit().createImage(_source);
    }

    public void resized() {
        sequenceLoaded();
        redrawAll();
        repaint();
    }

    public void updateFlowgrams(boolean fromWindow) {
        if(_parent == null)
            return;
        if(fromWindow && _flowViewer.isVisible()) {
            _flowViewer.setVisible(false);
            return;
        }
        if(!fromWindow && !_flowViewer.isVisible())
            return;
        _parent.findSNPs();
        int maxlength = 1000;
        for(Primer p : _parent.getAlignment().getPyroPrimers()) {
            maxlength = Math.min(maxlength, p.getMaxFlowgramLength())+20;
        }
        _flowViewer.setMaxLength(maxlength);
        _flowViewer.flowPanel.recalc();
        _flowViewer.setVisible(true);
        _flowViewer.repaint();
        _flowViewer.flowPanel.repaint();
    }

    private void init(int[] offset, EditorInterface parent) {
        InputStream stream = this.getClass().getResourceAsStream("/EditorName.png");
        addMouseWheelListener(this);
        try {
            _logoText = ImageIO.read(stream);
        } catch(Exception e) {
            e.printStackTrace();
        }

        stream = this.getClass().getResourceAsStream("/RKILogo.png");
        try {
            _logoRKI = ImageIO.read(stream);
        } catch(Exception e) {
            e.printStackTrace();
        }
        _flowViewer = new FlowFrame();
        _flowViewer.setParent(this);
        _parent = parent;
        _offset = offset;
        _width = getWidth();
        _height = getHeight();
        _pixels = new int[_width * _height];
        _overlayPixels = new int[_width * _height];
        setLayout(new AbsoluteLayout());
        _source = new MemoryImageSource(_width, _height, _pixels, 0, _width);
        _source.setAnimated(true);
        _overlay = new MemoryImageSource(_width, _height, _overlayPixels, 0, _width);
        _overlay.setAnimated(true);
        _disp = Toolkit.getDefaultToolkit().createImage(_source);
        _offset = offset;
        _parent = parent;
//        _flowViewer = new FlowViewer(_parent);
        setFocusable(true);
        requestFocus();
//        addMouseMotionListener(_parent);
//        addMouseListener(_parent);
//        addKeyListener(_parent);
        JMenuItem cmmenu = new JMenu("Color model");
        for (String name : ColorModels.models.keySet()) {
            JMenuItem jm = new ColorModelMenuItem(name);
            jm.addActionListener(this);
            cmmenu.add(jm);
        }
        JMenuItem ptmenu = new JMenu("Text size");
        for (int size : Letters.sizes) {
            JMenuItem jm = new SizeMenuItem(new Integer(size).toString());
            jm.addActionListener(this);
            ptmenu.add(jm);
        }


        JMenuItem tripmenu = new JMenu("Triplet grouping");
        JMenuItem it = new TripletMenuItem("None");
        it.addActionListener(this);
        tripmenu.add(it);

        it = new TripletMenuItem("+0");
        it.addActionListener(this);
        tripmenu.add(it);

        it = new TripletMenuItem("+1");
        it.addActionListener(this);
        tripmenu.add(it);

        it = new TripletMenuItem("+2");
        it.addActionListener(this);
        tripmenu.add(it);



        _deleteSelected.addActionListener(this);
        _annotationAbove.addActionListener(this);
        _annotationBelow.addActionListener(this);
        _annotation.add(_annotationAbove);
        _annotation.add(_annotationBelow);
//        if(_parent != null) {// && !_parent.isApplication()) {
            _primer.add(_primerAbove);
            _primer.add(_primerBelow);
            _primerAbove.addActionListener(this);
            _primerBelow.addActionListener(this);
//        }
        _loadFile.addActionListener(this);
        _addSequences.addActionListener(this);
        _loadFileRKI.addActionListener(this);
        _addSequencesRKI.addActionListener(this);
        _saveFileRKI.addActionListener(this);
        _saveFileRKIas.addActionListener(this);

        _copy.add(_copySequences);
        _copy.add(_copySequencesWithNames);
        _copy.add(_copyMaster);
        _copy.add(_copyGroup);
        _copySequences.addActionListener(this);
        _copyMaster.addActionListener(this);
        _copyGroup.addActionListener(this);
        _copySequencesWithNames.addActionListener(this);

        _snpToggle.addActionListener(this);

        if(_parent != null && !_parent.isApplication()) {
            _menu.add(_loadFile);
            _menu.add(_addSequences);
            _menu.add(_loadFileRKI);
            _menu.add(_addSequencesRKI);
            _menu.add(_saveFileRKI);
            _menu.add(_saveFileRKIas);
        }
        _menu.add(_deleteSelected);
        _menu.add(_copy);
        if(_parent != null && !_parent.isApplication())
            _menu.add(cmmenu);
        _menu.add(ptmenu);
        if(_parent != null && !_parent.isApplication()) {
            _menu.add(tripmenu);
        }
        _menu.add(_annotation);
//        if(_parent != null) {// && !_parent.isApplication()) {
            _menu.add(_primer);
//        }
        _menu.add(_snpToggle);
        addMouseListener(this);
        _annoEdit.addActionListener(this);

        _removeAnnotation.addActionListener(this);
        _recolorAnnotation.addActionListener(this);
        _degeneratePrimer.addActionListener(this);
        _degeneratePrimerGroup.addActionListener(this);
        _copyPrimerSequence.addActionListener(this);
        _copyPrimer.addActionListener(this);
        _copyPrimers.addActionListener(this);
        _sequencingPrimer.addActionListener(this);
        _copyProductWithPrimersGaps.addActionListener(this);
        _copyProductWithoutPrimersGaps.addActionListener(this);
        _copyProductWithoutPrimersWithoutGaps.addActionListener(this);
        _copyProductWithPrimersWithoutGaps.addActionListener(this);
        _annotationMenu.add(_removeAnnotation);
        _annotationMenu.add(_recolorAnnotation);
        _copyProduct.add(_copyProductWithPrimersGaps);
        _copyProduct.add(_copyProductWithPrimersWithoutGaps);
        _copyProduct.add(_copyProductWithoutPrimersGaps);
        _copyProduct.add(_copyProductWithoutPrimersWithoutGaps);
        _primerMenu.add(_degeneratePrimer);
        _primerMenu.add(_degeneratePrimerGroup);
        _primerMenu.add(_copyPrimerSequence);
        _primerMenu.add(_copyPrimer);
        _primerMenu.add(_copyPrimers);
        _primerMenu.add(_sequencingPrimer);
        _primerMenu.add(_removeAnnotation);
        _primerMenu.add(_recolorAnnotation);
        _primerMenu.add(_copyProduct);
        updateCopyMenues();

//        _timer = new Timer();
//        _timer.schedule(new RedrawTask(this), 10, 1000/25);
    }

    public void updateCopyMenues() {
        if(_parent == null)
            return;
        if(_parent.getMasterSequenceAll() == null) {
            _copyMaster.setEnabled(false);
        }
        else {
            _copyMaster.setEnabled(true);
        }
        if(!_parent.getMasterSequences().isEmpty()) {
            _copyGroup.setEnabled(true);
        }
        else {
            _copyGroup.setEnabled(false);
        }
        if(_parent.isSequenceSelected()) {
            _copySequences.setEnabled(true);
            _copySequencesWithNames.setEnabled(true);
        }
        else {
            _copySequences.setEnabled(false);
            _copySequencesWithNames.setEnabled(false);
        }
    }
    
    private void clear(int[] data) {
        for (int i = 0; i < data.length; i++) {
            data[i] = 0xffffffff;
        }
    }

    private void writeLetter(int[] data, int x, int y, int[] letter, int color, int bgcolor) {
        double tmp_lw = (int)_parent.getLetterWidth();
        int lw = tmp_lw==0?1:(int)tmp_lw;
        int lh = _parent.getLetterHeight();
        if (x + lw + (_width * (y + lw)) >= data.length || x + lw >= _width) {
            return;
        }
        for (int i = 0; i < lw; i++) {
            if (x + i >= _width) {
                return;
            }
            for (int j = 0; j < lh && x + i + (_width * (y + j)) < data.length; j++) {
                data[x + i + (_width * (y + j))] = (color * letter[i + lw * j] + bgcolor * (1 - letter[i + lw * j])) | 0xff000000;
            }
        }
    }

    private void updateCursor(Graphics g) {
        double tmp_lw = (int)_parent.getLetterWidth();
        int lw = tmp_lw==0?1:(int)tmp_lw;
        int lh = _parent.getLetterHeight();
        int ypos = _parent.getScreenY(_cursorY);
        int xpos = _parent.getScreenX(_cursorX);
        int xto = _parent.getScreenX(_cursorX + 1);
//        int ypos = (_cursorY-_offset[1]+1)*lw+_parent.rulerHeight;
//        int xpos = (_cursorX-_offset[0])*lw;
//        if(_parent.getAlignment().tripletGrouping != -1) {
//            xpos += (((_parent.getAlignment().tripletGrouping+_offset[0])%3+_cursorX)/3)*_parent.tripletDist;
//        }
        g.setColor(new Color(190, 190, 190));
        g.drawLine(0, ypos, _width, ypos);
        g.drawLine(xpos, _parent.getRulerHeight()-2, xpos, _height);
        g.setFont(new Font("Arial", Font.PLAIN, 12));
        String valS = new Integer(_cursorX).toString();
        FontMetrics metrics = g.getFontMetrics(g.getFont());
        int w = metrics.stringWidth(valS);
        int h = metrics.getHeight();
        g.drawString(valS, xpos-w/2, h-3);
        if(_parent.getMode() == Editor.MODE_EDITING) {
            g.setColor(new Color(200, 200, 255, 70));
            g.fillRect(xpos, ypos-lh, xto-xpos, lh);
            g.setColor(Color.BLUE);
            g.drawRect(xpos, ypos-lh, xto-xpos, lh);
        }
    }

    private void drawMarkerBox(Graphics g, int[] from, int[] to, Color border, Color fill, boolean canResize, boolean active) {
        int xpos0 = _parent.getScreenX(from[0]);
        int ypos0 = _parent.getScreenY(from[1]);
        int xpos1 = _parent.getScreenX(to[0]);
        int ypos1 = _parent.getScreenY(to[1]-1);
        if(xpos0 > xpos1) {
            int tmp = xpos0;
            xpos0 = xpos1;
            xpos1 = tmp;
        }
        if(ypos0 > ypos1) {
            int tmp = ypos0;
            ypos0 = ypos1;
            ypos1 = tmp;
        }
        // Draw selection box around the selected sequences
        ypos0 = Math.max(_parent.getOffsetY(), ypos0);
        if(active) {
            g.setColor(new Color(fill.getRed(), fill.getGreen(), fill.getBlue(), fill.getAlpha() + 50));
        }
        else {
            g.setColor(fill);
        }
        g.fillRect(xpos0, ypos0-_parent.getLetterHeight(), xpos1-xpos0, ypos1-ypos0+_parent.getLetterHeight());
        if(canResize && active) {
            g.setColor(border.brighter());
            g.drawRect(xpos0+2, ypos0-_parent.getLetterHeight()+2, xpos1-xpos0-4, ypos1-ypos0+_parent.getLetterHeight()-4);
        }
        g.setColor(border);
        g.drawRect(xpos0, ypos0-_parent.getLetterHeight(), xpos1-xpos0, ypos1-ypos0+_parent.getLetterHeight());
        // Draw selection box around master consensus sequence, if any
        if(_parent.getMasterSequenceAll() != null) {
            g.setColor(fill);
            g.fillRect(xpos0, _parent.getRulerHeight()+5, xpos1-xpos0, _parent.getLetterHeight());
            g.setColor(border);
            g.drawRect(xpos0, _parent.getRulerHeight()+5, xpos1-xpos0, _parent.getLetterHeight());
        }
    }

    private void updateSelection(Graphics g) {
        if(_parent.getSelFrom()[0] == _parent.getSelTo()[0] &&
                _parent.getSelFrom()[1] == _parent.getSelTo()[1]) {
            return;
        }
        else {
            drawMarkerBox(g, _parent.getSelFrom(), _parent.getSelTo(), Color.BLUE, new Color(200, 200, 255, 70), true, _parent.isSelectionActive());
        }
    }
    
    private void drawRuler(Graphics g) {
        int steps = 4;
        int bases = (int)(_width/_parent.getLetterWidth());
        int step = (int)(bases/steps);
        int first = 0;
        int goffset = 0;
        g.setColor(Color.BLACK);
        if(_offset[0]%step == 0) {
            first = _offset[0];
        }
        else {
            first = (int)(_offset[0]/step)*step;
            goffset = (int)((_offset[0]%step)*_parent.getLetterWidth());
        }
        g.drawLine(0, _parent.getRulerHeight()-1, _width, _parent.getRulerHeight()-1);
        g.setFont(new Font("Arial", Font.PLAIN, 12));
        for(int i=0; i<=steps; i++) {
            int val = first + i*step;
            String valS = new Integer(val).toString();
            FontMetrics metrics = g.getFontMetrics(g.getFont());
            int w = metrics.stringWidth(valS);
            int h = metrics.getHeight();
            int pos = (int)(_parent.getLetterWidth()*step*i - goffset + _parent.getLetterWidth()/2);
            if(_parent.getAlignment().tripletGrouping != -1) {
                pos += (((_parent.getAlignment().tripletGrouping+_offset[0])%3+step*i-goffset/_parent.getLetterWidth())/3)*_parent.getTripletDist();
                if(_offset[0]%3 == _parent.getAlignment().tripletGrouping)
                    pos += _parent.getTripletDist();
            }
            g.drawString(valS, pos-w/2, h-3);
            g.drawLine(pos, _parent.getRulerHeight()-1, pos, _parent.getRulerHeight()-3);
        }
    }
    
    private void drawConservation(int tripletGrouping, int ypos) {
        double[] conservation = _parent.getMasterSequenceAll().conservation;
        drawConservation(tripletGrouping, ypos, conservation);
    }
    
    private void drawConservation(int tripletGrouping, int ypos, double[] conservation) {
        double lw = _parent.getLetterWidth();
        int lh = _parent.getConsHeight();
        int ml =(int)Math.min(conservation.length, _offset[0] + _width / lw);
        int tripletSum = 0;
        for (int j = _offset[0]; j < ml; j++) {
            int ch = (int)(conservation[j]*lh);
            if(ch == 0) 
                ch = 1;
            if(tripletGrouping != -1) {
                if(j%3 == tripletGrouping) {
                    tripletSum += _parent.getTripletDist();
                }
            }
            int color = 0xff00ff00;
            if(conservation[j] == 1)
                color = 0xff00ff00;                
            else if(conservation[j] > 0.3)
                color = 0xffffac4b;
            else
                color = 0xffff0000;
            int xpos = (int)(((j - _offset[0]) * lw) + tripletSum);
            try {
                for (int x = 0; x < lw; x++) {
                    for (int y = 0; y < ch; y++) {
                        _pixels[xpos + x + (_width * (ypos + lh - y))] = color;
                    }
                }
            }
            catch(ArrayIndexOutOfBoundsException e) {
                
            }
        }
    }

    private int getMaximumLength(int seqlen) {
        return (int)Math.min(seqlen, _offset[0] + _width / _parent.getLetterWidth());
    }

    private void drawSequence(int tripletGrouping, BaseColorModel cm, Sequence s, int ypos, boolean highlight) {
        int[] colors = null;
        double lw = _parent.getLetterWidth();
        char[] seq = s.seq.toCharArray();
        int ml = getMaximumLength(s.seq.length());
        int tripletSum = 0;
        char[] masterSeq = null;
        if(_parent.getMasterSequenceAll() != null) {
            if(_parent.useIUB()) {
                masterSeq = _parent.getMasterSequenceAll().IUB.seq.toCharArray();
            }
            else {
                masterSeq = _parent.getMasterSequenceAll().seq.toCharArray();
            }
        }
        int lastDrawn = -1;
        for (int j = _offset[0]; j < ml; j++) {
            // Slower method of finding out which bases need to be drawn, but pixel perfect.
            if(lw < 1) {
                int npos = _parent.getScreenX(j);
                if(npos <= lastDrawn)
                    continue;
                lastDrawn = npos;
            }
            if(tripletGrouping != -1) {
                if(j%3 == tripletGrouping) {
                    tripletSum += _parent.getTripletDist();
                }
            }
            colors = cm.baseColor.get(seq[j]);
            if(colors == null) {
                colors = cm.baseColor.get('-');
            }
            int fgcolor = colors[0];
            int bgcolor = colors[1];
            // colors[2] == 0: Background color dominant in zoomed-out view
            if(lw < 8 && colors[2] == 0) {
                fgcolor = colors[1];
                bgcolor = colors[0];                
            }
            // Get the letter of appropriate size, or for size 1 if the size is too small.
            int[] letter = null;
            if(lw >= 1) {
                if(Letters.letters.containsKey(seq[j] + 100 * (int)lw)) {
                    letter = Letters.letters.get(seq[j] + 100 * (int)lw);
                }
                else {
                    letter = Letters.letters.get('?' + 100 * (int)lw);                    
                }
                if(highlight && _parent.getMasterSequenceAll() != null
                        && j<_parent.getMasterSequenceAll().conservation.length) {
                    if(colors[2] == 0) {
                        bgcolor = masterSeq[j] == seq[j]?0xffc0c0c0:bgcolor;
                        if(lw <= 4) {
                            fgcolor = masterSeq[j] == seq[j]?0xffc0c0c0:fgcolor;
                            bgcolor = 0xffffffff;
                        }
                    }
                    else {
                        fgcolor = masterSeq[j] == seq[j]?0xffc0c0c0:fgcolor;
                    }
                }
//                letter = Letters.letters.get('R' + 100 * (int)lw);
            }
            else {
                if(Letters.letters.containsKey(seq[j] + 100)) {
                    letter = Letters.letters.get(seq[j] + 100);
                }
                else {
                    letter = Letters.letters.get('?' + 100);                    
                }
                letter = Letters.letters.get(seq[j] + 100);
                bgcolor = 0xffffffff;
                if(_parent.getMasterSequenceAll() != null
                        && j<_parent.getMasterSequenceAll().conservation.length)
                    fgcolor = masterSeq[j] == seq[j]?0xffa0a0a0:0xff000000;
                else
                    fgcolor = 0xff000000;
            }
            writeLetter(_pixels, (int)((j - _offset[0]) * lw) + tripletSum, ypos-(int)_parent.getLetterHeight(), letter, fgcolor, bgcolor);
        }
    }

    private void drawSNPmarkers(Graphics g) {
        int ml = getMaximumLength(_parent.getAlignment().getLongestAll());
        for(SNP s : _parent.getAlignment().snps) {
            if(s.pos >= _offset[0] && s.pos < ml) {
                int[] from = {s.pos, 0};
                int[] to = {s.pos+1, _height};
                drawMarkerBox(g, from, to, Color.RED, new Color(255, 200, 200, 70), false, true);
            }
        }
    }

    @Override
    public synchronized void paint(Graphics g) {
//		int[] data = _disp.getRGB(0, 0, _width, _height, null, 0, _width);
        if(_parent == null) {
            g.setColor(Color.white);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.drawString("Please load a file", 10, 10);
            g.drawImage(_logoText, 20, 50, Color.RED, null);
//            BlingFilter bf = new BlingFilter(_lastFrame);
//            g.drawImage(bf.filter(_logoRKI, null), 450, 50+_logoRKI.getHeight()+10, Color.RED, null);
            g.drawImage(_logoRKI, 450, 50+_logoRKI.getHeight()+10, Color.RED, null);
//            _lastFrame += 10;
//            if(_lastFrame > _logoRKI.getWidth() + _logoRKI.getHeight() + 500) {
//                _lastFrame = 0;
//            }
            return;
        }
        if(_redrawAll) {
            double lw = _parent.getLetterWidth();
            int lh = _parent.getLetterHeight();
            Alignment align = _parent.getAlignment();
            clear(_pixels);
            BaseColorModel cm = ColorModels.models.get(ColorModels.currentModel);
            int ypos = _parent.getTopOffset();
            /**
             * Draw the sequences
             */
            for (int i = _offset[1]; i < align.countAll(); i++) {
                Sequence s = align.getSequence(i);
                Sequence ls = align.getSequence(i-1);
//                ypos += s.above.levels.size() * _parent.annotationHeight;
                if(ls != null && ls.group == s.group && s.collapsed) {
                    continue;
                }
                ypos = _parent.getScreenY(i);
                if(ypos > _height) {
                    break;
                }
                drawSequence(align.tripletGrouping, cm, s, ypos, true);
                if(ls == null || ls.group != s.group) {
                    if(_parent.getMasterSequences().get(s.group) != null) {
                        ypos = _parent.getScreenYConsensus(i);
                        drawSequence(align.tripletGrouping, cm, _parent.getMasterSequences().get(s.group), ypos-_parent.getConsOffset(), _redrawAll);
                        drawConservation(ypos, ypos-_parent.getConsOffset(), _parent.getMasterSequences().get(s.group).conservation);
                    }
                }
//                ypos += lh;
//                ypos += s.below.levels.size()*_parent.annotationHeight;
            }
            if(_parent.getMasterSequenceAll() != null) {
                if(_parent.useIUB()) {
                    drawSequence(align.tripletGrouping, cm, _parent.getMasterSequenceAll().IUB, _parent.getRulerHeight()+_parent.getLetterHeight()+5, false);
                }
                else {
                    drawSequence(align.tripletGrouping, cm, _parent.getMasterSequenceAll(), _parent.getRulerHeight()+_parent.getLetterHeight()+5, false);
                }
                drawConservation(align.tripletGrouping, _parent.getConsOffset()+5);
            }
            _source.newPixels();
            _redrawAllBlocked = false;
            _redrawAll = false;
        }
        g.drawImage(_disp, 0, 0, null);
        drawAnnotations(g);
        drawRuler(g);
        updateCursor(g);
        drawSNPmarkers(g);
        updateSelection(g);
        updateToolTip(g);
        if(_annoEdit.isAdded) {
            _annoEdit.paint(g.create(_editPos[0], _editPos[1], _annoEdit.getWidth(), _annoEdit.getHeight()));
        }
    }

    private void updateToolTip(Graphics g) {
        if(!_parent.showSequenceName())
            return;
        int ypos = _parent.getScreenY(seqNum);
        FontMetrics metrics = g.getFontMetrics();
        int hgt = metrics.getHeight();
        int adv = metrics.stringWidth(toolTipText);
        g.setColor(new Color(200,200,255,220));
        g.fillRect(0, ypos, adv+6, hgt+2);
        g.setColor(Color.GRAY);
        g.drawRect(0, ypos, adv+6, hgt+2);
        g.setColor(Color.BLACK);
        g.drawString(toolTipText, 3, ypos+hgt-1);
    }

    public int[] getAnnotationXpos(Annotation anno) {
        int[] res = {0, 0};
        res[0] = (int)((anno.from - _offset[0])*_parent.getLetterWidth());
        res[1] = (int)((anno.to - _offset[0])*_parent.getLetterWidth());
        return(res);
    }

    private Polygon getAnnoBorder(int[] annoXpos, int ypos, int lwi, boolean above, boolean primer) {
        Polygon annoBorder = new Polygon();
        if(above) {
            annoBorder.addPoint(annoXpos[0], ypos+_parent.getAnnotationHeight());
            if(primer) {
                annoBorder.addPoint(annoXpos[0], ypos);
                annoBorder.addPoint(annoXpos[1]-lwi, ypos);
            }
            else {
                annoBorder.addPoint(annoXpos[0]+lwi/2, ypos);
                annoBorder.addPoint(annoXpos[1]-lwi/2, ypos);
            }
            annoBorder.addPoint(annoXpos[1], ypos+_parent.getAnnotationHeight());
        }
        else {
            annoBorder.addPoint(annoXpos[1], ypos);
            if(primer) {
                annoBorder.addPoint(annoXpos[1], ypos+_parent.getAnnotationHeight());
                annoBorder.addPoint(annoXpos[0]+lwi, ypos+_parent.getAnnotationHeight());
            }
            else {
                annoBorder.addPoint(annoXpos[1]-lwi/2, ypos+_parent.getAnnotationHeight());
                annoBorder.addPoint(annoXpos[0]+lwi/2, ypos+_parent.getAnnotationHeight());
            }
            annoBorder.addPoint(annoXpos[0], ypos);
        }
        return annoBorder;
    }

    private Polygon getLeftHandle(int[] annoXpos, int ypos, int lwi, boolean above, boolean primer) {
        Polygon leftHandle = new Polygon();
        if(above) {
            leftHandle.addPoint(annoXpos[0], ypos+_parent.getAnnotationHeight());
            if(primer) 
                leftHandle.addPoint(annoXpos[0], ypos);
            else 
                leftHandle.addPoint(annoXpos[0]+lwi/2, ypos);
            leftHandle.addPoint(annoXpos[0]+lwi, ypos);
            leftHandle.addPoint(annoXpos[0]+lwi, ypos+_parent.getAnnotationHeight());
        }
        else {
            leftHandle.addPoint(annoXpos[1], ypos);
            if(primer)
                leftHandle.addPoint(annoXpos[1], ypos+_parent.getAnnotationHeight());
            else
                leftHandle.addPoint(annoXpos[1]-lwi/2, ypos+_parent.getAnnotationHeight());
            leftHandle.addPoint(annoXpos[1]-lwi, ypos+_parent.getAnnotationHeight());
            leftHandle.addPoint(annoXpos[1]-lwi, ypos);
        }
        return leftHandle;
    }

    private Polygon getRightHandle(int[] annoXpos, int ypos, int lwi, boolean above, boolean primer) {
        Polygon rightHandle = new Polygon();
        if(above) {
            if(primer)
                rightHandle.addPoint(annoXpos[1]-lwi, ypos);
            else
                rightHandle.addPoint(annoXpos[1]-lwi/2, ypos);
            rightHandle.addPoint(annoXpos[1], ypos+_parent.getAnnotationHeight());
            rightHandle.addPoint(annoXpos[1]-lwi, ypos+_parent.getAnnotationHeight());
            rightHandle.addPoint(annoXpos[1]-lwi, ypos);
        }
        else {
            if(primer)
                rightHandle.addPoint(annoXpos[0]+lwi, ypos+_parent.getAnnotationHeight());
            else
                rightHandle.addPoint(annoXpos[0]+lwi/2, ypos+_parent.getAnnotationHeight());
            rightHandle.addPoint(annoXpos[0], ypos);
            rightHandle.addPoint(annoXpos[0]+lwi, ypos);
            rightHandle.addPoint(annoXpos[0]+lwi, ypos+_parent.getAnnotationHeight());
        }
        return rightHandle;
    }

    private void drawAnnotation(Graphics g, Annotation anno, int ypos, int lh, boolean above) {
        double lw = _parent.getLetterWidth();
        int lwi = (int)lw;
        int[] annoXpos = getAnnotationXpos(anno);

        drawBaseShape(g, anno, annoXpos, ypos, lw, lwi, above);
        int primerTmSize = 0;
        int primerLengthSize = 0;

        if(anno == _parent.getEditedAnnotation() && !((anno instanceof Primer) && _parent.isPrimerSequenceActive())) {
            int from = Math.max(annoXpos[0], 0);
            int to = Math.min(annoXpos[1], _width);
            int w = to-from;
            if(!_annoEdit.isAdded) {
                add(_annoEdit, new AbsoluteConstraints(from, ypos));
                _annoEdit.setBounds(from, ypos, _parent.getAnnotationHeight(), w);
                _annoEdit.isAdded = true;
            }
            _editPos[0] = annoXpos[0];
            _editPos[1] = ypos;
            _annoEdit.setPreferredSize(new Dimension(w, _parent.getAnnotationHeight()));
            _annoEdit.setSize(new Dimension(w, _parent.getAnnotationHeight()));
            doLayout();
            if(!_annoEdit.correctText) {
                _annoEdit.setFont(new Font("Arial", Font.PLAIN, lh-3));
                _annoEdit.setText(anno.text);
                _annoEdit.correctText = true;
                _annoEdit.requestFocus();
                _annoEdit.selectAll();
            }
        }
        else {
            if(lw > 2) {
                g.setColor(Color.BLACK);
                g.setFont(new Font("Arial", Font.PLAIN, _parent.getLetterHeight()));
                FontMetrics metrics = g.getFontMetrics(g.getFont());
                int w = metrics.stringWidth(anno.text);
                int h = metrics.getHeight();
                int dh = (_parent.getAnnotationHeight() - h)/2;
                if(!(anno instanceof Primer) || !_parent.isPrimerSequenceActive()) {
                    drawAnnotationName(g, anno, above, annoXpos, lw, w, ypos, lh, dh);
                }
                else {
                    Primer primer = (Primer)anno;
                    int move=above?0:2;
                    for(int i=0; i<primer.getSequence().length(); i++) {
                        g.drawString(primer.getSequence().substring(i, i+1), (primer.from-_offset[0]+i)*lwi, ypos+lh+dh+move);
                    }
                }
                if(anno instanceof Primer) {
                    Primer primer = (Primer)anno;
                    drawPrimerScores(g, primer, above, annoXpos, lwi, ypos);
                    if(anno == _parent.getEditedAnnotation()) {
                        g.setColor(Color.BLUE);
                        int dy = above?1:3;
                        g.drawRect(annoXpos[0]+_parent.getPrimerEditPos()*lwi-2, ypos+dy, lwi, lh);
                    }
                    if(lw >= 4 && _parent.isPrimerTmEnabled() && primer.getTm() > 0) {
                        primerTmSize = drawPrimerTm(g, primer, above, annoXpos, lwi, ypos);
                    }
                    if(lw >= 4 && _parent.isPrimerLengthEnabled()) {
                        primerLengthSize = drawPrimerLength(g, primer, above, annoXpos, lwi, ypos);
                    }
                }
            }
            if(anno instanceof Primer)
                drawPrimerProduct(g, (Primer)anno, above, annoXpos, ypos, lw, primerTmSize, primerLengthSize);
        }
    }

    private void drawBaseShape(Graphics g, Annotation anno, int[] annoXpos, int ypos, double lw, int lwi, boolean above) {
        boolean isPrimer = anno instanceof Primer;
        Color c = anno.background;
        Polygon annoBorder = getAnnoBorder(annoXpos, ypos, lwi, above, isPrimer);
        Polygon leftHandle = getLeftHandle(annoXpos, ypos, lwi, above, isPrimer);
        Polygon rightHandle = getRightHandle(annoXpos, ypos, lwi, above, isPrimer);

        g.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 100));
        g.fillPolygon(annoBorder);
        g.setColor(c);
        g.drawPolyline(annoBorder.xpoints, annoBorder.ypoints, annoBorder.npoints);
        g.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 40));
        if(lw > 1) {
            g.fillPolygon(leftHandle);
            g.fillPolygon(rightHandle);
        }
    }

    private void drawAnnotationName(Graphics g, Annotation anno, boolean above, int[] annoXpos, double lw, int w, int ypos, int lh, int dh) {
        int dpos = (int)(((anno.to-anno.from)*lw-w)/2);
        int textx = (int)((anno.from-_offset[0])*lw+dpos);
        if(textx+w > _width) {
            textx = Math.max(annoXpos[0]+1, _width-w);
        }
        else if(textx < 0) {
            textx = Math.min(annoXpos[1]-1-w, 0);
        }
        int move=above?0:2;
        g.drawString(anno.text, textx, ypos+lh+dh+move);
    }

    private void drawPrimerProduct(Graphics g, Primer primer, boolean above, int[] annoXpos, int ypos, double lw, int primerTmSize, int primerLengthSize) {
        if(primer.opposite == null) {
            return;
        }
        int[] oppositeXpos = getAnnotationXpos(primer.opposite);
        int startx = annoXpos[0]+(int)(primer.getSequence().length()*lw)+primerLengthSize+2;
        int starty = ypos+_parent.getAnnotationHeight()/2-2;
        int endx = oppositeXpos[0];

        if(Math.abs(startx - endx) < 4)
            return;
        Color dashc = Color.BLACK;
        Graphics2D g2d = (Graphics2D)g;
        Stroke oldStroke = g2d.getStroke();
        float dash_w = (float)lw/2<=2?2:(float)lw/2;
        float[] dash = {dash_w,dash_w};
        g2d.setColor(Color.BLACK);
        g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10, dash, 0));
        if(_parent.getSettings().getBoolean(Settings.SHOW_DTM_WARNING)) {
            if(Math.abs(primer.getTm() - primer.opposite.getTm()) > _parent.getSettings().getFloat(Settings.DTM_WARNING_TEMP)) {
                dashc = Color.decode(_parent.getSettings().get(Settings.DTM_WARNING_COLOR));
                g.setColor(dashc);
                g2d.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 10, dash, 0));
            }
        }
        if(above) {
            g.drawLine(startx, starty, endx, starty);
            g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));
            g.drawLine(startx, starty-2, startx, starty+2);
            g.drawLine(endx, starty-2, endx, starty+2);
            g2d.setStroke(oldStroke);

            NumberFormat formatter = new DecimalFormat("#");
            g.setFont(new Font("Arial", Font.PLAIN, (int)(0.9*_parent.getLetterHeight())));
            FontMetrics metrics = g.getFontMetrics(g.getFont());
            String plen = formatter.format(primer.opposite.from - primer.to);
            int w = metrics.stringWidth(plen);
            int leftSide = annoXpos[1] + primerLengthSize;
            int rightSide = oppositeXpos[0];
            if(leftSide < 0 && rightSide > w) {
                leftSide = 0;
            }
            if(rightSide > _width) {
                rightSide = _width;
            }
            int posx = leftSide/2+rightSide/2-w/2;
            if(posx < annoXpos[1] + primerLengthSize) {
                posx = annoXpos[1] + primerLengthSize;
            }
            Rectangle2D bounds = metrics.getStringBounds(plen, g);
            g.setColor(Color.WHITE);
            g.fillRect(posx-1, ypos, (int)bounds.getWidth()+1, (int)bounds.getHeight());
            g.setColor(dashc);
            g.drawRect(posx-2, ypos, (int)bounds.getWidth()+2, (int)bounds.getHeight()+2);
            g.setColor(Color.BLACK);
            g.drawString(plen, posx, ypos+(int)bounds.getHeight());
        }
        g2d.setStroke(oldStroke);
    }

    private void drawPrimerScores(Graphics g, Primer primer, boolean above, int[] annoXpos, int lwi, int ypos) {
        int[] scores = primer.getScores();
        int[] groupScores = primer.getGroupScores();
        for(int i=0; i<scores.length; i++) {
        float red = (1.f-(scores[i]/100.f))*1.f;
        float green = 1.f-red;
        Color color = new Color(red, green, 0.f, 1.f);
        g.setColor(color);
        if(above)
            g.fillRect(annoXpos[0]+i*lwi, ypos+_parent.getAnnotationHeight()-2, lwi, 2);
        else
            g.fillRect(annoXpos[0]+i*lwi, ypos, lwi, 2);
        }
        for(int i=0; i<groupScores.length; i++) {
            float blue = (1.f-(groupScores[i]/100.f))*1.f;
            float green = 1.f-blue;
            Color color = new Color(0.f, green, blue, 1.f);
            g.setColor(color);
            if(above)
                g.fillRect(annoXpos[0]+i*lwi, ypos+_parent.getAnnotationHeight()-4, lwi, 2);
            else
                g.fillRect(annoXpos[0]+i*lwi, ypos+2, lwi, 2);
        }
    }

    private int drawPrimerTm(Graphics g, Primer primer, boolean above, int[] annoXpos, int lwi, int ypos) {
        g.setColor(Color.BLACK);
        NumberFormat formatter = new DecimalFormat("#.0");
        g.setFont(new Font("Arial", Font.PLAIN, (int)(0.9*_parent.getLetterHeight())));
        FontMetrics metrics = g.getFontMetrics(g.getFont());
        String plen = formatter.format(primer.getTm());
        int w = metrics.stringWidth(plen);
        Rectangle2D bounds = metrics.getStringBounds(plen, g);
        if(above) {
            g.drawString(plen, annoXpos[0]-w, ypos+(int)bounds.getHeight());
        }
        else {
            g.drawString(plen, annoXpos[0]+primer.getSequence().length()*lwi+1, ypos+(int)bounds.getHeight());
        }
        return w;
    }

    private int drawPrimerLength(Graphics g, Primer primer, boolean above, int[] annoXpos, int lwi, int ypos) {
        g.setColor(Color.BLACK);
        NumberFormat formatter = new DecimalFormat("#");
        g.setFont(new Font("Arial", Font.PLAIN, (int)(0.9*_parent.getLetterHeight())));
        FontMetrics metrics = g.getFontMetrics(g.getFont());
        String tm = formatter.format(primer.getSequence().replace("-", "").length());
        int w = metrics.stringWidth(tm);
        Rectangle2D bounds = metrics.getStringBounds(tm, g);
        if(above) {
            g.drawString(tm, annoXpos[0]+primer.getSequence().length()*lwi, ypos+(int)bounds.getHeight());
        }
        else {
            g.drawString(tm, annoXpos[0]-w, ypos+(int)bounds.getHeight());
        }
        return w;
    }

    private void drawAnnotations(Graphics g) {
        int lh = _parent.getLetterHeight();
        Alignment align = _parent.getAlignment();
        int ypos = _parent.getTopOffset() + _parent.getLetterHeight();
        for (int i = _offset[1]; i < Math.min(align.countAll(), _offset[1]+_height/lh); i++) {
            Sequence s = align.getSequence(i);
            Sequence ls = align.getSequence(i-1);
            Sequence ns = align.getSequence(i+1);
            if(ls != null && s.group == ls.group && s.collapsed) {
                continue;
            }
            // -letterHeight for the sequence line, and another -annotationHeight for each
            // level of annotation
            ypos = _parent.getScreenY(i)-s.above.levels.size()*_parent.getAnnotationHeight() - _parent.getLetterHeight();
            for(LinkedList<Annotation> level : s.above.levels) {
                for(Annotation anno : level) {
                    drawAnnotation(g, anno, ypos, lh, true);
                }
                ypos += _parent.getAnnotationHeight();
            }
            ypos += lh;
            for(LinkedList<Annotation> level : s.below.levels) {
                for(Annotation anno : level) {
                    drawAnnotation(g, anno, ypos, lh, false);
                }
                ypos += _parent.getAnnotationHeight();
            }
        }
    }
    
    public int getImageWidth() {
        return(_width);
    }
    
    public void redrawCursor(int cursorX, int cursorY) {
        if(!_redrawAllBlocked) {
            _redrawAll = false;
        }
        _cursorX = cursorX;
        _cursorY = cursorY;
    }
    
    public void redrawAll() {
        _redrawAllBlocked = true;
        _redrawAll = true;
    }
    
    @Override
    public void update(Graphics g) {
        paint(g);
    }

    public void showPopup(MouseEvent e) {
        if(_parent == null)
            return;
        if (e.getButton() == 3) {
            if(_parent == null)
                return;
            updateCopyMenues();
            int[] pos = {e.getX(), e.getY()};
            Position realPos = _parent.getRealPos(pos);
            _rightClickPos = realPos;
            if(realPos.onAnnotation) {
                _primerMenu.add(_removeAnnotation);
                _primerMenu.add(_recolorAnnotation);
                _currentAnnotation = realPos.annotation;
                if(_currentAnnotation instanceof Primer) {
                    if(((Primer)(_currentAnnotation)).isPyro)
                        _sequencingPrimer.setSelected(true);
                    else
                        _sequencingPrimer.setSelected(false);
                    if(((Primer)(_currentAnnotation)).opposite != null)
                        _copyProduct.setEnabled(true);
                    else
                        _copyProduct.setEnabled(false);
                    _primerMenu.show(this, e.getX(), e.getY());
                }
                else {
                    _annotationMenu.add(_removeAnnotation);
                    _annotationMenu.add(_recolorAnnotation);
                    _annotationMenu.show(this, e.getX(), e.getY());
                }
                return;
            }
            if(!_parent.canAddAnnotation()) {
                _annotation.setEnabled(false);
                _primer.setEnabled(false);
            }
            else {
                _annotation.setEnabled(true); 
                _primer.setEnabled(true);
            }
            _menu.show(this, e.getX(), e.getY());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        showPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        showPopup(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void copySequences(boolean withNames) {
        Clipboard clip = getToolkit().getSystemClipboard();
        StringBuilder seqStringBuilder = new StringBuilder();
        for(int i=_parent.getSelFrom()[1]; i<_parent.getSelTo()[1]; i++) {
            Sequence seq = _parent.getAlignment().getSequence(i);
            if(withNames) {
                seqStringBuilder.append(">");
                seqStringBuilder.append(seq.name);
                seqStringBuilder.append(" (");
                seqStringBuilder.append(_parent.getSelFrom()[0]);
                seqStringBuilder.append(" - ");
                seqStringBuilder.append(_parent.getSelTo()[0]);
                seqStringBuilder.append(")\n");
            }
            seqStringBuilder.append(seq.seq.substring(_parent.getSelFrom()[0], _parent.getSelTo()[0]));
            seqStringBuilder.append("\n");
        }
        StringSelection contents = new StringSelection(seqStringBuilder.toString());
        clip.setContents(contents, this);
    }

    public void copyMasterConsensusSequence() {
        Clipboard clip = getToolkit().getSystemClipboard();
        String res = "";
        if(_parent.getSelFrom()[0] == 0 && _parent.getSelTo()[0] == 0) {
            res = _parent.getMasterSequenceAll().seq;

        }
        else {
            res = _parent.getMasterSequenceAll().seq.substring(_parent.getSelFrom()[0], _parent.getSelTo()[0]);
        }
        clip.setContents(new StringSelection(res), this);
    }

    public void copyGroupConsensusSequence() {
        Clipboard clip = getToolkit().getSystemClipboard();
        String res = _parent.getMasterSequences().get(_parent.getAlignment().getSequence(_parent.getSelFrom()[1]).group).seq;
        if(_parent.getSelFrom()[0] != 0 || _parent.getSelTo()[0] != 0) {
            res = res.substring(_parent.getSelFrom()[0], _parent.getSelTo()[0]);
        }
        clip.setContents(new StringSelection(res), this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == _annoEdit) {
            _parent.getEditedAnnotation().text = _annoEdit.getText();
            _annoEdit.correctText = false;
            _annoEdit.isAdded = false;
            remove(_annoEdit);
            _parent.setEditedAnnotation(null);
            doLayout();
            repaint();
        }
        else if(e.getSource() == _loadFile) {
            _parent.loadFile(true);
        }
        else if(e.getSource() == _loadFileRKI) {
            _parent.loadFileRKI(true);
        }
        else if(e.getSource() == _addSequencesRKI) {
            _parent.loadFileRKI(false);
        }
        else if(e.getSource() == _addSequences) {
            _parent.loadFile(false);
        }
        else if(e.getSource() == _saveFileRKI) {
            _parent.saveToRKITools(null);
        }
        else if(e.getSource() == _snpToggle) {
            _parent.getAlignment().toggleSnp(_rightClickPos.x);
        }
        else if(e.getSource() == _saveFileRKIas) {
            Frame parentFrame = _parent.getParentFrame();
            String name = JOptionPane.showInputDialog(parentFrame, "Please enter the new filename");
            if(name != null && name.length() != 0) {
                _parent.saveToRKITools(name);
            }
//            _parent.saveToRKITools();
        }
        else if(e.getSource() == _deleteSelected) {
            int from = Math.min(_parent.getSelFrom()[1], _parent.getSelTo()[1]);
            int to = Math.max(_parent.getSelFrom()[1], _parent.getSelTo()[1]);
            int xfrom = Math.min(_parent.getSelFrom()[0], _parent.getSelTo()[0]);
            int xto = Math.max(_parent.getSelFrom()[0], _parent.getSelTo()[0]);
            for(int i=from; i<to; i++) {
                _parent.getAlignment().getSequence(i).removePart(xfrom, xto);
            }
            _parent.setSelFrom(0, 0);
            _parent.setSelFrom(1, 0);
            _parent.setSelTo(0, 0);
            _parent.setSelTo(1, 0);
            _parent.consensusChange();
            _parent.repaintAll();
        }
        else if(e.getSource() == _removeAnnotation) {
            _currentAnnotation.list.removeAnnotation(_currentAnnotation);
            _currentAnnotation.list.resetAnnotations();
            redrawAll();
            repaint();
            updateFlowgrams(false);
        }
        else if(e.getSource() == _recolorAnnotation) {
            JDialog jc = JColorChooser.createDialog(this, TOOL_TIP_TEXT_KEY, true, _chooser,
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        _currentAnnotation.background = _chooser.getColor();
                        _parent.repaintAll();
                    }
                }
            , null);
            jc.setVisible(true);
        }
        else if(e.getSource() == _degeneratePrimer) {
            if(_parent.getMasterSequenceAll() == null) {
                _parent.getMasterConsensus();
            }
            Primer primer = (Primer)_currentAnnotation;
            primer.degenerate(_parent.getMasterSequenceAll().IUB.seq.substring(
                    primer.from, primer.to));
            repaint();
        }
        else if(e.getSource() == _degeneratePrimerGroup) {
            if(_parent.getMasterSequences().isEmpty()) {
                _parent.getGroupMasterSequences();
            }
            HashMap<GroupKey, MasterSequence> masterSequences = _parent.getMasterSequences();
            MasterSequence master = masterSequences.get(_currentAnnotation.list.sequence.group);
            Primer primer = (Primer)_currentAnnotation;
            primer.degenerate(master.IUB.seq.substring(primer.from, primer.to));
            repaint();
        }
        else if(e.getSource() == _copyPrimerSequence) {
            Primer primer = (Primer)_currentAnnotation;
            Clipboard clip = getToolkit().getSystemClipboard();
            if(primer.list.above)
                clip.setContents(new StringSelection(primer.getSequence()), this);
            else
                clip.setContents(new StringSelection(Sequence.invert(primer.getSequence())), this);
        }
        else if(e.getSource() == _copyPrimer) {
            Primer primer = (Primer)_currentAnnotation;
            Clipboard clip = getToolkit().getSystemClipboard();
            if(primer.list.above)
                clip.setContents(new StringSelection(">" + primer.text + "\n" + primer.getSequence()), this);
            else
                clip.setContents(new StringSelection(">" + primer.text + "\n" + Sequence.invert(primer.getSequence())), this);
        }
        else if(e.getSource() == _copyProductWithPrimersGaps
                || e.getSource() == _copyProductWithPrimersWithoutGaps
                || e.getSource() == _copyProductWithoutPrimersGaps
                || e.getSource() == _copyProductWithoutPrimersWithoutGaps) {
            Primer primer = (Primer)_currentAnnotation;
            Clipboard clip = getToolkit().getSystemClipboard();
            if(primer.list.above && e.getSource() == _copyProductWithPrimersGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.from, primer.opposite.to)), this);
            else if(!primer.list.above && e.getSource() == _copyProductWithPrimersGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.opposite.from, primer.to)), this);
            else if(primer.list.above && e.getSource() == _copyProductWithoutPrimersGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.to, primer.opposite.from)), this);
            else if(!primer.list.above && e.getSource() == _copyProductWithoutPrimersGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.opposite.to, primer.from)), this);
            else if(primer.list.above && e.getSource() == _copyProductWithPrimersWithoutGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.from, primer.opposite.to).replace("-", "")), this);
            else if(!primer.list.above && e.getSource() == _copyProductWithPrimersWithoutGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.opposite.from, primer.to).replace("-", "")), this);
            else if(primer.list.above && e.getSource() == _copyProductWithoutPrimersWithoutGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.to, primer.opposite.from).replace("-", "")), this);
            else if(!primer.list.above && e.getSource() == _copyProductWithoutPrimersWithoutGaps)
                clip.setContents(new StringSelection(primer.list.sequence.seq.substring(primer.opposite.to, primer.from).replace("-", "")), this);
        }
        else if(e.getSource() == _copyPrimers) {
            Clipboard clip = getToolkit().getSystemClipboard();
            StringBuilder primerSeqs = new StringBuilder();
            for(Sequence seq : _parent.getAlignment().getSequencesAll()) {
                for(Annotation anno : seq.above.getAnnotations()) {
                    if(anno instanceof Primer) {
                        Primer primer = (Primer)anno;
                        primerSeqs.append(">");
                        primerSeqs.append(primer.text);
                        primerSeqs.append("\n");
                        primerSeqs.append(primer.getSequence());
                        primerSeqs.append("\n");
                    }
                }
                for(Annotation anno : seq.below.getAnnotations()) {
                    if(anno instanceof Primer) {
                        Primer primer = (Primer)anno;
                        primerSeqs.append(">");
                        primerSeqs.append(primer.text);
                        primerSeqs.append("\n");
                        primerSeqs.append(Sequence.invert(primer.getSequence()));
                        primerSeqs.append("\n");
                    }
                }
            }
            clip.setContents(new StringSelection(primerSeqs.toString()), this);
        }
        else if(e.getSource() == _sequencingPrimer) {
            Primer primer = (Primer)_currentAnnotation;
            primer.isPyro = _sequencingPrimer.isSelected();
            _parent.updateFlowgrams(false);
        }
        else if (e.getSource().getClass() == ColorModelMenuItem.class) {
            JMenuItem jm = (JMenuItem) e.getSource();
            String name = jm.getText();
            ColorModels.currentModel = name;
            _parent.setColorOpts(name);
            _parent.repaintAll();
        }
        else if (e.getSource().getClass() == SizeMenuItem.class) {
            JMenuItem jm = (JMenuItem) e.getSource();
            int size = Integer.parseInt(jm.getText());
            _parent.setLetterWidth(size);
        }
        else if(e.getSource().getClass() == TripletMenuItem.class) {
            JMenuItem jm = (JMenuItem) e.getSource();
            if(jm.getText().equals("None")) {
                _parent.getAlignment().tripletGrouping = -1;
            }
            else if(jm.getText().equals("+0")) {
                _parent.getAlignment().tripletGrouping = 0;
            }
            else if(jm.getText().equals("+1")) {
                _parent.getAlignment().tripletGrouping = 1;
            }
            else if(jm.getText().equals("+2")) {
                _parent.getAlignment().tripletGrouping = 2;
            }
            _parent.repaintAll();
        }
        else if(e.getSource() == _annotationAbove) {
            _parent.addAnnotation(true, false);
        }
        else if(e.getSource() == _annotationBelow) {
            _parent.addAnnotation(false, false);
        }
        else if(e.getSource() == _primerAbove) {
            _parent.addAnnotation(true, true);
            _parent.updateFlowgrams(false);
        }
        else if(e.getSource() == _primerBelow) {
            _parent.addAnnotation(false, true);
            _parent.updateFlowgrams(false);
        }
        else if(e.getSource() == _copySequences || e.getSource() == _copySequencesWithNames) {
            Clipboard clip = getToolkit().getSystemClipboard();
            StringBuilder seqStringBuilder = new StringBuilder();
            for(int i=_parent.getSelFrom()[1]; i<_parent.getSelTo()[1]; i++) {
                Sequence seq = _parent.getAlignment().getSequence(i);
                if(e.getSource() == _copySequencesWithNames) {
                    seqStringBuilder.append(">");
                    seqStringBuilder.append(seq.name);
                    seqStringBuilder.append(" (");
                    seqStringBuilder.append(_parent.getSelFrom()[0]);
                    seqStringBuilder.append(" - ");
                    seqStringBuilder.append(_parent.getSelTo()[0]);
                    seqStringBuilder.append(")\n");
                }
                seqStringBuilder.append(seq.seq.substring(_parent.getSelFrom()[0], _parent.getSelTo()[0]));
                seqStringBuilder.append("\n");
            }
            StringSelection contents = new StringSelection(seqStringBuilder.toString());
            clip.setContents(contents, this);
        }
        else if(e.getSource() == _copyMaster) {
            copyMasterConsensusSequence();
        }
        else if(e.getSource() == _copyGroup) {
            copyGroupConsensusSequence();
        }
    }

    @Override
    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        _parent.mouseWheelMoved(e);
    }

    private class RedrawTask extends TimerTask {
        private SequencePane _pane;
        public RedrawTask(SequencePane pane) {
            super();
            _pane = pane;
        }

        @Override
        public void run() {
            _pane.repaint();
        }
    }
}
