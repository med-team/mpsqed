/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.io.File;
import javax.swing.JMenuItem;
import org.rki.sequenceeditor.model.filters.LoadFilter;

/**
 *
 * @author dabrowskiw
 */
public class RecentMenuItem extends JMenuItem {

    public String filename;
    public LoadFilter loadFilter;

    public RecentMenuItem(String fname, LoadFilter filter) {
        super(new File(fname).getName());
        filename = fname;
        loadFilter = filter;
    }
}
