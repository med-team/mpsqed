/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.Frame;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.util.HashMap;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.Annotation;
import org.rki.sequenceeditor.model.GroupKey;
import org.rki.sequenceeditor.model.MasterSequence;
import org.rki.sequenceeditor.model.Position;
import org.rki.sequenceeditor.model.Sequence;
import org.rki.sequenceeditor.model.Settings;
import org.rki.sequenceeditor.model.filters.LoadFilter;

/**
 *
 * @author dabrowskiw
 */
public interface EditorInterface extends MouseMotionListener, MouseListener, KeyListener, FocusListener, AdjustmentListener {
    public boolean isApplication();
    public boolean hasMenu();
    public boolean showSequenceName();
    public boolean useIUB();

    public int getAppletWidth();
    public int getAppletHeight();
    public Alignment getAlignment();
    public int getLetterHeight();
    public double getLetterWidth();
    public int getConsOffset();
    public int getOffsetY();
    public int getTopOffset();

    public int getTripletDist();
    public int getConsHeight();

    public int getScreenY(int pos);
    public int getScreenYConsensus(int pos);
    public int getScreenX(int pos);
    public int getRulerHeight();

    public Position getRealPos(int[] screenPos);
    public void setMasterSequence(MasterSequence master, GroupKey group);
    public void setMasterSequenceAll(MasterSequence master);

    public void repaintAll();

    public void getMasterConsensus();
    public void getGroupMasterSequences();
    public void getConsensus(GroupKey group);

    public int[] getSelFrom();
    public int[] getSelTo();

    public int getMode();

    public int getAnnotationHeight();

    public Annotation getEditedAnnotation();
    public void setEditedAnnotation(Annotation anno);
    public HashMap<GroupKey, MasterSequence> getMasterSequences();
    public MasterSequence getMasterSequenceAll();
    public Frame getParentFrame();

    public void loadFile(boolean replace);
    public boolean loadFileRKI(boolean replace);
    public void saveToRKITools(String name);
    public void saveFile();
    public void saveFileAs();
    public String seqsToString(boolean pureFASTA);
    public void findSNPs();
    public void addAnnotation(boolean above, boolean primer);
    public void setColorOpts(String name);
    public void setLetterWidth(int size);
    public void setSelFrom(int index, int value);
    public void setSelTo(int index, int value);

    public void mouseWheelMoved(MouseWheelEvent e);

    public boolean isSelectionActive();
    public boolean isPrimerSequenceActive();
    public boolean isPrimerTmEnabled();
    public boolean isPrimerLengthEnabled();

    public int getPrimerEditPos();

    public boolean canAddAnnotation();
    public boolean isSequenceSelected();

    public void consensusChange();

    public Settings getSettings();

    public void updateFlowgrams(boolean fromWindow);

    public void loadFile(boolean overwrite, LoadFilter filter, String filename);
}
