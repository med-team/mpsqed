/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;

/**
 *
 * @author dabrowskiw
 */
public class BlingFilter implements BufferedImageOp {

    private int _step;

    public BlingFilter(int step) {
        _step = step;
    }

    private void drawGradient(int x, int width, int imageWidth, int imageHeight, Graphics2D g) {
        for(int i=x-width; i<x+width; i++) {
//            if(i<0)
//                continue;
            float alpha = 0.5f*(1-(float)((i-x)*(i-x))/(float)(width*width));
            g.setColor(new Color(1.0f, 1.0f, 1.0f, alpha));
            g.drawLine(i, 0, i-width, imageHeight);
        }
    }

    @Override
    public BufferedImage filter(BufferedImage arg0, BufferedImage arg1) {
        BufferedImage tmp = new BufferedImage(arg0.getWidth(), arg0.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = tmp.createGraphics();
        g.drawImage(arg0, 0, 0, null);
//        g.setColor(new Color(0.0f, 1.0f, 0.0f, 0.5f));
//        g.drawLine(0+_step, 0, 50+_step, 50);
        drawGradient(_step, arg0.getHeight(), arg0.getWidth(), arg0.getHeight(), g);
        return tmp;
    }

    @Override
    public Rectangle2D getBounds2D(BufferedImage arg0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BufferedImage createCompatibleDestImage(BufferedImage arg0, ColorModel arg1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Point2D getPoint2D(Point2D arg0, Point2D arg1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public RenderingHints getRenderingHints() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
