/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import javax.swing.JMenuItem;
import org.rki.sequenceeditor.model.GroupKey;

/**
 *
 * @author dabrowskiw
 */
public class GroupMenuItem extends JMenuItem {
    public GroupKey group;
    
    public GroupMenuItem() {
        super();
    }
    
    public GroupMenuItem(String text, GroupKey group) {
        super(text);
        this.group = group;
    }
    
}
