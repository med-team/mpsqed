/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.view;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import org.json.JSONArray;
import org.json.JSONObject;
import org.rki.sequenceeditor.model.RKIFile;
import org.rki.sequenceeditor.model.RKIFolder;

/**
 *
 * @author dabrowskiw
 */
public class RKIFilesDialog extends Dialog implements ActionListener, TreeSelectionListener, ListSelectionListener {

    private JTree _tree;
    private JList _list = new JList();
    private JButton _accept = new JButton("Accept");
    private JButton _cancel = new JButton("Cancel");
    private Editor _editor;
    private DefaultListModel _lm = new DefaultListModel();
    private JScrollPane _scroll;

    public boolean accepted = false;

    public RKIFilesDialog(Frame parent, String name, DefaultMutableTreeNode root, Editor editor) {
        super(parent, name, true);
        _tree = new JTree(root);
        _editor = editor;
        _tree.setPreferredSize(new Dimension(300, 500));
        _tree.addTreeSelectionListener(this);
        _list.setModel(_lm);
        _scroll = new JScrollPane(_list);
        _scroll.setPreferredSize(new Dimension(300, 500));
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(2,2,2,2);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbl.setConstraints(_tree, gbc);
        add(_tree);
        gbc.gridx = 1;
        gbl.setConstraints(_scroll, gbc);
        add(_scroll);
        gbc.fill = GridBagConstraints.VERTICAL;
        gbc.insets = new Insets(0,0,0,0);
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.EAST;
        gbl.setConstraints(_cancel, gbc);
        add(_cancel);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbl.setConstraints(_accept, gbc);
        add(_accept);
        pack();

        _accept.addActionListener(this);
        _cancel.addActionListener(this);
        _list.addListSelectionListener(this);
        _accept.setEnabled(false);
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == _cancel) {
            accepted = false;
            setVisible(false);
        }
        else if(e.getSource() == _accept) {
            setVisible(false);
            accepted = true;
        }
    }

    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)_tree.getSelectionPath().getLastPathComponent();
        RKIFolder folder = (RKIFolder)node.getUserObject();
        try {
            String data = _editor.readFromURL(_editor.baseUrl + "nodefiles/" + folder.id + "/");
            JSONObject ob = new JSONObject(data);
            ArrayList<RKIFile> files = new ArrayList<RKIFile>();
            Iterator<String> keys = ob.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                if(!key.startsWith("fasta")) {
                    continue;
                }
                JSONObject fileso = ob.getJSONObject(key);
                JSONArray far = fileso.getJSONArray("files");
                for(int i=0; i<far.length(); i++) {
                    files.add(new RKIFile(far.getJSONObject(i).getString("name"), far.getJSONObject(i).getInt("id")));
                }
            }
            _accept.setEnabled(false);
            _lm.clear();
            for(RKIFile file : files) {
                _lm.addElement(file);
            }
            System.out.println("Bla");
        } catch(Exception ex) {}
    }

    public void valueChanged(ListSelectionEvent e) {
        _accept.setEnabled(true);
    }

    public RKIFile getSelectedFile() {
        return((RKIFile)_list.getSelectedValue());
    }
}
