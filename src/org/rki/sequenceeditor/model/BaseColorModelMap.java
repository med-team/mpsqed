/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.util.HashMap;

/**
 *
 * @author dabrowskiw
 */
public class BaseColorModelMap {
    private HashMap<Character, int[]> _colorModelMap = new HashMap<Character, int[]>();
    
    public BaseColorModelMap() {}
    
    public void put(Character base, int[] model) {
        _colorModelMap.put(base, model);
    }
    
    public int[] get(Character base) {
        try {
            return _colorModelMap.get(base);
        } catch(Exception e) {
            return _colorModelMap.get('?');
        }
    }
}
