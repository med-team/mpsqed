/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

/**
 *
 * @author dabrowskiw
 */
public class MasterSequence extends Sequence {
    public double[] conservation;
    public Sequence IUB;
    
    public MasterSequence(String name, String seq, GroupKey group, Alignment align) {
        super(name, seq, group, align);
    }

    public MasterSequence(Sequence s) {
        super(s.name, s.seq, s.group, s.align);
    }
    
    @Override
    public int compareTo(Object o) {
        if(!(o instanceof MasterSequence)) {
            return 0;
        }
        MasterSequence s = (MasterSequence)o;
        System.out.println(s.group.compareTo(group));
        return s.group.compareTo(group);
    }
}
