package org.rki.sequenceeditor.model;

import java.util.Collection;
import java.util.LinkedList;

public class AnnotationList {
    public boolean above;
    public LinkedList<LinkedList<Annotation>> levels = new LinkedList<LinkedList<Annotation>>();
    public Sequence sequence;

    public AnnotationList(boolean above, Sequence seq) {
        this.above = above;
        this.sequence = seq;
    }
    
    private void addLevelWithAnnotation(Annotation anno) {
        LinkedList<Annotation> level = new LinkedList<Annotation>();
        level.add(anno);
        levels.add(level);
    }

    private void addAnnotation(Annotation anno, boolean isNew) {        
        if(levels.size() == 0) {
            addLevelWithAnnotation(anno);
            return;
        }
        boolean added = false;
        for(LinkedList<Annotation> level : levels) {
            boolean hitOnLevel = false;
            for(Annotation a : level) {
                if(a == anno) {
                    continue;
                }
                if((anno.from >= a.from && anno.from <= a.to) || (anno.to >= a.from && anno.to <= a.to) ||
                   (a.from >= anno.from && a.from <= anno.to) || (a.to >= anno.from && a.to <= anno.to)) {
                    hitOnLevel = true;
                    break;
                }
            }
            if(!hitOnLevel) {
                added = true;
                if(!isNew) {
                    removeAnnotation(anno);
                }
                level.add(anno);
                break;
            }
        }
        if(!added) {
            if(!isNew) {
                removeAnnotation(anno);
            }
            addLevelWithAnnotation(anno);
        }
    }
    
    public void addAnnotation(Annotation anno) {
        anno.list = this;
        addAnnotation(anno, true);
    }

    public void removeAnnotation(Annotation anno) {
        for(LinkedList<Annotation> level : levels) {
            boolean found = false;
            for(Annotation a : level) {
                if(a == anno) {
                    found = true;
                    break;
                }
            }
            if(found) {
                level.remove(anno);
                break;
            }
        }
        if(anno instanceof Primer) {
            sequence.updateProducts();
        }
    }
    
    public void resetAnnotation(Annotation anno) {
        addAnnotation(anno, false);
    }
    
    public void resetAnnotations() {
        LinkedList<LinkedList<Annotation>> tmplevels = new LinkedList<LinkedList<Annotation>>();
        for(LinkedList<Annotation> level : levels) {
            tmplevels.add((LinkedList<Annotation>)(level.clone()));
        }
        for(LinkedList<Annotation> level : tmplevels) {
            for(Annotation anno : level) {
                resetAnnotation(anno);
            }
        }
        cleanupLevels();
    }
    
    private void cleanupLevels() {        
        LinkedList<LinkedList<Annotation>> toRemove = new LinkedList<LinkedList<Annotation>>();
        for(LinkedList<Annotation> level : levels) {
            if(level.size() == 0) {
                toRemove.add(level);
            }
        }
        for(LinkedList<Annotation> level : toRemove) {
            levels.remove(level);
        }
    }

    public Collection<Annotation> getAnnotations() {
        LinkedList<Annotation> annotations = new LinkedList<Annotation>();
        for(LinkedList<Annotation> level : levels) {
            annotations.addAll(level);
        }
        return annotations;
    }
}
