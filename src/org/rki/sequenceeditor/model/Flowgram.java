/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author dabrowskiw
 */
public class Flowgram implements Comparable<Flowgram> {
    private char[] _order;
    private ArrayList<Integer> _signals = new ArrayList<Integer>();
    private Color[] _colors = {Color.RED, Color.GREEN, Color.BLUE, Color.ORANGE};
    private String _name;

    public Flowgram(char[] seq, char[] order, int len, String name) {
        _order = order;
        _name = name.substring(0, Math.min(name.length(), 20));
        int pos = 0;
        int flowPos = 0;
        int totalFlowPos = 0;
        while(pos < seq.length) {
            if(totalFlowPos > len) {
                break;
            }
            if(seq[pos] == '-') {
                pos += 1;
                continue;
            }
            if(!inOrder(seq[pos])) {
                break;
            }
            if(seq[pos] == _order[flowPos]) {
                _signals.add(1);
                pos ++;
                while(pos < seq.length && seq[pos] == _order[flowPos]) {
                    _signals.set(_signals.size()-1, _signals.get(_signals.size()-1)+1);
                    pos ++;
                }
                flowPos = (flowPos + 1)%_order.length;
                totalFlowPos += 1;
                continue;
            }
            _signals.add(0);
            flowPos = (flowPos + 1)%_order.length;
            totalFlowPos += 1;
        }
    }

    public int draw(Graphics g, int x0, int y0, Collection<Flowgram> identical) {
        int pos = 0;
        int xf = x0 + 125;
        int additional_dist = 0;
        g.setFont(new Font("Courier", 0, 10));
        for(Integer signal : _signals) {
            g.setColor(Color.BLACK);
            g.drawString(_name, x0, y0);
            if(signal != 0)
                g.drawRect(xf+10*pos-1, y0-signal*5-1, 6, signal*5);
            g.setColor(_colors[pos%(_colors.length)]);
            g.fillRect(xf+10*pos, y0-signal*5, 5, signal*5);
            char[] data = {_order[pos%_order.length]};
            g.drawChars(data, 0, 1, xf-2+10*pos, y0+10);
            if(identical.size() >= 1) {
                int ipos = 0;
                for(Flowgram flow : identical) {
                    drawIdentical(flow.getName(), g, x0, y0+ipos*10, ipos==identical.size()-1);
                    ipos++;
                }
                additional_dist = (ipos-1)*10;
            }
            pos ++;
        }
        return additional_dist;
    }

    private void drawIdentical(String name, Graphics g, int x, int y, boolean last) {
        g.setFont(new Font("Courier", 0, 10));
        g.setColor(Color.GRAY);
        g.drawLine(x+4, y, x+4, y+10);
        if(!last)
            g.drawLine(x+4, y+10, x+4, y+20);
        g.drawLine(x+4, y+10, x+9, y+10);
        g.drawString(name, x+10, y+5+8);
    }

    private boolean inOrder(char x) {
        for(char o : _order) {
            if(o == x) {
                return true;
            }
        }
        return false;
    }

    public void add(Flowgram flow) {
        ArrayList<Integer> longFlow = flow._signals.size() >= _signals.size() ? flow._signals : _signals;
        ArrayList<Integer> shortFlow = flow._signals.size() >= _signals.size() ? _signals : flow._signals;
        for(int i=0; i<shortFlow.size(); i++) {
            longFlow.set(i, longFlow.get(i) + shortFlow.get(i));
        }
        _signals = longFlow;
    }

    @Override
    public boolean equals(Object o) 
    {
        if(!(o instanceof Flowgram)) {
            return false;
        }
        Flowgram flow = (Flowgram)o;
        ArrayList<Integer> longFlow = flow._signals.size() >= _signals.size() ? flow._signals : _signals;
        ArrayList<Integer> shortFlow = flow._signals.size() >= _signals.size() ? _signals : flow._signals;
        for(int i=0; i<shortFlow.size(); i++) {
            if(shortFlow.get(i) != longFlow.get(i))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this._signals != null ? this._signals.hashCode() : 0);
        return hash;
    }

    public String getName() {
        return _name;
    }

    public int compareTo(Flowgram o) {
        return o.getName().compareTo(getName());
    }
}
