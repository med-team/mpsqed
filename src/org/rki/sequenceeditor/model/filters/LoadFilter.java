/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model.filters;

import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.GroupKey;

/**
 *
 * @author dabrowskiw
 */
public interface LoadFilter {
    public String getExtension();
    public void loadAlignment(String filename, Alignment align, GroupKey baseGroup);
}
