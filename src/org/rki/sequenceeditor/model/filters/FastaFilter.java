/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model.filters;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.GroupKey;
import org.rki.sequenceeditor.model.Sequence;

/**
 *
 * @author dabrowskiw
 */

public class FastaFilter extends FileFilter implements SaveFilter, LoadFilter {
    
    private String[] _allowedExtensions = {".fas", ".fasta"};
    
    @Override
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return(true);
        }
        String name = f.getName().toLowerCase();
        for(String ext : _allowedExtensions) {
            if(name.endsWith(ext))
                return true;
        }
        return(false);
    }

    @Override
    public String getDescription() {
        return("FASTA file (preserves grouping)");
    }

    @Override
    public String getExtension() {
        return(".fas");
    }

    @Override
    public void saveAlignment(Alignment align, String filename) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(filename));
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "File could not be opened for writing. Check permissions.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
        boolean hasGroups = align.getGroups().size()>1;
        try {
            for(Sequence seq : align.getSequencesAll()) {
                out.write(">" + seq.name);
                if(hasGroups) {
                    out.write("%%" + seq.group.getName() + "%%");
                }
                out.write("\n");
                out.write(seq.seq);
                out.write("\n");
            }
            out.close();
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Could not write to file. Check disk space.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

    @Override
    public void loadAlignment(String filename, Alignment align, GroupKey baseGroup) {
        try {
            File file = new File(filename);
            BufferedReader input = new BufferedReader(new FileReader(file));
            StringBuilder s = new StringBuilder();
            while(input.ready()) {
                s.append(input.readLine());
                s.append("\n");
            }
            loadString(s.toString(), align, baseGroup);
        }
        catch(IOException e) {
            JOptionPane.showMessageDialog(null, "File could not be opened for reading. Check permissions.", "Error loading file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void loadString(String s, Alignment align, GroupKey baseGroup) {
        System.out.println("Loading...");
        String[] lines = s.split(">");
        HashMap<String, GroupKey> loadedGroups = new HashMap<String, GroupKey>();
//        if(!overwrite) {
            for(GroupKey key : align.getGroups()) {
                if(!loadedGroups.containsKey(key.getName())) {
                    loadedGroups.put(key.getName(), key);
                }
            }
//        }
        for(String line : lines) {
            if(line.equals("")) continue;
            String[] vals = line.split("\n");
            String name = vals[0];
            vals[0] = "";
            StringBuilder seqBuilder = new StringBuilder();
            for(String val : vals) {
                seqBuilder.append(val.replace("\r", "").replace(" ", ""));
            }
            String seq = seqBuilder.toString().toUpperCase();
            GroupKey group = baseGroup;
            if(loadedGroups.containsKey(baseGroup.getName())) {
                group = loadedGroups.get(baseGroup.getName());
            }
            // Get the name of the group the sequence belongs to from the name
            if(name.contains("%%")) {
                String[] nameVals = name.split("%%");
                name = nameVals[0];
                String groupName = nameVals[1];
                if(loadedGroups.containsKey(groupName)) {
                    group = loadedGroups.get(groupName);
                }
                else {
                    group = new GroupKey(groupName);
                    loadedGroups.put(groupName, group);
                }
            }
            align.addSequence(new Sequence(name, seq, group, align));
        }
        for(GroupKey group : loadedGroups.values()) {
            System.out.println(group.getName());
        }
    }
}
