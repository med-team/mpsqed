/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model.filters;

import com.sun.org.apache.xerces.internal.dom.DOMImplementationImpl;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.Annotation;
import org.rki.sequenceeditor.model.Sequence;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Rich sequence format filter. See http://www.hku.hk/bruhk/gcgdoc/using_sequences.html
 * for documentation.
 * @author dabrowskiw
 */

public class GeneiousFilter extends FileFilter implements SaveFilter {
    private String[] _allowedExtensions = {".geneious"};
    
    @Override
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return(true);
        }
        String name = f.getName().toLowerCase();
        for(String ext : _allowedExtensions) {
            if(name.endsWith(ext))
                return true;
        }
        return(false);
    }

    @Override
    public String getDescription() {
        return("Geneious file (preserves annotations and grouping)");
    }

    @Override
    public String getExtension() {
        return(".geneious");
    }

    @Override
    public void saveAlignment(Alignment align, String filename) {
        DOMImplementation impl = DOMImplementationImpl.getDOMImplementation();
        Document doc = impl.createDocument(null, "geneious", null);
        Element root = doc.createElement("geneious");
        root.setAttribute("version", "4.8.3");
        Element fullDoc = doc.createElement("fullDocument");
        fullDoc.setAttribute("class", "com.biomatters.geneious.publicapi.implementations.DefaultAlignmentDocument");
        fullDoc.setAttribute("version", "1.3-11");
        fullDoc.setAttribute("revisionNumber", "2");
        fullDoc.setAttribute("geneiousVersion", "4.8.3");
        fullDoc.setAttribute("isReferenceOnly", "false");
        root.appendChild(fullDoc);
        Element hiddenFields = doc.createElement("hiddenFields");
        Element cacheUrn = doc.createElement("cache_urn");
        cacheUrn.setAttribute("type", "urn");
        cacheUrn.setTextContent("urn:local:externally_generated:" + new Integer((int)(10000000*Math.random())).toString());
        hiddenFields.appendChild(cacheUrn);
        Element el = doc.createElement("description");
        el.setTextContent("File generated by RKI-Tools");
        hiddenFields.appendChild(el);
        el = doc.createElement("cache_name");
        el.setTextContent("Alignment file");
        hiddenFields.appendChild(el);
        el = doc.createElement("cache_plugin_document_urn");
        hiddenFields.appendChild(el);
        fullDoc.appendChild(hiddenFields);

        Element originalElement = doc.createElement("originalElement");
        Element alignmentDoc = doc.createElement("DefaultAlignmentDocument");
        el = doc.createElement("alphabet");
        el.setTextContent("NUCLEOTIDE");
        alignmentDoc.appendChild(el);

        el = doc.createElement("name");
        el.setTextContent("Alignment file");
        alignmentDoc.appendChild(el);
        Element sequences = doc.createElement("sequences");

        for(Sequence seq : align.getSequencesAll()) {
            Element sequence = doc.createElement("seq");
            sequence.setAttribute("type", "DefaultNucleotideSequence");
            el = doc.createElement("name");
            el.setTextContent(seq.name);
            sequence.appendChild(el);
            el = doc.createElement("description");
            el.setTextContent("%%" + seq.group.getName() + "%%");
            sequence.appendChild(el);

            if(seq.below.getAnnotations().size() != 0 || seq.above.getAnnotations().size() != 0) {
                Element seqAnnos = doc.createElement("sequenceAnnotations");
                for(Annotation anno : seq.above.getAnnotations()) {
                    Element annotation = doc.createElement("annotation");
                    el = doc.createElement("description");
                    el.setTextContent(anno.text);
                    annotation.appendChild(el);
                    el = doc.createElement("type");
                    el.setTextContent(Integer.toHexString(anno.background.getRGB()).substring(2));
                    annotation.appendChild(el);
                    Element intervals = doc.createElement("intervals");
                    Element interval = doc.createElement("interval");

                    el = doc.createElement("minimumIndex");
                    el.setTextContent(new Integer(anno.from).toString());
                    interval.appendChild(el);

                    el = doc.createElement("maximumIndex");
                    el.setTextContent(new Integer(anno.to).toString());
                    interval.appendChild(el);

                    el = doc.createElement("direction");
                    el.setTextContent("leftToRight");
                    interval.appendChild(el);


                    intervals.appendChild(interval);
                    annotation.appendChild(intervals);
                    seqAnnos.appendChild(annotation);
                }
                for(Annotation anno : seq.below.getAnnotations()) {
                    Element annotation = doc.createElement("annotation");
                    el = doc.createElement("description");
                    el.setTextContent(anno.text);
                    annotation.appendChild(el);
                    el = doc.createElement("type");
                    el.setTextContent(Integer.toHexString(anno.background.getRGB()).substring(2));
                    annotation.appendChild(el);
                    Element intervals = doc.createElement("intervals");
                    Element interval = doc.createElement("interval");

                    el = doc.createElement("minimumIndex");
                    el.setTextContent(new Integer(anno.from).toString());
                    interval.appendChild(el);

                    el = doc.createElement("maximumIndex");
                    el.setTextContent(new Integer(anno.to).toString());
                    interval.appendChild(el);

                    el = doc.createElement("direction");
                    el.setTextContent("rightToLeft");
                    interval.appendChild(el);


                    intervals.appendChild(interval);
                    annotation.appendChild(intervals);
                    seqAnnos.appendChild(annotation);
                }
                sequence.appendChild(seqAnnos);
            }


            el = doc.createElement("charSequence");
            if(seq.seq.length() < align.getLongestAll()) {
                el.setAttribute("gapSuffixLength", new Integer(align.getLongestAll()-seq.seq.length()).toString());
            }
            el.setTextContent(seq.seq);
            sequence.appendChild(el);
            sequences.appendChild(sequence);
        }

        alignmentDoc.appendChild(sequences);
        originalElement.appendChild(alignmentDoc);
        fullDoc.appendChild(originalElement);
        boolean hasGroups = align.getGroups().size()>1;
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            XMLSerializer serializer = new XMLSerializer(bout, null);
            serializer.serialize(root);
            ZipOutputStream zout = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(filename)));
            ZipEntry entry = new ZipEntry(filename);
            zout.putNextEntry(entry);
            zout.write(bout.toByteArray());
            zout.close();
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Could not write to file. Check permissions and disk space.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

}
