/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model.filters;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.GroupKey;
import org.rki.sequenceeditor.model.Sequence;

/**
 *
 * @author dabrowskiw
 */

public class BasicFastaFilter extends FileFilter implements SaveFilter {

    private String[] _allowedExtensions = {".fas", ".fasta"};

    @Override
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return(true);
        }
        String name = f.getName().toLowerCase();
        for(String ext : _allowedExtensions) {
            if(name.endsWith(ext))
                return true;
        }
        return(false);
    }

    @Override
    public String getDescription() {
        return("Blank FASTA file");
    }

    @Override
    public String getExtension() {
        return(".fas");
    }

    @Override
    public void saveAlignment(Alignment align, String filename) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(filename));
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "File could not be opened for writing. Check permissions.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
        boolean hasGroups = align.getGroups().size()>1;
        try {
            for(Sequence seq : align.getSequencesAll()) {
                out.write(">" + seq.name);
                out.write("\n");
                out.write(seq.seq);
                out.write("\n");
            }
            out.close();
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Could not write to file. Check disk space.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

}
