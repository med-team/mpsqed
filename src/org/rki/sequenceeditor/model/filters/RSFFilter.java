/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model.filters;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.rki.sequenceeditor.model.Alignment;
import org.rki.sequenceeditor.model.Annotation;
import org.rki.sequenceeditor.model.GroupKey;
import org.rki.sequenceeditor.model.Primer;
import org.rki.sequenceeditor.model.Sequence;

/**
 * Rich sequence format filter. See http://www.hku.hk/bruhk/gcgdoc/using_sequences.html
 * for documentation.
 * @author dabrowskiw
 */

public class RSFFilter extends FileFilter implements SaveFilter, LoadFilter {
    private String[] _allowedExtensions = {".rsf"};
    
    private static int STATE_NONE = -1;
    private static int STATE_ATTRIBUTES = 0;
    private static int STATE_COMMENT = 1;
    private static int STATE_FEATURE = 2;
    private static int STATE_SEQUENCE = 3;


    @Override
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return(true);
        }
        String name = f.getName().toLowerCase();
        for(String ext : _allowedExtensions) {
            if(name.endsWith(ext))
                return true;
        }
        return(false);
    }

    @Override
    public String getDescription() {
        return("Rich Sequence Format (preserves annotations and grouping)");
    }

    @Override
    public String getExtension() {
        return(".rsf");
    }

    @Override
    public void saveAlignment(Alignment align, String filename) {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(filename));
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "File could not be opened for writing. Check permissions.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
        boolean hasGroups = align.getGroups().size()>1;
        try {
            out.write("!!RICH_SEQUENCE 1.0\n");
            out.write("..\n");
            for(Sequence seq : align.getSequencesAll()) {
                out.write("{\n");
                out.write("name    " + seq.name + "\n");
                out.write("descrip Sequence exported from RKI-Tools\n");
                out.write("type    DNA\n");
                out.write("strands 1\n");
                if(hasGroups) {
                    out.write("comments\n    %%" + seq.group.getName() + "%%\n");
                }
                for(Annotation anno : seq.above.getAnnotations()) {
                    writeAnnotation(anno, out);
                }
                for(Annotation anno : seq.below.getAnnotations()) {
                    writeAnnotation(anno, out);
                }
                out.write("sequence\n    ");
                out.write(seq.seq);
                out.write("\n}\n");
            }
            out.close();
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Could not write to file. Check disk space.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }

    private void writeAnnotation(Annotation anno, BufferedWriter out) throws IOException {
        String direction = anno.list.above?"right_arrow":"left_arrow";
        if(anno instanceof Primer) {
            Primer primer = (Primer)anno;
            if(primer.isPyro)
                out.write("feature " + primer.from + " " +primer.to + " 0x" + Integer.toHexString(primer.background.getRGB()).substring(2)+ " " + direction + " t_star ANNOTATION\n    " + primer.text + "%%--%%" + primer.getSequence() + "\n");
            else
                out.write("feature " + primer.from + " " +primer.to + " 0x" + Integer.toHexString(primer.background.getRGB()).substring(2)+ " " + direction + " t_diamond ANNOTATION\n    " + primer.text + "%%--%%" + primer.getSequence() + "\n");
        }
        else {
            out.write("feature " + anno.from + " " + anno.to + " 0x" + Integer.toHexString(anno.background.getRGB()).substring(2)+ " " + direction + " t_hash ANNOTATION\n    " + anno.text + "\n");
        }
    }

    @Override
    public void loadAlignment(String filename, Alignment align, GroupKey baseGroup) {
        try {
            File file = new File(filename);
            BufferedReader input = new BufferedReader(new FileReader(file));
            int headerLine = 0;
            while(input.ready()) {
                String line = input.readLine();
                if(headerLine == 0 && !line.startsWith("!!RICH_SEQUENCE 1.0")) {
                    JOptionPane.showMessageDialog(null, "File seems not to be in rich sequence format (missing header).", "Error loading file", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                headerLine ++;
                if(line.startsWith(".."))
                    break;
            }
            HashMap<String, GroupKey> loadedGroups = new HashMap<String, GroupKey>();
            for(GroupKey key : align.getGroups()) {
                if(!loadedGroups.containsKey(key.getName())) {
                    loadedGroups.put(key.getName(), key);
                }
            }

            int state = STATE_NONE;
            Annotation anno = null;
            LinkedList<Annotation> below = new LinkedList<Annotation>();
            LinkedList<Annotation> above = new LinkedList<Annotation>();
            StringBuilder builder = new StringBuilder();
            String comment = "";
            String sequence = "";
            String name = "";
            boolean annotationBelow = false;
            while(input.ready()) {
                String line = input.readLine();
                if(line.startsWith("{")) {
                    state = STATE_ATTRIBUTES;
                    continue;
                }
                else if(line.startsWith("}")) {
                    if(state == STATE_SEQUENCE)
                        sequence = builder.toString();
                    if(state == STATE_COMMENT)
                        comment = builder.toString();
                    sequence = sequence.trim();
                    String[] commentDetails = comment.split("%%");
                    Sequence seq = null;
                    GroupKey group = baseGroup;
                    if(commentDetails.length > 1) {
                        String groupName = commentDetails[1];
                        if(loadedGroups.containsKey(groupName)) {
                            group = loadedGroups.get(groupName);
                        }
                        else {
                            group = new GroupKey(groupName);
                            loadedGroups.put(groupName, group);
                        }
                    }
                    seq = new Sequence(name, sequence, group, align);
                    for(Annotation a : above) {
                        seq.above.addAnnotation(a);
                        a.list = seq.above;
                    }
                    for(Annotation a : below) {
                        seq.below.addAnnotation(a);
                        a.list = seq.below;
                    }
                    align.addSequence(seq);
                    below = new LinkedList<Annotation>();
                    above = new LinkedList<Annotation>();
                    comment = "";
                    sequence = "";
                    name = "";
                }
                else if(line.startsWith("comments")) {
                    if(state == STATE_SEQUENCE)
                        sequence = builder.toString();
                    state = STATE_COMMENT;
                    builder = new StringBuilder();
                    continue;
                }
                else if(line.startsWith("sequence")) {
                    if(state == STATE_COMMENT)
                        comment = builder.toString();
                    state = STATE_SEQUENCE;
                    builder = new StringBuilder();
                    continue;
                }
                else if(line.startsWith("feature")) {
                    annotationBelow = false;
                    if(state == STATE_SEQUENCE)
                        sequence = builder.toString();
                    if(state == STATE_COMMENT)
                        comment = builder.toString();
                    String[] featureData = line.split("\\s");
                    if(featureData[4].equals("left_arrow"))
                        annotationBelow = true;
                    if(featureData[5].equals("t_diamond") || featureData[5].equals("t_star")) {
                        anno = new Primer(Integer.decode(featureData[1]),
                            Integer.decode(featureData[2]), Color.decode(featureData[3]), "", !annotationBelow, null);
                        if(featureData[5].equals("t_star")) {
                            ((Primer)anno).isPyro = true;
                        }
                    }
                    else {
                        anno = new Annotation(Integer.decode(featureData[1]),
                            Integer.decode(featureData[2]), Color.decode(featureData[3]), "", null);
                    }
                    state = STATE_FEATURE;
                    continue;
                }
                else if(state == STATE_FEATURE) {
                    if(anno instanceof Primer) {
                        String[] primerData = line.split("%%--%%");
                        Primer primer = (Primer)anno;
                        primer.text = primerData[0].trim();
                        primer.loadSequence = primerData[1];
                    }
                    else
                        anno.text = line.trim();
                    if(annotationBelow)
                        below.add(anno);
                    else
                        above.add(anno);
                    anno = null;
                    state = STATE_ATTRIBUTES;
                    continue;
                }
                else if(state == STATE_COMMENT) {
                    builder.append(line);
                }
                else if(state == STATE_SEQUENCE) {
                    builder.append(line);
                }
                else if(line.startsWith("name")) {
                    name = line.substring(4).trim();
                }
            }
            for(Sequence seq : align.getSequencesAll()) {
                for(Annotation annotation : seq.above.getAnnotations()) {
                    if(annotation instanceof Primer) {
                        finalizePrimer((Primer)annotation);
                    }
                }
                for(Annotation annotation : seq.below.getAnnotations()) {
                    if(annotation instanceof Primer) {
                        finalizePrimer((Primer)annotation);
                    }
                }
            }
        }
        catch(IOException e) {
            JOptionPane.showMessageDialog(null, "File could not be opened for reading. Check permissions.", "Error loading file", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void finalizePrimer(Primer primer) {
        char[] loadSequence = primer.loadSequence.toCharArray();
        for(int pos=0; pos<loadSequence.length; pos++) {
            primer.lockBase(pos, loadSequence[pos]);
        }
        primer.updateScores();
        primer.updateOpposite();
        primer.updateTm();
    }
}
