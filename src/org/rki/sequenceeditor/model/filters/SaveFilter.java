/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model.filters;

import org.rki.sequenceeditor.model.Alignment;

/**
 *
 * @author dabrowskiw
 */
public interface SaveFilter {
    public String getExtension();
    public void saveAlignment(Alignment align, String filename);
}
