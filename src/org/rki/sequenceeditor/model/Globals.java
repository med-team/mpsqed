package org.rki.sequenceeditor.model;

import java.util.HashMap;

public class Globals {
    public static HashMap<Character, Character> complementary = new HashMap<Character, Character>();
    
    static {
        complementary.put('A', 'T');
        complementary.put('G', 'C');
        complementary.put('T', 'A');
        complementary.put('C', 'G');
        complementary.put('R', 'Y');
        complementary.put('Y', 'R');
        complementary.put('N', 'N');
        complementary.put('W', 'W');
        complementary.put('S', 'S');
        complementary.put('H', 'D');
        complementary.put('D', 'H');
        complementary.put('B', 'V');
        complementary.put('V', 'B');
        complementary.put('-', '-');
    }
}
