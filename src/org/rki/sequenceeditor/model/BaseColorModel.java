/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.util.HashMap;

/**
 *
 * @author dabrowskiw
 */
public class BaseColorModel {
//    public HashMap<Character, int[]> baseColor = new HashMap<Character, int[]>();
    public BaseColorModelMap baseColor = new BaseColorModelMap();
    
    public void addBaseColor(char base, int fg, int bg, int foregroundDominant) {
        int[] colors = {fg, bg, foregroundDominant};
        baseColor.put(base, colors);
    }
}
