/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.util.HashMap;

/**
 *
 * @author dabrowskiw
 */
public class ColorModels {
    public static HashMap<String, BaseColorModel> models = new HashMap<String, BaseColorModel>();
    public static String currentModel = "UPGMA";
    
    static {
        BaseColorModel cm = new BaseColorModel();
        cm.addBaseColor('A', 0x000000, 0xffffff, 1);
        cm.addBaseColor('G', 0x000000, 0xffffff, 1);
        cm.addBaseColor('T', 0x000000, 0xffffff, 1);
        cm.addBaseColor('U', 0x000000, 0xffffff, 1);
        cm.addBaseColor('C', 0x000000, 0xffffff, 1);
        cm.addBaseColor('R', 0x000000, 0xffffff, 1);
        cm.addBaseColor('Y', 0x000000, 0xffffff, 1);
        cm.addBaseColor('S', 0x000000, 0xffffff, 1);
        cm.addBaseColor('W', 0x000000, 0xffffff, 1);
        cm.addBaseColor('K', 0x000000, 0xffffff, 1);
        cm.addBaseColor('M', 0x000000, 0xffffff, 1);
        cm.addBaseColor('B', 0x000000, 0xffffff, 1);
        cm.addBaseColor('D', 0x000000, 0xffffff, 1);
        cm.addBaseColor('H', 0x000000, 0xffffff, 1);
        cm.addBaseColor('V', 0x000000, 0xffffff, 1);
        cm.addBaseColor('N', 0x000000, 0xffffff, 1);
        cm.addBaseColor('-', 0x000000, 0xffffff, 1);
        cm.addBaseColor('?', 0x000000, 0xffffff, 1);
        models.put("Monochrome", cm);

        cm = new BaseColorModel();
        cm.addBaseColor('A', 0xff0000, 0xffffff, 1);
        cm.addBaseColor('G', 0xdddd00, 0xffffff, 1);
        cm.addBaseColor('T', 0x0000ff, 0xffffff, 1);
        cm.addBaseColor('U', 0x0000ff, 0xffffff, 1);
        cm.addBaseColor('C', 0x00ff00, 0xffffff, 1);
        cm.addBaseColor('R', 0x000000, 0xffffff, 1);
        cm.addBaseColor('Y', 0x000000, 0xffffff, 1);
        cm.addBaseColor('S', 0x000000, 0xffffff, 1);
        cm.addBaseColor('W', 0x000000, 0xffffff, 1);
        cm.addBaseColor('K', 0x000000, 0xffffff, 1);
        cm.addBaseColor('M', 0x000000, 0xffffff, 1);
        cm.addBaseColor('B', 0x000000, 0xffffff, 1);
        cm.addBaseColor('D', 0x000000, 0xffffff, 1);
        cm.addBaseColor('H', 0x000000, 0xffffff, 1);
        cm.addBaseColor('V', 0x000000, 0xffffff, 1);
        cm.addBaseColor('N', 0x000000, 0xffffff, 1);
        cm.addBaseColor('-', 0x000000, 0xffffff, 1);
        cm.addBaseColor('?', 0x000000, 0xffffff, 1);
        models.put("UPGMA", cm);

        cm = new BaseColorModel();
        cm.addBaseColor('A', 0x000000, 0xff0000, 0);
        cm.addBaseColor('G', 0x000000, 0xffff00, 0);
        cm.addBaseColor('T', 0x000000, 0x0000ff, 0);
        cm.addBaseColor('U', 0x000000, 0x0000ff, 0);
        cm.addBaseColor('C', 0x000000, 0x00ff00, 0);
        cm.addBaseColor('R', 0x000000, 0xffffff, 0);
        cm.addBaseColor('Y', 0x000000, 0xffffff, 0);
        cm.addBaseColor('S', 0x000000, 0xffffff, 0);
        cm.addBaseColor('W', 0x000000, 0xffffff, 0);
        cm.addBaseColor('K', 0x000000, 0xffffff, 0);
        cm.addBaseColor('M', 0x000000, 0xffffff, 0);
        cm.addBaseColor('B', 0x000000, 0xffffff, 0);
        cm.addBaseColor('D', 0x000000, 0xffffff, 0);
        cm.addBaseColor('H', 0x000000, 0xffffff, 0);
        cm.addBaseColor('V', 0x000000, 0xffffff, 0);
        cm.addBaseColor('N', 0x000000, 0xffffff, 0);
        cm.addBaseColor('-', 0x000000, 0xffffff, 1);
        cm.addBaseColor('?', 0x000000, 0xffffff, 1);
        models.put("UPGMA (background)", cm);
        
        cm = new BaseColorModel();
        cm.addBaseColor('A', 0x000000, 0x9bf48a, 0);
        cm.addBaseColor('G', 0x000000, 0xb8c7cc, 0);
        cm.addBaseColor('T', 0x000000, 0xf7b289, 0);
        cm.addBaseColor('U', 0x000000, 0xf7b289, 0);
        cm.addBaseColor('C', 0x000000, 0xbef2ee, 0);
        cm.addBaseColor('R', 0x000000, 0xffffff, 0);
        cm.addBaseColor('Y', 0x000000, 0xffffff, 0);
        cm.addBaseColor('S', 0x000000, 0xffffff, 0);
        cm.addBaseColor('W', 0x000000, 0xffffff, 0);
        cm.addBaseColor('K', 0x000000, 0xffffff, 0);
        cm.addBaseColor('M', 0x000000, 0xffffff, 0);
        cm.addBaseColor('B', 0x000000, 0xffffff, 0);
        cm.addBaseColor('D', 0x000000, 0xffffff, 0);
        cm.addBaseColor('H', 0x000000, 0xffffff, 0);
        cm.addBaseColor('V', 0x000000, 0xffffff, 0);
        cm.addBaseColor('N', 0x000000, 0xffffff, 0);
        cm.addBaseColor('-', 0x000000, 0xffffff, 1);
        cm.addBaseColor('?', 0x000000, 0xffffff, 1);
        models.put("BioEdit (background)", cm);        
    }
}
