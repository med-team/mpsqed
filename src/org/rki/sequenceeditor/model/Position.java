package org.rki.sequenceeditor.model;

public class Position {
    public int x;
    public int y;
    public boolean onAnnotation;
    public Annotation annotation;
    public boolean onCollapser;
    public boolean onConsensus;
    public GroupKey consensusGroup = null;
    
    public Position(int x, int y, boolean onAnnotation, Annotation annotation, boolean onCollapser, boolean onConsensus, GroupKey consensusGroup) {
        this.x = x;
        this.y = y;
        this.onAnnotation = onAnnotation;
        this.annotation = annotation;
        this.onCollapser = onCollapser;
        this.onConsensus = onConsensus;
        this.consensusGroup = consensusGroup;
    }
    
    public int[] getCoordinates() {
        int[] res = {x, y};
        return(res);
    }
}
