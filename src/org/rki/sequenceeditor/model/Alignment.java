package org.rki.sequenceeditor.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Alignment {
    public int tripletGrouping = -1;
    private Settings _settings;
    public MasterSequence masterSequenceAll = null;
    public HashMap<GroupKey, MasterSequence> masterSequences = new HashMap<GroupKey, MasterSequence>();

    
    private ArrayList<Sequence> _sequences = new ArrayList<Sequence>();

    public ArrayList<SNP> snps = new ArrayList<SNP>();
	
	public Alignment(Settings settings) {
            _settings = settings;
	}

        public void setSettings(Settings settings) {
            _settings = settings;
        }

        public Settings getSettings() {
            return _settings;
        }

	public void addSequence(Sequence seq) {
		_sequences.add(seq);
	}
	
	public Sequence getSequence(int index) {
            try {
		return(_sequences.get(index));
            } catch(IndexOutOfBoundsException e) {
                return(null);
            }
	}
	
    public Collection<Sequence> getSequencesAll() {
        return(_sequences);
    }

    public int getGroupFirst(GroupKey group) {        
        for(int i=0; i<_sequences.size(); i++) {
            if(_sequences.get(i).group == group) {
                return(i);
            }
        }
        return 0;
    }

	public Collection<Sequence> getSequences(GroupKey group) {
            LinkedList<Sequence> seqs = new LinkedList<Sequence>();
            for(Sequence s : _sequences) {
                if(s.group == group) {
                    seqs.add(s);
                }
            }
            return(seqs);
	}
	
        public Collection<GroupKey> getGroups() {
            LinkedList<GroupKey> groups = new LinkedList<GroupKey>();
            for(Sequence seq : _sequences) {
                if(!groups.contains(seq.group)) {
                    groups.add(seq.group);
                }
            }
            return(groups);
        }
        
        public int countAll() {
            return(_sequences.size());            
        }
        
	public int count(GroupKey group) {
            return(getSequences(group).size());
	}
        
        public int getLongestAll() {
            int res = 0;
            for(Sequence seq : _sequences) {
                res = Math.max(res, seq.seq.length());
            }
            return(res);
        }

        public int getLongest(GroupKey group) {
            int res = 0;
            for(Sequence seq : getSequences(group)) {
                res = Math.max(res, seq.seq.length());
            }
            return(res);
        }

        
        public char[][] toCharArrayAll(int len) {
            char[][] res = new char[countAll()][len];
            int nseq = 0;
            for(Sequence seq : _sequences) {
                char[] s = seq.seq.toCharArray();
                int pos = 0;
                for(; pos<Math.min(len, s.length); pos++) {
                    res[nseq][pos] = s[pos];
                }
                for(; pos<len; pos++) {
                    res[nseq][pos] = '\0';
                }
                nseq ++;
            }
            return(res);
        }

        public char[][] toCharArray(int len, GroupKey group) {
            char[][] res = new char[count(group)][len];
            int nseq = 0;
            for(Sequence seq :getSequences(group)) {
                char[] s = seq.seq.toCharArray();
                int pos = 0;
                for(; pos<Math.min(len, s.length); pos++) {
                    res[nseq][pos] = s[pos];
                }
                for(; pos<len; pos++) {
                    res[nseq][pos] = '\0';
                }
                nseq ++;
            }
            return(res);
        }
        
        public void moveSelected(int dist) {
            LinkedList<Sequence> toMove = new LinkedList<Sequence>();
            for(Sequence seq : _sequences) {
                if(seq.selected) {
                    if(dist < 0)
                        toMove.add(seq);
                    else 
                        toMove.add(0, seq);
                    int pos = _sequences.indexOf(seq);
                    if(!seq.group.getName().equals(_sequences.get(pos+dist).group.getName())) {
                        return;
                    }
                }
            }
            for(Sequence seq : toMove) {
                int oldpos = _sequences.indexOf(seq);
                _sequences.remove(oldpos);
                _sequences.add(oldpos + dist, seq);
            }
        }

        private String translateToRegexp(String sequence, boolean allowGaps) {
            StringBuilder res = new StringBuilder();
            char[] seq = sequence.toUpperCase().toCharArray();
            for(char c : seq) {
                switch(c) {
                    case 'A':
                        res.append("A");
                        break;
                    case 'G':
                        res.append("G");
                        break;
                    case 'T':
                        res.append("T");
                        break;
                    case 'C':
                        res.append("C");
                        break;
                    case 'R':
                        res.append("[AG]");
                        break;
                    case 'Y':
                        res.append("[CT]");
                        break;
                    case 'M':
                        res.append("[CA]");
                        break;
                    case 'K':
                        res.append("[TG]");
                        break;
                    case 'W':
                        res.append("[TA]");
                        break;
                    case 'S':
                        res.append("[CG]");
                        break;
                    case 'B':
                        res.append("[CTG]");
                        break;
                    case 'D':
                        res.append("[ATG]");
                        break;
                    case 'H':
                        res.append("[ATC]");
                        break;
                    case 'V':
                        res.append("[ACG]");
                        break;
                    case 'N':
                        res.append("[AGTC]");
                        break;
                }
                if(allowGaps) {
                    res.append("-*");
                }
            }
            return res.toString();
        }

        public int[] findSubSequence(int fromSeq, int fromIndex, String sequence, boolean allowGaps) {
            int[] res = {-1,-1, -1};
            for(int i=fromSeq; i<_sequences.size(); i++) {
                int from = 0;
                if(i == fromSeq) {
                    from = fromIndex;
                }
                Pattern search = Pattern.compile(translateToRegexp(sequence, allowGaps));
                Matcher match = search.matcher(_sequences.get(i).seq);
                if(match.find(from)) {
                    res[0] = i;
                    res[1] = match.start();
                    res[2] = match.end() - match.start();
                    return(res);
                }
/*                int pos = _sequences.get(i).seq.indexOf(sequence, from);
                if(pos != -1) {
                    res[0] = i;
                    res[1] = pos;
                    return(res);
                }*/
            }
            return(res);
        }
        
        public void sortGroups() {
            Collections.sort(_sequences, new Comparator<Sequence>() {
                @Override
                public int compare(Sequence s1, Sequence s2) {
                    return(s1.group.compareTo(s2.group));
            }});            
        }

        public void sortByName() {
            Collections.sort(_sequences, new Comparator<Sequence>() {
                @Override
                public int compare(Sequence s1, Sequence s2) {
                    if(s1.group.compareTo(s2.group) == 0) {
                        return(s1.name.toLowerCase().compareTo(s2.name.toLowerCase()));
                    }
                    return(s1.group.compareTo(s2.group)); 
            }});
        }

        public void sortByHomology() {
            Collections.sort(_sequences, new Comparator<Sequence>() {
                @Override
                public int compare(Sequence s1, Sequence s2) {
                    if(s1.group.compareTo(s2.group) == 0) {
                        return((int)Math.signum(s2.homology - s1.homology));
                    }
                    return(s1.group.compareTo(s2.group)); 
            }});
        }

        public void sortByHomology(final int from, final int to) {
            System.out.println(from + " : " + to);
            Collections.sort(_sequences, new Comparator<Sequence>() {
                public int compare(Sequence s1, Sequence s2) {
                    if(s1.seq.length() < to || s2.seq.length() < to) {
                        return s2.seq.length() - s1.seq.length();
                    }
                    if(s1.group.compareTo(s2.group) == 0) {
                        byte[] seqs1 = s1.seq.substring(from, to).getBytes();
                        byte[] seqs2 = s2.seq.substring(from, to).getBytes();
                        int diffs = 0;
                        int sign = 1;
                        if(s1.seq.substring(from, to).compareTo(s2.seq.substring(from, to)) < 0) {
                            sign = -1;
                        }
                        for(int i=0; i<seqs1.length; i++) {
                            if(seqs1[i] != seqs2[i]) {
                                diffs++;
                            }
                        }
                        return(sign * diffs);
                    }
                    return(s1.group.compareTo(s2.group)); 
            }});
        }

        public void calculateHomology(Sequence master) {
            for(Sequence seq : _sequences) {
                seq.calculateHomology(master);
            }
        }

        public void toggleSnp(int pos) {
            for(SNP snp : snps) {
                if(snp.pos == pos) {
                    snps.remove(snp);
                    return;
                }
            }
            snps.add(new SNP(pos));
        }

        public void setSnp(int pos) {
            for(SNP snp : snps) {
                if(snp.pos == pos) {
                    return;
                }
            }
            snps.add(new SNP(pos));
        }

        public Collection<Primer> getPrimers() {
            ArrayList<Primer> primers = new ArrayList<Primer>();
            for(Sequence seq : _sequences) {
                for(Annotation a : seq.above.getAnnotations()) {
                    if(a instanceof Primer) {
                        primers.add((Primer)a);
                    }
                }
                for(Annotation a : seq.below.getAnnotations()) {
                    if(a instanceof Primer) {
                        primers.add((Primer)a);
                    }
                }
            }
            return primers;
        }

        public Collection<Primer> getPyroPrimers() {
            ArrayList<Primer> primers = new ArrayList<Primer>();
            for(Sequence seq : _sequences) {
                for(Annotation a : seq.above.getAnnotations()) {
                    if(a instanceof Primer && ((Primer)a).isPyro) {
                        primers.add((Primer)a);
                    }
                }
                for(Annotation a : seq.below.getAnnotations()) {
                    if(a instanceof Primer && ((Primer)a).isPyro) {
                        primers.add((Primer)a);
                    }
                }
            }
            return primers;
        }

        public int[] getGroupScores(String seq, int from, GroupKey group) {
            int[] scores = new int[seq.length()];
            for(int i=0; i<seq.length(); i++) {
                scores[i] = 100;
                for(Sequence sequence : _sequences) {
                    if(sequence.group.compareTo(group) != 0) {
                        continue;
                    }
                    int cmpscore = compareBases(seq.charAt(i), sequence.seq.charAt(from+i));
                    if(cmpscore < scores[i])
                        scores[i] = cmpscore;
                }
            }
            return scores;

        }

        public int[] getScores(String seq, int from) {
            int[] scores = new int[seq.length()];
            for(int i=0; i<seq.length(); i++) {
                scores[i] = 100;
                for(Sequence sequence : _sequences) {
                    if(sequence.seq.length() <= from+i || seq.length() <= i)
                        continue;
                    int cmpscore = compareBases(seq.charAt(i), sequence.seq.charAt(from+i));
                    if(cmpscore < scores[i])
                        scores[i] = cmpscore;
                }
            }
            return scores;
        }

        public int compareBases(char primer, char sequence) {
            primer = Character.toLowerCase(primer);
            sequence = Character.toLowerCase(sequence);
            if(primer == sequence)
                return 100;
            switch(primer) {
                case 'r':
                    switch(sequence) {
                        case 'a':
                            return 80;
                        case 'g':
                            return 80;
                        default:
                            return 0;
                    }
                case 'm':
                    switch(sequence) {
                        case 'c':
                            return 80;
                        case 'a':
                            return 80;
                        default:
                            return 0;
                    }
                case 's':
                    switch(sequence) {
                        case 'c':
                            return 80;
                        case 'g':
                            return 80;
                        default:
                            return 0;
                    }
                case 'y':
                    switch(sequence) {
                        case 'c':
                            return 80;
                        case 't':
                            return 80;
                        default:
                            return 0;
                    }
                case 'k':
                    switch(sequence) {
                        case 't':
                            return 80;
                        case 'g':
                            return 80;
                        default:
                            return 0;
                    }
                case 'w':
                    switch(sequence) {
                        case 'a':
                            return 80;
                        case 't':
                            return 80;
                        default:
                            return 0;
                    }
                case 'b':
                    switch(sequence) {
                        case 'c':
                            return 65;
                        case 't':
                            return 65;
                        case 'g':
                            return 65;
                        default:
                            return 0;
                    }
                case 'd':
                    switch(sequence) {
                        case 'a':
                            return 65;
                        case 't':
                            return 65;
                        case 'g':
                            return 65;
                        default:
                            return 0;
                    }
                case 'h':
                    switch(sequence) {
                        case 'a':
                            return 65;
                        case 't':
                            return 65;
                        case 'c':
                            return 65;
                        default:
                            return 0;
                    }
                case 'v':
                    switch(sequence) {
                        case 'c':
                            return 65;
                        case 'a':
                            return 65;
                        case 'g':
                            return 65;
                        default:
                            return 0;
                    }
                case 'n':
                    return 50;
            }
            return 0;
        }
}
