/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author dabrowskiw
 */
public class Primer extends Annotation {
    private boolean _above;
    private HashMap<Integer, Character> _locked = new HashMap<Integer, Character>();
    private int[] _scores;
    private int[] _groupScores;
    private float _tm;
    public boolean isPyro = false;
    // Just a helper variable for loading of primers
    public String loadSequence;
    // If another primer is present on the sequence so that it can form a product with this one,
    // opposite should reference that primer.
    public Primer opposite = null;

    public Primer(int from, int to, Color background, String text, boolean above, AnnotationList list) {
        super(from, to, background, text, list);
        _above = above;
        if(list != null) {
            updateScores();
            updateOpposite();
        }
    }

    public final void updateScores() {
        _scores = list.sequence.align.getScores(getSequence(), from);
        _groupScores = list.sequence.align.getGroupScores(getSequence(), from, list.sequence.group);
        updateTm();
    }

    public final void updateOpposite() {
        Collection<Annotation> otherSide = null;
        Collection<Annotation> thisSide = null;
        if(_above) {
            otherSide = list.sequence.below.getAnnotations();
            thisSide = list.sequence.above.getAnnotations();
        }
        else {
            otherSide = list.sequence.above.getAnnotations();
            thisSide = list.sequence.below.getAnnotations();
        }
        int direction = _above?-1:1;
        int closestOtherside = list.sequence.seq.length();
        int closestThisside = closestOtherside;
        Primer closest = null;
        for(Annotation anno : otherSide) {
            if(anno instanceof Primer) {
                Primer p = (Primer)anno;
                int dist = direction*((_above?to:from) - (_above?p.from:p.to));
                if(dist < closestOtherside && dist > 0) {
                    closestOtherside = dist;
                    closest = p;
                }
            }
        }
        for(Annotation anno : thisSide) {
            if(anno instanceof Primer) {
                Primer p = (Primer)anno;
                int dist = direction*((_above?to:from) - (_above?p.to:p.from));
                if(dist < closestThisside && dist > 0) {
                    closestThisside = dist;
                }
            }
        }
        if(closestOtherside < closestThisside && closest != null) {
            opposite = closest;
            opposite.opposite = this;
        }
        else {
            opposite = null;
        }
    }

    public Flowgram getFlowgram(Sequence seq, int len, String name) {
        char[] order = {'A', 'G', 'T', 'C'};
        if(_above) {
            return new Flowgram(seq.seq.substring(to, to+10*len).toCharArray(), order, len, name);
        }
        else {
            return new Flowgram(Sequence.invert(seq.seq.substring(Math.max(from-10*len, 0), from)).toCharArray(), order, len, name);
        }
    }

    public int getMaxFlowgramLength() {
        int maxlength = 1000;
        System.out.println(list.sequence.align.masterSequences.size());
        for(GroupKey group : list.sequence.align.masterSequences.keySet()) {
            int gmaxlen = 0;
            MasterSequence mseq = list.sequence.align.masterSequences.get(group);
            if(_above) {
                for(int i=to; i<mseq.seq.length() && mseq.conservation[i]==1; i++) {
                    gmaxlen++;
                }
            }
            else {
                for(int i=from; i>=0 && mseq.conservation[i]==1; i--) {
                    gmaxlen++;
                }
            }
            maxlength = Math.min(maxlength, gmaxlen);
        }
        return(maxlength);
    }

    public String getSequence() {
        char[] res = list.sequence.seq.substring(from, to).toCharArray();
        for(Integer pos : _locked.keySet()) {
            if(pos >= from && pos+1 <= to) {
                res[pos-from] = _locked.get(pos);
            }
        }
        return new String(res);
    }

    public int[] getScores() {
        return _scores;
    }

    public int[] getGroupScores() {
        return _groupScores;
    }

    public void lockBase(int pos, char base) {
        int fullpos = pos+from;
        if(base != list.sequence.seq.charAt(fullpos)) {
            _locked.put(fullpos, base);
        }
        else {
            _locked.remove(fullpos);
        }
    }

    public void degenerate(String master) {
        for(int i=0; i<master.length(); i++)
            lockBase(i, master.charAt(i));
        updateScores();
    }

    private char[] getBases(char base) {
        switch(base) {
            case 'a':
                char[] res = {'a'};
                return res;
            case 'g':
                char[] res2 = {'g'};
                return res2;
            case 't':
                char[] res3 = {'t'};
                return res3;
            case 'u':
                char[] res16 = {'u'};
                return res16;
            case 'c':
                char[] res4 = {'c'};
                return res4;
            case 'r':
                char[] res5 = {'a', 'g'};
                return res5;
            case 'y':
                char[] res6 = {'c', 't'};
                return res6;
            case 'm':
                char[] res7 = {'c', 'a'};
                return res7;
            case 'k':
                char[] res8 = {'t', 'g'};
                return res8;
            case 'w':
                char[] res9 = {'t', 'a'};
                return res9;
            case 's':
                char[] res10 = {'c', 'g'};
                return res10;
            case 'b':
                char[] res11 = {'c', 't', 'g'};
                return res11;
            case 'd':
                char[] res12 = {'a', 't', 'g'};
                return res12;
            case 'h':
                char[] res13 = {'a', 't', 'c'};
                return res13;
            case 'v':
                char[] res14 = {'a', 'g', 'c'};
                return res14;
            case 'n':
                char[] res15 = {'a', 't', 'c', 'g'};
                return res15;
        }
        return null;
    }

    private LinkedList<char[]> getAllDimeres(char[] degenerated) {
        LinkedList<char[]> res = new LinkedList<char[]>();
        char[] bases1 = getBases(degenerated[0]);
        char[] bases2 = getBases(degenerated[1]);
        for(char base1 : bases1) {
            for(char base2 : bases2) {
                char[] dimer = {base1, base2};
                res.add(dimer);
            }
        }
        return res;
    }

    private char[] getLowestDimer(char[] degenerated) {
        LinkedList<char[]> all = getAllDimeres(degenerated);
        char[] lowest = all.getFirst();
        float[] lowestDhDs = getDimereDhDs(lowest);
        for(char[] dimer : all) {
            float[] DhDs = getDimereDhDs(dimer);
            if(DhDs[0] > lowestDhDs[0]) {
                lowest = dimer;
                lowestDhDs = DhDs;
            }
            else if(DhDs[0] == lowestDhDs[0] && DhDs[1] < lowestDhDs[1]) {
                lowest = dimer;
                lowestDhDs = DhDs;
            }
        }
        return lowest;
    }

    private char[] getLowestMelting() {
        char[] seq = getSequence().replace("-", "").toLowerCase().toCharArray();
        if(seq.length < 2) {
            char[] res = {'A', 'A'};
            return(res);
        }
        char[] resSeq = new char[seq.length];
        char[] dimer = {seq[0], seq[1]};
        dimer = getLowestDimer(dimer);
        resSeq[0] = dimer[0];
        for(int i=1; i<seq.length-1; i++) {
            dimer[0] = dimer[1];
            dimer[1] = seq[i+1];
            dimer = getLowestDimer(dimer);
            resSeq[i] = dimer[0];
            resSeq[i+1] = dimer[1];
        }
        return resSeq;
    }

    private float[] getDimereDhDs(char[] dimere) {
        float[] res = {0, 0};
        float sumdh = 0;
        float sumds = 0;
        switch (dimere[0]) {
            case 'a':
                switch (dimere[1]) {
                    case 'a':
                        sumdh -= 7.9;
                        sumds -= 22.2;
                        break;
                    case 'g':
                        sumdh -= 7.8;
                        sumds -= 21.0;
                        break;
                    case 't':
                        sumdh -= 7.2;
                        sumds -= 20.4;
                        break;
                    case 'c':
                        sumdh -= 8.4;
                        sumds -= 22.4;
                        break;
                }
                break;
            case 'g':
                switch (dimere[1]) {
                    case 'a':
                        sumdh -= 8.2;
                        sumds -= 22.2;
                        break;
                    case 'g':
                        sumdh -= 8.0;
                        sumds -= 19.9;
                        break;
                    case 't':
                        sumdh -= 8.4;
                        sumds -= 22.4;
                        break;
                    case 'c':
                        sumdh -= 10.6;
                        sumds -= 27.2;
                        break;
                }
                break;
            case 't':
                switch (dimere[1]) {
                    case 'a':
                        sumdh -= 7.2;
                        sumds -= 21.3;
                        break;
                    case 'g':
                        sumdh -= 8.5;
                        sumds -= 22.7;
                        break;
                    case 't':
                        sumdh -= 7.9;
                        sumds -= 22.2;
                        break;
                    case 'c':
                        sumdh -= 8.2;
                        sumds -= 22.2;
                        break;
                }
                break;
            case 'c':
                switch (dimere[1]) {
                    case 'a':
                        sumdh -= 8.5;
                        sumds -= 22.7;
                        break;
                    case 'g':
                        sumdh -= 10.6;
                        sumds -= 27.2;
                        break;
                    case 't':
                        sumdh -= 7.8;
                        sumds -= 21.0;
                        break;
                    case 'c':
                        sumdh -= 8;
                        sumds -= 19.9;
                        break;
                }
                break;

        }
        res[0] = sumdh;
        res[1] = sumds;
        return res;
    }

    public void updateTm() {
        float sumdh = 0;
        float sumds = 0;
//        char[] seq = getSequence().toLowerCase().toCharArray();
        char[] seq = getLowestMelting();
        char base = seq[0];
        // dH, dS calculations (Santalucia, 1998)
        for(int i=1; i<seq.length; i++) {
            char nbase = seq[i];
            char[] dimere = {base, nbase};
            float[] energies = getDimereDhDs(dimere);
            sumdh += energies[0];
            sumds += energies[1];
            base = nbase;
        }
        // Terminal and starting corrections, Santalucia 1998
        if(seq[0] == 'a' || seq[0] == 't') {sumdh += 2.3; sumds += 4.1; }
        else if(seq[0] == 'g' || seq[0] == 'c') {sumdh += 0.1; sumds += -2.8; }
        if(seq[seq.length-1] == 'a' || seq[seq.length-1] == 't') {sumdh += 2.3; sumds += 4.1; }
        else if(seq[seq.length-1] == 'g' || seq[seq.length-1] == 'c') {sumdh += 0.1; sumds += -2.8; }

        //Salt correction, von Shsen et al 1999
        Settings settings = list.sequence.align.getSettings();
        float saltconc = settings.getFloat(Settings.SALT_CONCENTRATION);
        float mgconc = settings.getFloat(Settings.MG_CONCENTRATION);
        float conc_primer = settings.getFloat(Settings.PRIMER_CONCENTRATION);
        sumds += 0.368*(seq.length)*Math.log(saltconc/1000 + mgconc/1000*140);
        _tm = (float)((1000*sumdh)/(sumds+(1.987*Math.log(conc_primer/2000000000)))-273.15);
    }

    public float getTm() {
        return _tm;
    }
}
