/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author dabrowskiw
 */
public class Settings {
    public static String[] SALT_CONCENTRATION = {"saltConcentration", "200"};
    public static String[] MG_CONCENTRATION = {"mgConcentration", "0"};
    public static String[] PRIMER_CONCENTRATION = {"primerConcentration", "50"};
    public static String[] DISPLAY_PRIMER_LENGTH = {"displayPrimerLength", "1"};
    public static String[] DISPLAY_PRIMER_TEMPERATURE = {"displayPrimerTemperature", "1"};
    public static String[] DISPLAY_PRIMER_NAME = {"displayPrimerName", "1"};
    public static String[] DISPLAY_IUB = {"DisplayIUB", "0"};
    public static String[] DISPLAY_SEQUENCE_NAME = {"displaySequenceName", "1"};
    public static String[] LAST_LOAD_PATH = {"lastLoadPath", "."};
    public static String[] LAST_SAVE_PATH = {"lastSavePath", "."};
    public static String[] DEFAULT_FORWARD_PRIMER_COLOR = {"defaultForwardPrimerColor", "0x0000ff"};
    public static String[] DEFAULT_REVERSE_PRIMER_COLOR = {"defaultReversePrimerColor", "0x84b47f"};
    public static String[] DEFAULT_ABOVE_ANNOTATION_COLOR = {"defaultAboveAnnotationColor", "0xff0000"};
    public static String[] DEFAULT_BELOW_ANNOTATION_COLOR = {"defaultBelowAnnotationColor", "0xde842a"};
    public static String[] SHOW_DTM_WARNING = {"showDeltaTmWarning", "1"};
    public static String[] DTM_WARNING_TEMP = {"deltaTmWarningTemperature", "10"};
    public static String[] DTM_WARNING_COLOR = {"deltaTmWarningColor", "0xff0000"};
    public static String[] RECENT_FILES = {"recentFiles", ""};

    public Map<String, String> settings = new HashMap<String, String>();

    public Settings() {
        load();
    }

    public String get(String[] keydef) {
        return get(keydef[0], keydef[1]);
    }

    public String get(String key, String def) {
        if(!settings.containsKey(key)) {
            return def;
        }
        return settings.get(key);
    }

    public float getFloat(String[] keydef) {
        return getFloat(keydef[0], Float.parseFloat(keydef[1]));
    }

    public float getFloat(String key, float def) {
        if(!settings.containsKey(key)) {
            return def;
        }
        return Float.parseFloat(settings.get(key));
    }

    public boolean getBoolean(String[] keydef) {
        return getBoolean(keydef[0], Integer.parseInt(keydef[1])!=0);
    }

    public boolean getBoolean(String key, boolean def) {
        if(!settings.containsKey(key)) {
            return def;
        }
        return Integer.parseInt(settings.get(key)) != 0;
    }

    public String[] getStringArray(String key, String[] def) {
        if(!settings.containsKey(key)) {
            if(def == null) {
                String[] res = {};
                return res;
            }
            return def;
        }
        String val = settings.get(key);
        return val.split("\t");
    }

    public void set(String[] key, String value) {
        set(key[0], value);
    }

    public void set(String key, String value) {
        settings.put(key, value);
        save();
    }

    public void set(String[] key, boolean value) {
        set(key[0], value);
    }
    public void set(String key, boolean value) {
        settings.put(key, Integer.toString(value?1:0));
        save();
    }

    public void set(String[] key, int value) {
        set(key[0], value);
    }

    public void set(String key, int value) {
        settings.put(key, Integer.toString(value));
        save();
    }

    public void set(String[] key, float value) {
        set(key[0], value);
    }

    public void set(String key, float value) {
        settings.put(key, Float.toString(value));
        save();
    }

    public void set(String[] key, double value) {
        set(key[0], value);
    }

    public void set(String key, double value) {
        settings.put(key, Double.toString(value));
        save();
    }

    public void set(String[] key, String[] value) {
        set(key[0], value);
    }

    public void set(String key, String[] values) {
        StringBuilder res = new StringBuilder();
        for(int i=0; i<values.length; i++) {
            res.append(values[i]);
            res.append((i!=values.length-1?"\t":""));
        }
        settings.put(key, res.toString());
        save();
    }

    public final void load() {
        try {
            File file = new File(".settings");
            if(!file.exists())
                return;
            BufferedReader input = new BufferedReader(new FileReader(file));
            while(input.ready()) {
                String line = input.readLine();
                String key = line.substring(0, line.indexOf('='));
                if(key.length() != 0) {
                    settings.put(key, line.substring(line.indexOf('=')+1).trim());
                }
            }
        }
        catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Setting file '.settings' could not be opened for reading. Check permissions.", "Error loading file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void save() {
        BufferedWriter out = null;
        try {
            out = new BufferedWriter(new FileWriter(".settings"));
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Settings file '.settings' could not be opened for writing. Check permissions.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            for(String key : settings.keySet()) {
                out.write(key);
                out.write("=");
                out.write(settings.get(key));
                out.write("\n");
            }
            out.close();
        } catch(IOException e) {
            JOptionPane.showMessageDialog(null, "Could not write settings to file. Check disk space.", "Error saving file", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }
}
