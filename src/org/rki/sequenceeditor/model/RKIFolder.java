/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

/**
 *
 * @author dabrowskiw
 */
public class RKIFolder {
    public String name = "";
    public String id = "";

    public RKIFolder(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return(name);
    }
}
