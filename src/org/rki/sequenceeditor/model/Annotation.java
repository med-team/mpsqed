package org.rki.sequenceeditor.model;

import java.awt.Color;

public class Annotation {
    public int from;
    public int to;
    public Color background;
    public String text;
    public AnnotationList list = null;
    
    public Annotation(int from, int to, Color background, String text, AnnotationList list) {
        this.from = from;
        this.to = to;
        this.background = background;
        this.text = text;
        this.list = list;
    }
    
    public void reset() {
        list.resetAnnotation(this);
    }

    public String toString(boolean above) {
        return "";
    }
}
