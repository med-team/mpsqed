/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

/**
 *
 * @author dabrowskiw
 */
public class RKIFile {
    public String name;
    public String id;

    public RKIFile(String name, int id) {
        this.name = name;
        this.id = new Integer(id).toString();
    }

    @Override
    public String toString() {
        return(name);
    }
}
