package org.rki.sequenceeditor.model;

import java.awt.Color;
import java.util.HashMap;

public class Sequence implements Comparable {
    public String seq;
    public String name;
    public double homology = 0.0;
    public boolean selected = false;
    public boolean collapsed = false;
    public AnnotationList above = new AnnotationList(true, this);
    public AnnotationList below = new AnnotationList(false, this);
    public Alignment align;

    public GroupKey group;
    
    public Sequence(String name, String seq, GroupKey group, Alignment align) {
        this.seq = seq;
        this.name = name;
        this.group = group;
        this.align = align;
    }

    /**
     * Creates complementary, inverted sequence.
     */
    public void invert() {
        this.seq = invert(this.seq);
    }
    
    public static String cleanUpSequence(String seq) {
        seq = seq.replaceAll(" ", "");
        seq = seq.replaceAll("[0-9]", "");
        return(seq);
    }

    public static String invert(String sequence) {
        char[] cseq = sequence.toCharArray();
        HashMap<Character, Character> gc = Globals.complementary;
        char[] nseq = new char[cseq.length];
        for(int i=cseq.length-1; i>=0; i--) {
            nseq[cseq.length-i-1] = Globals.complementary.get(cseq[i]);
        }
        return(new String(nseq));
    }
    
    public void setCharAt(int pos, char ch) {
        char[] cseq = seq.toCharArray();
        cseq[pos] = ch;
        this.seq = new String(cseq);
    }
    
    public void calculateHomology(Sequence master) {
        int minlen = Math.min(master.seq.length(), seq.length());
        char[] mastercseq = master.seq.toCharArray();
        char[] cseq = seq.toCharArray();
        int identities = 0;
        for(int i=0; i<minlen; i++) {
            if(mastercseq[i] == cseq[i]) {
                identities ++;
            }
        }
        homology = (double)identities/(double)minlen;
    }
    
    /**
     * Adds an annotation to the sequence
     * @param above true if annotation is to be added above the sequence, false otherwise
     * @param from first base to be included in the annotation
     * @param to last base to be included in the annotation
     * @param text text of the annotation
     */
    public void addAnnotation(boolean above, int from, int to, String text, boolean primer) {
        Annotation anno = null;
        Color c = null;
        if(primer && above)
            c = Color.decode(align.getSettings().get(Settings.DEFAULT_FORWARD_PRIMER_COLOR).trim());
        else if(primer && !above)
            c = Color.decode(align.getSettings().get(Settings.DEFAULT_REVERSE_PRIMER_COLOR).trim());
        else if(!primer && above)
            c = Color.decode(align.getSettings().get(Settings.DEFAULT_ABOVE_ANNOTATION_COLOR).trim());
        else if(!primer && !above)
            c = Color.decode(align.getSettings().get(Settings.DEFAULT_BELOW_ANNOTATION_COLOR).trim());
        if(primer)
            anno = new Primer(from, to, c, text, above, this.above);
        else
            anno = new Annotation(from, to, c, text, this.below);
        if(above) {
            this.above.addAnnotation(anno);
        }
        else {
            this.below.addAnnotation(anno);
        }
        if(primer) {
            ((Primer)anno).updateOpposite();
        }
    }

    public void updateProducts() {
        for(Annotation anno : above.getAnnotations()) {
            if(anno instanceof Primer) {
                ((Primer)anno).updateOpposite();
            }
        }
    }

    public boolean removePart(int start, int end) {
        if(start < 0)
            start = 0;
        if(end > seq.length()) {
            end = seq.length();
        }
        if(start > end) {
            int tmp = start;
            start = end;
            end = start;
        }
        char[] cseq = seq.toCharArray();
        char[] res = new char[cseq.length - (end - start)];
        int j=0;
        for(int i=0; i<cseq.length; i++) {
            if(i >= start && i < end) {
                continue;
            }
            res[j] = cseq[i];
            j++;
        }
        this.seq = new String(res);
        return true;
    }

    /**  
     * Moves part of the sequece
     * 
     * @param start beginning of slice to move
     * @param end end of slice to move
     * @param diff amount to move by
     * @return true on success, false on failure (e.g. out of bounds) 
     */
    public boolean movePart(int start, int end, int diff) {
        // In Editor, the selection is given as from (including) to (excluding). The
        // calculations here are using bounds from (including) to (including).
        end -= 1;
        if(start+diff < 0 || end + diff > seq.length()) {
            return(false);
        }
        char[] cseq = seq.toCharArray();
        char[] tmp = new char[Math.abs(diff)];
        if(diff > 0) {
            for(int i=end+1; i<=end+diff; i++) {
                tmp[i-end-1] = cseq[i];
            }
            for(int i=end; i>=start; i--) {
                cseq[i+diff] = cseq[i];
            }
            for(int i=start; i<start+diff; i++) {
                cseq[i] = tmp[i-start];
            }
        }
        else if(diff < 0) {
            diff *= -1;
            for(int i=start-diff; i<start; i++) {
                tmp[i-start+diff] = cseq[i];
            }
            for(int i=start; i<=end; i++) {
                cseq[i-diff] = cseq[i];
            }
            for(int i=end-diff+1; i<=end; i++) {
                cseq[i] = tmp[i-end+diff-1];
            }
        }
        this.seq = new String(cseq);        
        return(true);
    }

    public int compareTo(Object o) {
        if(!(o instanceof Sequence)) {
            return 0;
        }
        Sequence s = (Sequence)o;
        return s.group.compareTo(group);
    }
}
