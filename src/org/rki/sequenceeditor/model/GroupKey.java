/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

/**
 *
 * @author dabrowskiw
 */
public class GroupKey implements Comparable<GroupKey> {
    private String _name;
    
    public GroupKey(Object name) {
        _name = name.toString();
    }

    public String getName() {
        return(_name);
    }
    
    @Override
    public String toString() {
        return(_name);
    }

    public int compareTo(GroupKey o) {
        return(_name.toLowerCase().compareTo(o.getName().toLowerCase()));
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof GroupKey) 
            return ((GroupKey)o).getName().toLowerCase().equals(_name.toLowerCase());
        else
            return false;
    }
}
