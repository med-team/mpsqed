/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.rki.sequenceeditor.model;

import java.util.HashMap;

/**
 *
 * @author dabrowskiw
 */
public class Letters {
    public static final int[] sizes = {1, 2, 4, 8, 10, 12};
    
    private static final int[] __1 = 
    {
        0,
        0,
        0,
        1,
        0,
        0,
        0,
        0,
    };
    private static final int[] _a1 = 
    {
        0,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
    };
    private static final int[] _g1 = _a1;
    private static final int[] _t1 = _a1;
    private static final int[] _u1 = _a1;
    private static final int[] _c1 = _a1;

    private static final int[] __2 = 
    {
        0,0,
        0,0,
        0,0,
        1,1,
        0,0,
        0,0,
        0,0,
        0,0,
    };
    private static final int[] _a2 = 
    {
        0,0,
        1,1,
        1,1,
        1,1,
        1,1,
        1,1,
        1,1,
        0,0,
    };
    private static final int[] _g2 = _a2;
    private static final int[] _t2 = _a2;
    private static final int[] _u2 = _a2;
    private static final int[] _c2 = _a2;

    private static final int[] __4 = 
    {
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        1,1,1,1,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0,
        0,0,0,0
    };
    private static final int[] _a4 = 
    {
        0,0,0,0,
        0,1,1,0,
        0,1,1,0,
        0,1,1,0,
        0,1,1,0,
        0,1,1,0,
        0,1,1,0,
        0,0,0,0
    };
    private static final int[] _g4 = _a4;
    private static final int[] _t4 = _a4;
    private static final int[] _u4 = _a4;
    private static final int[] _c4 = _a4;

    
    private static final int[] __8 = 
    {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 1, 1, 1, 1, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    private static final int[] _a8 = {0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 1, 0, 0, 1, 0, 0,
        0, 0, 1, 0, 0, 1, 0, 0,
        0, 0, 1, 1, 1, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 1, 0,
        0, 1, 0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    private static final int[] _g8 = {0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 1, 0, 0, 1, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 1, 1, 0, 0,
        0, 0, 1, 0, 0, 1, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    private static final int[] _t8 = {0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 1, 1, 1, 1, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    private static final int[] _u8 = {0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 0,
        0, 0, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    private static final int[] _c8 = {0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 1, 0, 0, 1, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 1, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };

    private static final int[] _qm8 = {0, 0, 0, 0, 0, 0, 0, 0,
        0,0,0,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,0,0,0,1,0,0,
        0,0,0,0,1,0,0,0,
        0,0,0,1,0,0,0,0,
        0,0,0,0,0,0,0,0,
        0,0,0,1,0,0,0,0
    };

    private static final int[] _r8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,1,1,0,0,0,
        0,0,1,0,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _y8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,
        0,0,0,1,0,1,0,0,
        0,0,0,0,1,0,0,0,
        0,0,0,0,1,0,0,0,
        0,0,0,0,1,0,0,0,
        0,0,0,0,1,0,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _s8 = {
        0,0,0,0,0,0,0,0,
        0,0,0,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,0,0,0,
        0,0,0,1,1,0,0,0,
        0,0,0,0,0,1,0,0,
        0,0,0,1,1,0,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _w8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,1,0,1,0,
        0,0,1,0,1,0,1,0,
        0,0,0,1,0,1,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _k8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,1,0,0,0,
        0,0,1,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,0,1,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _m8 = {
        0,0,0,0,0,0,0,0,
        0,0,0,1,0,1,0,0,
        0,0,1,0,1,0,1,0,
        0,0,1,0,1,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _b8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,1,1,0,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _d8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,1,1,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,1,1,0,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _h8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,1,1,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _v8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,1,0,0,0,1,0,
        0,0,0,1,0,1,0,0,
        0,0,0,1,0,1,0,0,
        0,0,0,0,1,0,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] _n8 = {
        0,0,0,0,0,0,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,1,0,1,0,0,
        0,0,1,0,1,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,0,
        0,0,0,0,0,0,0,0
    };

    private static final int[] __10 = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 1, 1, 1, 1, 1, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };
    
    private static final int[] _a10 = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 1, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 1, 1, 1, 1, 1, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };
    private static final int[] _g10 = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 1, 1, 1, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
        0, 0, 0, 0, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };

    private static final int[] _t10 = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 1, 1, 1, 1, 1, 1, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };

    private static final int[] _u10 = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 1, 0, 0, 0, 1, 1, 0, 0,
        0, 0, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };

    private static final int[] _c10 = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 1, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 1, 0, 0,
        0, 0, 0, 1, 0, 0, 1, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    };

    private static final int[] _r10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,1,1,0,0,0,0,0,0,
        0,0,1,0,1,0,0,0,0,0,
        0,0,1,0,0,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _y10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,1,0,
        0,0,0,1,0,0,0,1,0,0,
        0,0,0,0,1,0,1,0,0,0,
        0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _s10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,1,1,1,1,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,1,1,1,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,0,1,1,1,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _w10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,1,1,0,1,0,0,
        0,0,1,0,1,1,0,1,0,0,
        0,0,0,1,0,0,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _k10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,1,0,0,0,0,
        0,0,1,0,1,0,0,0,0,0,
        0,0,1,1,1,0,0,0,0,0,
        0,0,1,0,0,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _m10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,1,1,0,1,1,0,0,
        0,0,1,0,0,1,0,0,1,0,
        0,0,1,0,0,1,0,0,1,0,
        0,0,1,0,0,0,0,0,1,0,
        0,0,1,0,0,0,0,0,1,0,
        0,0,1,0,0,0,0,0,1,0,
        0,0,1,0,0,0,0,0,1,0,
        0,0,1,0,0,0,0,0,1,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _b10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _d10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,1,1,1,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _h10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,1,1,1,1,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _v10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,0,1,0,0,1,0,0,0,
        0,0,0,1,0,0,1,0,0,0,
        0,0,0,0,1,1,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };
    
    private static final int[] _n10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,1,0,0,0,1,0,0,
        0,0,1,0,1,0,0,1,0,0,
        0,0,1,0,0,1,0,1,0,0,
        0,0,1,0,0,0,1,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _qm10 = {
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,1,1,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,1,0,0,0,1,0,0,0,
        0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] __12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,1,1,1,1,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };
    private static final int[] _a12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,1,0,0,0,0,0,
        0,0,0,0,1,1,1,0,0,0,0,0,
        0,0,0,0,1,0,1,0,0,0,0,0,
        0,0,0,1,1,0,1,1,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,1,1,1,1,1,1,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,1,0,0,
        0,1,1,1,1,0,0,1,1,1,1,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _g12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,1,1,1,1,0,1,0,0,
        0,0,0,1,1,0,0,0,1,1,0,0,
        0,0,1,1,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,1,1,1,1,1,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,1,0,0,0,0,0,1,0,0,
        0,0,0,1,1,0,0,0,1,1,0,0,
        0,0,0,0,1,1,1,1,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _t12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,1,1,1,0,0,0,
        0,0,1,0,0,1,0,0,1,0,0,0,
        0,0,1,0,0,1,0,0,1,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,1,1,1,1,1,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _u12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,1,1,0,0,0,1,1,0,0,0,
        0,0,0,1,1,1,1,1,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    
    private static final int[] _c12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,1,1,1,1,0,1,0,0,
        0,0,0,1,1,0,0,0,1,1,0,0,
        0,0,1,1,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,0,0,0,
        0,0,1,1,0,0,0,0,0,0,0,0,
        0,0,0,1,1,0,0,0,1,1,0,0,
        0,0,0,0,1,1,1,1,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _r12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,1,0,0,1,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,1,1,1,0,0,1,1,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _y12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,1,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,1,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,1,1,1,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _s12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,1,1,1,0,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,0,0,0,1,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,0,1,1,1,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _w12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,1,1,0,0,1,0,0,
        0,0,1,0,1,0,0,1,0,1,0,0,
        0,0,1,0,1,0,0,1,0,1,0,0,
        0,0,1,0,1,0,0,1,0,1,0,0,
        0,0,0,1,1,0,0,1,1,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _k12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,1,0,0,0,0,0,1,0,0,
        0,0,0,1,0,0,0,0,1,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,1,0,0,1,0,0,0,0,0,
        0,0,0,1,0,1,0,0,0,0,0,0,
        0,0,0,1,1,1,0,0,0,0,0,0,
        0,0,0,1,0,0,1,0,0,0,0,0,
        0,0,0,1,0,0,0,1,0,0,0,0,
        0,0,0,1,0,0,0,0,1,0,0,0,
        0,0,0,1,0,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _m12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,1,1,1,0,0,0,1,1,1,0,0,
        0,1,0,0,1,0,1,0,0,1,0,0,
        0,1,0,0,0,1,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,1,0,0,0,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _b12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,1,0,0,1,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _d12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,0,1,0,0,0,0,
        0,0,1,0,0,0,1,0,0,0,0,0,
        0,0,1,1,1,1,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _h12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,1,1,1,1,1,1,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _v12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,0,1,0,
        0,0,1,0,0,0,0,0,0,0,1,0,
        0,0,0,1,0,0,0,0,0,1,0,0,
        0,0,0,1,0,0,0,0,0,1,0,0,
        0,0,0,0,1,0,0,0,1,0,0,0,
        0,0,0,0,1,0,0,0,1,0,0,0,
        0,0,0,0,0,1,0,1,0,0,0,0,
        0,0,0,0,0,1,0,1,0,0,0,0,
        0,0,0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _n12 = {
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,1,0,0,0,0,0,1,0,0,
        0,0,1,0,1,0,0,0,0,1,0,0,
        0,0,1,0,0,1,0,0,0,1,0,0,
        0,0,1,0,0,0,1,0,0,1,0,0,
        0,0,1,0,0,0,0,1,0,1,0,0,
        0,0,1,0,0,0,0,0,1,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,1,0,0,0,0,0,0,1,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    private static final int[] _qm12 = {
        0,0,0,0,0,1,1,0,0,0,0,0,
        0,0,0,0,1,0,0,1,0,0,0,0,
        0,0,0,1,0,0,0,0,1,0,0,0,
        0,0,0,1,0,0,0,0,1,0,0,0,
        0,0,0,0,1,0,0,0,1,0,0,0,
        0,0,0,0,0,0,0,1,0,0,0,0,
        0,0,0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,1,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,
    };

    
public static HashMap<Integer, int[]> letters = new HashMap<Integer, int[]>();

    static {
        letters.put('-' + 100, __1);
        letters.put('A' + 100, _a1);
        letters.put('G' + 100, _g1);
        letters.put('T' + 100, _t1);
        letters.put('U' + 100, _t1);
        letters.put('C' + 100, _c1);
        letters.put('R' + 100, _c1);
        letters.put('Y' + 100, _c1);
        letters.put('S' + 100, _c1);
        letters.put('W' + 100, _c1);
        letters.put('K' + 100, _c1);
        letters.put('M' + 100, _c1);
        letters.put('B' + 100, _c1);
        letters.put('D' + 100, _c1);
        letters.put('H' + 100, _c1);
        letters.put('V' + 100, _c1);
        letters.put('N' + 100, _c1);
        letters.put('?' + 100, __1);

        letters.put('-' + 200, __2);
        letters.put('A' + 200, _a2);
        letters.put('G' + 200, _g2);
        letters.put('T' + 200, _t2);
        letters.put('U' + 200, _u2);
        letters.put('R' + 200, _c2);
        letters.put('C' + 200, _c2);
        letters.put('Y' + 200, _c2);
        letters.put('S' + 200, _c2);
        letters.put('W' + 200, _c2);
        letters.put('K' + 200, _c2);
        letters.put('M' + 200, _c2);
        letters.put('B' + 200, _c2);
        letters.put('D' + 200, _c2);
        letters.put('H' + 200, _c2);
        letters.put('V' + 200, _c2);
        letters.put('N' + 200, _c2);
        letters.put('?' + 200, __2);
        
        letters.put('-' + 400, __4);
        letters.put('A' + 400, _a4);
        letters.put('G' + 400, _g4);
        letters.put('T' + 400, _t4);
        letters.put('U' + 400, _u4);
        letters.put('C' + 400, _c4);
        letters.put('R' + 400, _c4);
        letters.put('Y' + 400, _c4);
        letters.put('S' + 400, _c4);
        letters.put('W' + 400, _c4);
        letters.put('K' + 400, _c4);
        letters.put('M' + 400, _c4);
        letters.put('B' + 400, _c4);
        letters.put('D' + 400, _c4);
        letters.put('H' + 400, _c4);
        letters.put('V' + 400, _c4);
        letters.put('N' + 400, _c4);
        letters.put('?' + 400, __4);
        
        letters.put('-' + 800, __8);
        letters.put('A' + 800, _a8);
        letters.put('G' + 800, _g8);
        letters.put('T' + 800, _t8);
        letters.put('U' + 800, _u8);
        letters.put('C' + 800, _c8);
        letters.put('R' + 800, _r8);
        letters.put('Y' + 800, _y8);
        letters.put('S' + 800, _s8);
        letters.put('W' + 800, _w8);
        letters.put('K' + 800, _k8);
        letters.put('M' + 800, _m8);
        letters.put('B' + 800, _b8);
        letters.put('D' + 800, _d8);
        letters.put('H' + 800, _h8);
        letters.put('V' + 800, _v8);
        letters.put('N' + 800, _n8);
        letters.put('?' + 800, _qm8);

        letters.put('-' + 1000, __10);
        letters.put('A' + 1000, _a10);
        letters.put('G' + 1000, _g10);
        letters.put('T' + 1000, _t10);
        letters.put('U' + 1000, _u10);
        letters.put('C' + 1000, _c10);
        letters.put('R' + 1000, _r10);
        letters.put('Y' + 1000, _y10);
        letters.put('S' + 1000, _s10);
        letters.put('W' + 1000, _w10);
        letters.put('K' + 1000, _k10);
        letters.put('M' + 1000, _m10);
        letters.put('B' + 1000, _b10);
        letters.put('D' + 1000, _d10);
        letters.put('H' + 1000, _h10);
        letters.put('V' + 1000, _v10);
        letters.put('N' + 1000, _n10);
        letters.put('?' + 1000, _qm10);
        
        letters.put('-' + 1200, __12);
        letters.put('A' + 1200, _a12);
        letters.put('G' + 1200, _g12);
        letters.put('T' + 1200, _t12);
        letters.put('U' + 1200, _u12);
        letters.put('C' + 1200, _c12);
        letters.put('R' + 1200, _r12);
        letters.put('Y' + 1200, _y12);
        letters.put('S' + 1200, _s12);
        letters.put('W' + 1200, _w12);
        letters.put('K' + 1200, _k12);
        letters.put('M' + 1200, _m12);
        letters.put('B' + 1200, _b12);
        letters.put('D' + 1200, _d12);
        letters.put('H' + 1200, _h12);
        letters.put('V' + 1200, _v12);
        letters.put('N' + 1200, _n12);
        letters.put('?' + 1200, _qm12);
    }
    
}
